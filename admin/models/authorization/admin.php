<?php

class user_admin extends ModelAdmin {
    public function __construct() {
        $this->menuIcon = "fa-user";

        $this->listColumns = array(
            "username",
            "fullname",
            "is_staff",
            "is_superuser",
            "is_active",
            "last_login_date",
        );

        $this->searchFields = array(
            "first_name",
            "last_name",
        );

        $this->searchFilters = array(
            "is_active",
        );
    }
}

class group_admin extends ModelAdmin {
    public function __construct() {
        $this->menuIcon = "fa-users";
    }
}

?>
