<?php

class user extends Model {

    public function __construct($db = null) {
        $this->fields['username'] = new CharField(array("required"=>true,"readonlyOnUpdate"=>true,"helpText"=>"This will be used in order for you to login into the system."));
        $this->fields['password'] = new PasswordField(array("helpText"=>"If you are updating this user and you want to change this user's password then provide a password else leave it blank."));
        $this->fields['repeat_password'] = new PasswordField(array("helpText"=>"This field must match the password you provided"));
        $this->fields['first_name'] = new CharField(array("required"=>true));
        $this->fields['last_name'] = new CharField(array("required"=>true));
        $this->fields['is_staff'] = new BooleanField(array("value"=>true,"helpText"=>"Determines whether this user can login into admin site or not"));
        $this->fields['is_superuser'] = new BooleanField(array("value"=>false,"helpText"=>"This means that this user can do anything in the system without restrictions"));
        $this->fields['is_active'] = new BooleanField(array("value"=>true,"helpText"=>"Uncheck this if you don't want this user to be used anymore"));
        $this->fields['create_date'] = new DateTimeField(array("hidden"=>true,"canAdd"=>false,"canUpdate"=>false));
        $this->fields['update_date'] = new DateTimeField(array("hidden"=>true,"canAdd"=>false,"canUpdate"=>false));
        $this->fields['delete_date'] = new DateTimeField(array("hidden"=>true,"canAdd"=>false,"canUpdate"=>false));
        $this->fields['last_login_date'] = new DateTimeField(array("readonly"=>true,"canAdd"=>false,"canUpdate"=>true));

        $this->m2m['group_user'] = new M2MField(array("objectName"=>"user","otherObjectName"=>"group","displayName"=>"User Groups","helpText"=>"Select the group/s where this user belongs"));
        $this->m2m['user_permission'] = new M2MField(array("objectName"=>"user","otherObjectName"=>"permission","displayName"=>"User Permissions","helpText"=>"Select the allowable actions for the user"));

        parent::__construct($db);
    }

    public function fullname() {
        return $this->first_name . " " . $this->last_name;
    }

    public function __toString() {
        return $this->fullname() . " (" . $this->username . ")";
    }

    public function can_approve() {
        if ($this->is_superuser) {
            return true;
        }
        
        $group_user = new group_user();
        $qs = $group_user->filter(
            $group_user->user__exact($this->id),
            $group_user->group__can_approve__exact(1)
        )->count();
        
        return ($qs > 0);
    }

    // function: login
    // description: returns true if given username and password is correct otherwise false
    // parameters: $username, $password
    // return: boolean
    public function login($username, $password) {
        $result = false;

        $qs = $this->filter(
            $this->username__exact($username),
            $this->password__exact(md5($password)),
            $this->is_active__exact(1),
            _OR(
                $this->is_staff__exact(1),
                $this->is_superuser__exact(1)
            )
        )->fetch();

        if(count($qs) > 0) {
            $result = true;
            $user = $qs[0];
            $user->last_login_date = date("Y-m-d H:i:s");
            $rs = $user->save();
        }
        return $result;
    }

    public function save() {
        $db = $this->get_db();

        $this->username = strtolower($this->username);

        // fetch previous user info
        $prev = new User($db);
        $qs = $prev->filter($prev->username__contains($this->username))->fetch();
        if (count($qs) > 0) {
            $prev = $qs[0];
        }

        if ($this->id == 0) {
            if ($this->username == $prev->username) {
                return array("status"=>"ERROR","msg"=>"Username already used!");
            }
        }

        if ($this->password == "") {
            // password will not be changed
            unset($this->modified["password"]);
        } elseif ($this->password == $this->repeat_password) {
            // set password
            $this->password = md5($this->password);
            $this->repeat_password = "";
        } else {
            if ($this->id != 0 && $this->repeat_password != "") {
                // Someone is trying to update the password but not match.
                return array("status"=>"ERROR","msg"=>"Password and repeat password not match!");
            }
        }
        return parent::save();
    }
}

class group extends Model {

    public function __construct($db = null) {
        $this->fields['name'] = new CharField(array("required"=>true));
        $this->fields['desc'] = new CharField(array("displayName"=>"Description"));
        $this->fields['can_approve'] = new BooleanField(array("helpText"=>"Tells whether a user belonging to this group can approve delivery receipt form"));

        $this->m2m['group_user'] = new M2MField(array("objectName"=>"group","otherObjectName"=>"user","displayName"=>"Group Users","helpText"=>"Select the user/s belonging to this group"));
        $this->m2m['group_permission'] = new M2MField(array("objectName"=>"group","otherObjectName"=>"permission","displayName"=>"User Permissions","helpText"=>"Select the allowable actions for the group"));

        parent::__construct($db);
    }

    public function __toString() {
        return $this->name;
    }
}

class permission extends Model {

    public function __construct($db = null) {
        $this->fields['name'] = new CharField();
        $this->fields['model'] = new CharField();
        $this->fields['module'] = new CharField();

        parent::__construct($db);
    }

    public function __toString() {
        return str_replace("_"," ",$this->name);
    }
}

class group_user extends Model {

    public function __construct($db = null) {
        $this->fields['group'] = new ForeignKey(array("modelName"=>"group"));
        $this->fields['user'] = new ForeignKey(array("modelName"=>"user"));

        parent::__construct($db);
    }
}

class user_permission extends Model {

    public function __construct($db = null) {
        $this->fields['user'] = new ForeignKey(array("modelName"=>"user"));
        $this->fields['permission'] = new ForeignKey(array("modelName"=>"permission"));

        parent::__construct($db);
    }
}

class group_permission extends Model {

    public function __construct($db = null) {
        $this->fields['group'] = new ForeignKey(array("modelName"=>"group"));
        $this->fields['permission'] = new ForeignKey(array("modelName"=>"permission"));

        parent::__construct($db);
    }
}

?>
