<?php

class delivery_receipt_admin extends ModelAdmin {
    public function __construct() {
        $this->inlines = array(
            "delivery_receipt_line"
        );

        $this->listColumns = array(
            "dr_no",
            "create_date",
            "warehouse_To",
            "is_approved",
            "is_returned",
            "download",
        );

        $this->searchFields = array(
            "dr_no",
            "warehouse_To__name",
        );

        $this->searchFilters = array(
            "create_date",
            "is_returned",
            "is_approved",
        );
    }
}

class delivery_receipt_line_admin extends ModelAdmin {
    public function __construct() {
        $this->inlines = array(
            "delivery_receipt_line_serial_number"
        );

        $this->listColumns = array(
            "item",
            "quantity",
            "unit_of_measure",
            "remarks",
            "is_returned",
        );
    }

    public function IsDisplayed() {
        return false;
    }
}

class delivery_receipt_line_serial_number_admin extends ModelAdmin {
    public function __construct() {
        
    }

    public function IsDisplayed() {
        return false;
    }
}

class return_form_admin extends ModelAdmin {
    public function __construct() {
        $this->inlines = array(
            "return_form_line"
        );

        $this->listColumns = array(
            "return_no",
            "warehouse_To",
            "approved_by",
            "create_date",
            "download",
        );

        $this->searchFields = array(
            "return_no",
            "warehouse_To__name",
        );

        $this->searchFilters = array(
            "create_date",
        );
    }
}

class return_form_line_admin extends ModelAdmin {
    public function __construct() {
        $this->inlines = array(
            "return_form_line_serial_number"
        );

        $this->listColumns = array(
            "item",
            "quantity",
            "unit_of_measure",
            "remarks",
        );
    }

    public function IsDisplayed() {
        return false;
    }
}

class return_form_line_serial_number_admin extends ModelAdmin {
    public function __construct() {
        
    }

    public function IsDisplayed() {
        return false;
    }
}

?>