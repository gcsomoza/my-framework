<?php

class delivery_receipt extends Model {

    public function __construct($db = null) {
        $this->fields['dr_no'] = new CharField(array("canAdd"=>false,"readonlyOnUpdate"=>true));
        $this->fields['warehouse_To'] = new ForeignKey(array("modelName"=>"warehouse","displayName"=>"Destination","helpText"=>"Select the location where you will deliver the items"));
        $this->fields['approved_by'] = new ForeignKey(array("modelName"=>"user","canAdd"=>false,"readonlyOnUpdate"=>true));
        $this->fields['approved_date'] = new DateTimeField(array("hidden"=>true));
        $this->fields['is_approved'] = new BooleanField(array("value"=>false,"hidden"=>true));
        $this->fields['is_returned'] = new BooleanField(array("value"=>false,"canAdd"=>false,"canUpdate"=>false));

        parent::__construct($db);
    }

    public function __toString() {
        return $this->dr_no;
    }

    public function hasLines() {
        $qs = $this->delivery_receipt_line__set();
        return (count($qs) > 0);
    }

    public function isReturned() {
        $sql = "
        SELECT 
            COUNT(id) as `count` 
        FROM 
            delivery_receipt_line
        WHERE
            is_deleted = 0 AND delivery_receipt = {$this->id} AND is_returned = 0";

        $db = new DB();
        $db->connect();

        $result = false;
        $query = $db->query($sql);
        if ($query !== false) {
            $row = $db->fetch_assoc($query);
            $result = $row['count'] < 1;
        }

        $db->close();

        return $result;
    }

    public function setReturned() {
        $newValue = $this->isReturned();
        if ($this->is_returned != $newValue) {
            $this->is_returned = $newValue;
            $result = parent::save();
        }
    }

    public function save() {

        $prev = new delivery_receipt();
        $prev->get($this->id);
        if ($prev->approved_by != $this->approved_by) {
            // Approver was saved.
            $this->is_approved = ($this->approved_by > 0);
            $this->approved_date = date("Y-m-d H:i:s");
            return parent::save();
        }

        if ($this->hasLines()) {
            return array("status"=>"ERROR","msg"=>"You are not allowed to update delivery receipt. It already have items in it.");
        }

        // Construct dr_no
        if ($this->dr_no == "") {
            $this->dr_no = $this->getNextDrNo();
        }

        // Check if warehouse_To was changed
        if ($prev->warehouse_To != $this->warehouse_To) {
            $lines = $this->delivery_receipt_line__set();
            if ($result["status"] == "ERROR") {
                return $result;
            }
            foreach ($lines as $line) {
                $line->set_db($this->get_db());
                $result = $line->processInventory($line->warehouse_From, $this->warehouse_To);
                if ($result["status"] == "ERROR") {
                    return $result;
                }
                $result = $line->saveAsIs();
                if ($result["status"] == "ERROR") {
                    return $result;
                }
            }
        }

        return parent::save();
    }

    public function remove($remove = true) {
        if ($this->hasLines()) {
            return array(
                "status" => "ERROR",
                "msg" => "Cannot delete delivery receipt {$this->dr_no}! Kindly remove its lines first before proceeding."
            );
        } else {
            return parent::remove($remove);
        }
    }

    public function fetchOptions($selectedValue, $filter = null, $filterValue = null) {

        // Fetch options
        $list = $this->filter(
            $this->is_returned__exact('0')
        )->fetch();

        // Check if selected value is in options
        $inOptions = false;
        foreach ($list as $obj) {
            if ($selectedValue == $obj->id) {
                $inOptions = true;
                break;
            }
        }
        if (!$inOptions) {
            // Get selected value
            if ($selectedValue != '' || $selectedValue != 0) {
                $objName = get_class($this);
                $obj = new $objName;
                $obj->get($selectedValue);
                $list[] = $obj;
            }
        }
        return $list;
    }

    private function getNextDrNo() {
        $result = date("Ym") . "-001";

        // Get last dr_no
        $qs = $this->filter(
            $this->create_date__gte(date("Y-m-1")),
            $this->create_date__lte(date("Y-m-t"))
        )->order_by(
            "-dr_no"
        )->limit(
            0, 1
        )->fetch();

        // Make sure that there is a dr_no
        // else this is the start of dr_no
        if (count($qs) > 0) {
            $lastDrNo = $qs[0]->dr_no;

            $split = explode("-", $lastDrNo);
            // Make sure that the dr_no
            // was splitted properly
            if (count($split) == 2) {
                // Add 1 to last dr_no
                $result = date("Ym") . "-" . str_pad(($split[1] + 1), 3, "0", STR_PAD_LEFT);
            }
        }

        return $result;
    }

    public function download() {
        return 
        "<div class='divHref' class='report_pdf' data-href='../reports/receipt.php?dr&code={$this->dr_no}' target='_newtab'>
            <i class='fa fa-file-pdf-o' style='font-size: 35px; color: red;'></i>
        </div>";
    }

    public function approve() {
        $result = array("ERROR", "Unable to approve. Your session expired. Please login again.");
        if (isset($_SESSION['auth'])) {
            $this->approved_by = $_SESSION['auth']['userid'];
            if (is_numeric($this->approved_by)) {
                $result = $this->save();
                if ($result['status'] == "OK") {
                    $result['msg'] = "You approved a delivery receipt.";
                }
            }
        }
        return $result;
    }

    public function reject() {
        $lines = $this->delivery_receipt_line__set();
        if ($result["status"] == "ERROR") {
            return $result;
        }
        foreach ($lines as $line) {
            $line->set_db($this->get_db());
            $result = $line->remove();
            if ($result["status"] == "ERROR") {
                return $result;
            }
        }
        $result = $this->remove();
        if ($result['status'] == "OK") {
            $result['msg'] = "You rejected a delivery receipt.";
        }
        return $result;
    }
}

class delivery_receipt_line extends Model {
    public function __construct($db = null) {
        $this->fields['delivery_receipt'] = new ForeignKey(array("modelName"=>"delivery_receipt"));
        $this->fields['warehouse_From'] = new ForeignKey(array("modelName"=>"warehouse","readonlyOnUpdate"=>true,"displayName"=>"From Location"));
        $this->fields['item'] = new ForeignKey(array("modelName"=>"item","readonlyOnUpdate"=>true,"dependsOn"=>"warehouse_From","linkerObj"=>"warehouse_line","linkerDependeeField"=>"warehouse","linkerDependentField"=>"item"));
        $this->fields['available'] = new CharField(array("displayName"=>"Available Item","readonly"=>true,"canUpdate"=>false));
        $this->fields['quantity'] = new CharField(array("required"=>true,"readonlyOnUpdate"=>false));
        $this->fields['unit_of_measure'] = new ForeignKey(array("modelName"=>"unit_of_measure","readonlyOnUpdate"=>true,"dependsOn"=>"item","linkerObj"=>"item_unit_of_measure","linkerDependeeField"=>"item","linkerDependentField"=>"unit_of_measure"));
        $this->fields['remarks'] = new CharField();
        $this->fields['is_returned'] = new BooleanField(array("value"=>false,"canAdd"=>false,"canUpdate"=>false));
        $this->fields['transaction_Release'] = new ForeignKey(array("modelName"=>"transaction", "canAdd"=>false, "canUpdate"=>false, "required"=>false));
        $this->fields['transaction_Receive'] = new ForeignKey(array("modelName"=>"transaction", "canAdd"=>false, "canUpdate"=>false, "required"=>false));

        $this->m2m['delivery_receipt_line_serial_number'] = new M2MField(
            array(
                "objectName"=>"delivery_receipt_line",
                "otherObjectName"=>"serial_number",
                "displayName"=>"Serial Numbers",
                "helpText"=>"Choose the serial numbers of the selected item for this delivery receipt."
            )
        );

        parent::__construct($db);
    }

    public function __toString() {
        return 
        '('.$this->delivery_receipt__obj()->dr_no.') '.
        $this->quantity.' '.
        $this->unit_of_measure__obj()->abbreviation.' `'.
        $this->item__obj()->name.'`';
    }

    public function fetchOptions($selectedValue, $filter = null, $filterValue = null) {
        $list = $this->filter(
            $this->is_returned__exact('0')
        )->fetch();
        if ($selectedValue != '' || $selectedValue != 0) {
            $objName = get_class($this);
            $obj = new $objName;
            $obj->get($selectedValue);
            $list[] = $obj;
        }
        return $list;
    }

    public function getReturned() {
        $sql = "
        SELECT 
            SUM(quantity) as total 
        FROM 
            return_form_line
        WHERE 
            is_deleted = 0 AND delivery_receipt_line = {$this->id}
        GROUP BY 
            delivery_receipt_line";

        $db = new DB();
        $db->connect();

        $result = 0;
        $query = $db->query($sql);
        if ($query !== false) {
            $row = $db->fetch_assoc($query);
            $result = $row['total'];
        }

        $db->close();

        return $result;
    }

    public function setReturned() {
        $newValue = ($this->quantity <= $this->getReturned());

        if ($this->is_returned != $newValue) {
            $this->is_returned = $newValue;
            $result = parent::save();
            if ($result["status"] == "OK") {
                $this->delivery_receipt__obj()->setReturned();
            }
        }
    }

    public function doVoid() {
        if ($this->transaction_Release != 0) {
            $this->transaction_Release__obj()->status = "void";
            $result = $this->transaction_Release__obj()->save();
            if ($result["status"] == "ERROR") {
                return $result;
            }
        }
        if ($this->transaction_Receive != 0) {
            $this->transaction_Receive__obj()->status = "void";
            $result = $this->transaction_Receive__obj()->save();
            if ($result["status"] == "ERROR") {
                return $result;
            }
        }
        return array("status"=>"OK","msg"=>"");
    }

    public function doRelease($warehouseFrom) {

        $tran = new transaction($this->get_db());
        $tran->action = "released";
        $tran->warehouse = $warehouseFrom;
        $tran->status = "processed";
        $tran->remarks = $this->delivery_receipt__obj()->dr_no;
        $result = $tran->save();
        if ($result["status"] == "ERROR") {
            return $result;
        }

        $line = new transaction_line($this->get_db());
        $line->transaction = $tran->id;
        $line->item = $this->item;
        $line->quantity = $this->quantity;
        $line->unit_of_measure = $this->unit_of_measure;
        $line->remarks = $this->remarks;
        $result = $line->save();
        if ($result["status"] == "ERROR") {
            return $result;
        }

        $this->transaction_Release = $tran->id;
        return array("status"=>"OK","msg"=>"");
    }

    public function doReceive($warehouseTo) {
        
        $tran = new transaction($this->get_db());
        $tran->action = "received";
        $tran->warehouse = $warehouseTo;
        $tran->status = "processed";
        $tran->remarks = $this->delivery_receipt__obj()->dr_no;
        $result = $tran->save();
        if ($result["status"] == "ERROR") {
            return $result;
        }

        $line = new transaction_line($this->get_db());
        $line->transaction = $tran->id;
        $line->item = $this->item;
        $line->quantity = $this->quantity;
        $line->unit_of_measure = $this->unit_of_measure;
        $line->remarks = $this->remarks;
        $result = $line->save();
        if ($result["status"] == "ERROR") {
            return $result;
        }

        $this->transaction_Receive = $tran->id;
        return array("status"=>"OK","msg"=>"");
    }

    public function processInventory($warehouseFrom, $warehouseTo) {
        // Do void transaction if already transacted.
        $result = $this->doVoid();
        if ($result["status"] == "ERROR") {
            return $result;
        }
        
        // Do release transaction.
        $result = $this->doRelease($warehouseFrom);
        if ($result["status"] == "ERROR") {
            return $result;
        }

        // Do receive transaction.
        $result = $this->doReceive($warehouseTo);
        if ($result["status"] == "ERROR") {
            return $result;
        }
        return array("status"=>"OK","msg"=>"");
    }

    public function doRemoveSerialNumbers($warehouseFrom) {
        $result = array("status"=>"OK", "msg"=>"");

        $qs = $this->delivery_receipt_line_serial_number__set();
        foreach ($qs as $drlsn) {
            $sn = $drlsn->serial_number__obj();
            $sn->warehouse = $warehouseFrom;
            $result = $sn->save();
            if ($result['status'] == 'OK') {
                $result = $drlsn->remove();
            }
        }

        return $result;
    }

    public function saveAsIs() {
        return parent::save();
    }

    public function save() {

        $this->available = "";

        $result = $this->doRemoveSerialNumbers($this->warehouse_From);
        if ($result["status"] == "ERROR") {
            return $result;
        }

        $result = $this->processInventory($this->warehouse_From, $this->delivery_receipt__obj()->warehouse_To);
        if ($result["status"] == "ERROR") {
            return $result;
        }

        return parent::save();
    }

    public function remove($remove = true) {

        if ($this->is_returned) {
            return array("status"=>"ERROR","msg"=>"Cannot remove! All items were already returned.");
        }
        if ($this->getReturned() > 0) {
            return array("status"=>"ERROR","msg"=>"Cannot remove! Some items were already returned.");
        }
        
        $result = $this->doRemoveSerialNumbers($this->warehouse_From);
        if ($result["status"] == "ERROR") {
            return $result;
        }

        $result = $this->doVoid();
        if ($result["status"] == "ERROR") {
            return $result;
        }

        return parent::remove();
    }

    public function delete() {

        if ($this->is_returned) {
            return array("status"=>"ERROR","msg"=>"Cannot remove! All items were already returned.");
        }
        if ($this->getReturned() > 0) {
            return array("status"=>"ERROR","msg"=>"Cannot remove! Some items were already returned.");
        }

        $result = $this->doRemoveSerialNumbers($this->warehouse_From);
        if ($result["status"] == "ERROR") {
            return $result;
        }

        $result = $this->doVoid();
        if ($result["status"] == "ERROR") {
            return $result;
        }

        return parent::delete();
    }
}

class delivery_receipt_line_serial_number extends Model {
    public function __construct($db = null) {
        $this->fields['delivery_receipt_line'] = new ForeignKey(array("modelName"=>"delivery_receipt_line"));
        $this->fields['serial_number'] = new ForeignKey(array("modelName"=>"serial_number"));
        $this->fields['is_returned'] = new BooleanField(array("value"=>false,"readonly"=>true));

        parent::__construct($db);
    }

    public function verbose() {
        return "DR Line Serial Numbers";
    }

    public function setAsReturned($value) {
        $this->is_returned = $value;
        return parent::save();
    }

    public function save() {

        $warehouse_id = $this->delivery_receipt_line__obj()->delivery_receipt__obj()->warehouse_To;

        if ($warehouse_id != 0) {
            $sn = $this->serial_number__obj();
            $sn->warehouse = $warehouse_id;
            $result = $sn->save();
            if ($result["status"] == "ERROR") {
                return $result;
            }
            return parent::save();
        }
        
        return array("status" => "ERROR", "Cannot save serial number. Warehouse not provided");
    }
}

class return_form extends Model {
    public function __construct($db = null) {
        $this->fields['return_no'] = new CharField(array("readonly"=>true));
        $this->fields['delivery_receipt'] = new ForeignKey(array("modelName"=>"delivery_receipt", "displayName"=>"Dr No"));
        $this->fields['warehouse_To'] = new ForeignKey(array(
            "modelName"=>"warehouse",
            "displayName"=>"Destination",
            "helpText"=>"Select the location where you will return the items",
            "dependsOn"=>"delivery_receipt",
            "linkerObj"=>"warehouse",
            "linkerDependeeField"=>"delivery_receipt",
            "linkerDependentField"=>"warehouse"
        ));
        $this->fields['approved_by'] = new ForeignKey(array("modelName"=>"user"));

        parent::__construct($db);
    }

    public function download() {
        return 
        "<div class='divHref' class='report_pdf' data-href='../reports/receipt.php?return&code={$this->return_no}' target='_newtab'>
            <i class='fa fa-file-pdf-o' style='font-size: 35px; color: red;'></i>
        </div>";
    }

    public function __toString() {
        return $this->return_no;
    }

    public function hasLines() {
        $qs = $this->return_form_line__set();
        return (count($qs) > 0);
    }

    public function save() {

        if ($this->hasLines()) {
            return array("status"=>"ERROR","msg"=>"You are not allowed to update return form. It already have items in it.");
        }

        // Construct return_no
        if ($this->return_no == "") {
            $this->return_no = $this->getNextReturnNo();
        }

        
        $prev = new return_form();
        $prev->get($this->id);

        // Check if delivery_receipt was changed
        if ($prev->delivery_receipt != $this->delivery_receipt && $this->return_no != "") {
            $this->return_no = $this->getNextReturnNo();
        }

        // Check if warehouse_To was changed
        if ($prev->warehouse_To != $this->warehouse_To) {
            $lines = $this->return_form_line__set();
            if ($result["status"] == "ERROR") {
                return $result;
            }
            foreach ($lines as $line) {
                $line->set_db($this->get_db());
                $result = $line->processInventory($line->warehouse_From, $this->warehouse_To);
                if ($result["status"] == "ERROR") {
                    return $result;
                }
                $result = $line->saveAsIs();
                if ($result["status"] == "ERROR") {
                    return $result;
                }
            }
        }

        return parent::save();
    }

    public function remove($remove = true) {
        if ($this->hasLines()) {
            return array(
                "status" => "ERROR",
                "msg" => "Cannot delete return form {$this->return_no}! Kindly remove its lines first before proceeding."
            );
        } else {
            return parent::remove($remove);
        }
    }

    private function getNextReturnNo() {
        $result = $this->delivery_receipt__obj()->dr_no . "-01";

        // Get last return_no
        $qs = $this->filter(
            $this->delivery_receipt__exact($this->delivery_receipt),
            $this->create_date__gte(date("Y-m-1")),
            $this->create_date__lte(date("Y-m-t"))
        )->order_by(
            "-return_no"
        )->limit(
            0, 1
        )->fetch();

        // Make sure that there is a return_no
        // else this is the start of return_no
        if (count($qs) > 0) {
            $lastReturnNo = $qs[0]->return_no;

            $split = explode("-", $lastReturnNo);
            // Make sure that the return_no
            // was splitted properly
            if (count($split) == 3) {
                // Add 1 to last dr_no
                $result = $this->delivery_receipt__obj()->dr_no . "-" . str_pad(($split[2] + 1), 2, "0", STR_PAD_LEFT);
            }
        }

        return $result;
    }
}

class return_form_line extends Model {
    public function __construct($db = null) {
        $this->fields['return_form'] = new ForeignKey(array("modelName"=>"return_form"));
        $this->fields['delivery_receipt_line'] = new ForeignKey(array("modelName"=>"delivery_receipt_line","dependsOn"=>"return_form","linkerObj"=>"delivery_receipt_line","linkerDependeeField"=>"return_form__delivery_receipt","linkerDependentField"=>"delivery_receipt_line"));
        $this->fields['available'] = new CharField(array("displayName"=>"Remaining Item to Return","readonly"=>true,"canUpdate"=>false));
        $this->fields['quantity'] = new CharField(array("required"=>true));
        $this->fields['remarks'] = new CharField();
        $this->fields['transaction_Release'] = new ForeignKey(array("modelName"=>"transaction", "canAdd"=>false, "canUpdate"=>false, "required"=>false));
        $this->fields['transaction_Receive'] = new ForeignKey(array("modelName"=>"transaction", "canAdd"=>false, "canUpdate"=>false, "required"=>false));

        $this->m2m['return_form_line_serial_number'] = new M2MField(
            array(
                "objectName"=>"return_form_line",
                "otherObjectName"=>"serial_number",
                "displayName"=>"Serial Numbers",
                "helpText"=>"Choose the serial numbers of the selected item that you want to return."
            )
        );

        parent::__construct($db);
    }
    
    public function item() {
        return $this->delivery_receipt_line__obj()->item__obj()->name;
    }

    public function unit_of_measure() {
        return $this->delivery_receipt_line__obj()->unit_of_measure__obj()->name;
    }

    public function __toString() {
        return 
        '('.$this->return_form__obj()->return_no.') '.
        $this->quantity.' '.
        $this->delivery_receipt_line__obj()->unit_of_measure__obj()->abbreviation.' `'.
        $this->delivery_receipt_line__obj()->item__obj()->name.'`';
    }

    public function doVoid() {
        
        if ($this->transaction_Release != 0) {
            $this->transaction_Release__obj()->status = "void";
            $result = $this->transaction_Release__obj()->save();
            if ($result["status"] == "ERROR") {
                return $result;
            }
        }
        if ($this->transaction_Receive != 0) {
            $this->transaction_Receive__obj()->status = "void";
            $result = $this->transaction_Receive__obj()->save();
            if ($result["status"] == "ERROR") {
                return $result;
            }
        }

        return array("status"=>"OK","msg"=>"");
    }

    public function doRelease($warehouseFrom) {

        $tran = new transaction($this->get_db());
        $tran->action = "released";
        $tran->warehouse = $warehouseFrom;
        $tran->status = "processed";
        $tran->remarks = $this->return_form__obj()->return_no;
        $result = $tran->save();
        if ($result["status"] == "ERROR") {
            return $result;
        }

        $line = new transaction_line($this->get_db());
        $line->transaction = $tran->id;
        $line->item = $this->delivery_receipt_line__obj()->item;
        $line->quantity = $this->quantity;
        $line->unit_of_measure = $this->delivery_receipt_line__obj()->unit_of_measure;
        $line->remarks = $this->remarks;
        $result = $line->save();
        if ($result["status"] == "ERROR") {
            return $result;
        }

        $this->transaction_Release = $tran->id;
        return array("status"=>"OK","msg"=>"");
    }

    public function doReceive($warehouseTo) {
        
        $tran = new transaction($this->get_db());
        $tran->action = "received";
        $tran->warehouse = $warehouseTo;
        $tran->status = "processed";
        $tran->remarks = $this->return_form__obj()->return_no;
        $result = $tran->save();
        if ($result["status"] == "ERROR") {
            return $result;
        }

        $line = new transaction_line($this->get_db());
        $line->transaction = $tran->id;
        $line->item = $this->delivery_receipt_line__obj()->item;
        $line->quantity = $this->quantity;
        $line->unit_of_measure = $this->delivery_receipt_line__obj()->unit_of_measure;
        $line->remarks = $this->remarks;
        $result = $line->save();
        if ($result["status"] == "ERROR") {
            return $result;
        }

        $this->transaction_Receive = $tran->id;
        return array("status"=>"OK","msg"=>"");
    }

    public function processInventory($warehouseFrom, $warehouseTo) {
        // Do void transaction if already transacted.
        $result = $this->doVoid();
        if ($result["status"] == "ERROR") {
            return $result;
        }
        
        // Do release transaction.
        $result = $this->doRelease($warehouseFrom);
        if ($result["status"] == "ERROR") {
            return $result;
        }

        // Do receive transaction.
        $result = $this->doReceive($warehouseTo);
        if ($result["status"] == "ERROR") {
            return $result;
        }
        return array("status"=>"OK","msg"=>"");
    }

    public function doRemoveSerialNumbers($warehouseFrom) {
        $result = array("status"=>"OK", "msg"=>"");

        $qs = $this->return_form_line_serial_number__set();
        foreach ($qs as $rflsn) {
            $sn = $rflsn->serial_number__obj();
            $sn->warehouse = $warehouseFrom;
            $result = $sn->save();
            if ($result['status'] == 'OK') {
                // rollback is_returned to false
                $result = $rflsn->tagDeliveryReceiptLineSerialNumber(false);

                if ($result['status'] == 'OK') {
                    $result = $rflsn->remove();
                }
            }
        }

        return $result;
    }

    public function saveAsIs() {
        return parent::save();
    }

    public function save() {

        $this->available = "";
        
        $warehouseFrom = $this->delivery_receipt_line__obj()->delivery_receipt__obj()->warehouse_To;

        $result = $this->doRemoveSerialNumbers($warehouseFrom);
        if ($result["status"] == "ERROR") {
            return $result;
        }

        $result = $this->processInventory($warehouseFrom, $this->return_form__obj()->warehouse_To);
        if ($result["status"] == "ERROR") {
            return $result;
        }

        $result = parent::save();

        if ($result["status"] == "OK") {
            $this->delivery_receipt_line__obj()->setReturned();
        }

        return $result;
    }

    public function remove($remove = true) {
        
        $warehouseFrom = $this->delivery_receipt_line__obj()->delivery_receipt__obj()->warehouse_To;
        
        $result = $this->doRemoveSerialNumbers($warehouseFrom);
        if ($result["status"] == "ERROR") {
            return $result;
        }

        $result = $this->doVoid();
        if ($result["status"] == "ERROR") {
            return $result;
        }

        $result = parent::remove();

        if ($result["status"] == "OK") {
            $this->delivery_receipt_line__obj()->setReturned();
        }

        return $result;
    }

    public function delete() {

        $warehouseFrom = $this->delivery_receipt_line__obj()->delivery_receipt__obj()->warehouse_To;
        
        $result = $this->doRemoveSerialNumbers($warehouseFrom);
        if ($result["status"] == "ERROR") {
            return $result;
        }

        $result = $this->doVoid();
        if ($result["status"] == "ERROR") {
            return $result;
        }

        $result = parent::delete();
        
        if ($result["status"] == "OK") {
            $this->delivery_receipt_line__obj()->setReturned();
        }
    }
}

class return_form_line_serial_number extends Model {
    public function __construct($db = null) {
        $this->fields['return_form_line'] = new ForeignKey(array("modelName"=>"return_form_line"));
        $this->fields['serial_number'] = new ForeignKey(array("modelName"=>"serial_number"));

        parent::__construct($db);
    }

    public function verbose() {
        return "RF Line Serial Numbers";
    }

    // Sets whether delivery receipt line serial number object
    // is returned or not.
    public function tagDeliveryReceiptLineSerialNumber($value) {

        $serial_number = $this->serial_number;

        // Get delivery_receipt_serial_number object
        $drl_id = $this->return_form_line__obj()->delivery_receipt_line;

        $drlsn = new delivery_receipt_line_serial_number();
        $qs = $drlsn->filter(
            $drlsn->delivery_receipt_line__exact($drl_id),
            $drlsn->serial_number__exact($serial_number)
        )->fetch();

        if (count($qs) > 0) {
            // Tag delivery_receipt_serial_number as returned
            $drlsn = $qs[0];
            $result = $drlsn->setAsReturned($value);
            if ($result["status"] == "ERROR") {
                return $result;
            }

            return array("status"=>"OK","msg"=>"");
        } else {
            return array("status"=>"ERROR","msg"=>"The `delivery_receipt_serial_number` object you are trying to return does not exist.");
        }
    }

    public function save() {
        
        $warehouse_id = $this->return_form_line__obj()->return_form__obj()->warehouse_To;

        if ($warehouse_id != 0) {

            $result = $this->tagDeliveryReceiptLineSerialNumber(true);
            if ($result["status"] == "ERROR") {
                return $result;
            }

            $sn = $this->serial_number__obj();
            $sn->warehouse = $warehouse_id;
            $result = $sn->save();
            if ($result["status"] == "ERROR") {
                return $result;
            }

            return parent::save();
        }
        
        return array("status" => "ERROR", "Cannot save serial number. Warehouse not provided");
    }
}

?>