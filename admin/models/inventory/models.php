<?php

class inventory {

    private function __convert_to_smallest($item, $qty, $uomFrom) {

        $uomSmallest = $item->smallest_unit();
        if (!($uomSmallest instanceof unit_of_measure)) {
            $result = array(
                "status" => "ERROR",
                "msg" => "Smallest unit of measure is not set for {$item->name}",
            );
            return $result;
        }

        if ($uomFrom->id == $uomSmallest->id) {
            $result = array(
                "status" => "OK",
                "msg" => "",
                "result" => $qty,
            );
            return $result;
        }

        $itemUoms = $item->item_unit_of_measure__set();
        if (count($itemUoms) < 1) {
            $result = array(
                "status" => "ERROR",
                "msg" => "{$item->name} does not have unit of measure. Kindly assign atleast one."
            );
            return $result;
        }
        $ids = "";
        foreach($itemUoms as $itemUom) {
            $ids .= "{$itemUom->unit_of_measure},";
        }
        $ids = trim($ids,",");

        $uomConv = new unit_of_measure_conversion();
        $uomConvs = $uomConv->filter(
            $uomConv->unit_of_measure_from__exact($uomFrom->id),
            $uomConv->unit_of_measure_to__in($ids)
        )->fetch();
        if (count($uomConvs) < 1) {
            $result = array(
                "status" => "ERROR",
                "msg" => "Conversion from {$uomFrom->name} to {$uomSmallest->name} is not available."
            );
            return $result;
        }

        $uomConv = $uomConvs[0];

        $divider = $uomConv->divider;

        $multiplier = 1;
        if ($uomConv->unit_of_measure_to == $uomSmallest->id) {
            $multiplier = $uomConv->multiplier;
        } else {
            $uom = new unit_of_measure();
            $uom->get($uomConv->unit_of_measure_to);
            $result = $this->__convert_to_smallest($item, $uomConv->multiplier, $uom);
            if ($result["status"] == "OK") {
                $multiplier = $result["result"];
            } else {
                return $result;
            }
        }

        $result = array(
            "status" => "OK",
            "msg" => "",
            "result" => $qty * $multiplier / $divider
        );

        return $result;
    }

    private function __convert($item, $qty, $uomFrom, $uomTo) {

        $uomConvFrom = new unit_of_measure_conversion();
        $uomConvFrom->divider = 1;
        $result = $this->__convert_to_smallest($item, 1, $uomFrom);
        if ($result["status"] == "OK") {
            $uomConvFrom->multiplier = $result["result"];
        } else {
            return $result;
        }

        $uomSmallest = $item->smallest_unit();

        $uomConvTo = new unit_of_measure_conversion();
        if ($uomTo->id == $uomSmallest->id) {
            $uomConvTo->divider = 1;
            $uomConvTo->multiplier = 1;
        } else {
            $uomConvTo = new unit_of_measure_conversion();
            $uomConvTo->divider = 1;
            $result = $this->__convert_to_smallest($item, 1, $uomTo);
            if ($result["status"] == "OK") {
                $uomConvTo->multiplier = $result["result"];
            } else {
                return $result;
            }
        }

        $multiplier = $uomConvFrom->multiplier;

        $divider = 1;

        if ($uomSmallest->id != $uomTo->id) {
            $divider = $uomConvTo->multiplier;
        }

        $result = array(
            "status" => "OK",
            "msg" => "",
            "result" => ($qty * $multiplier) / $divider
        );

        return $result;
    }

    public function convert($itemID, $qty, $fromUnitID, $toUnitID) {

        $result = array(
            "status" => "OK",
            "msg" => "",
            "result" => $qty
        );

        if ($fromUnitID == $toUnitID) {
            return $result;
        }

        $item = new item();
        $item->get($itemID);

        // verify if the given $fromUnitID is unit of the given item
        $uomFrom = new unit_of_measure();
        $uomFrom->get($fromUnitID);
        $itemUomFrom = new item_unit_of_measure();
        $qs = $itemUomFrom->filter($itemUomFrom->item__exact($itemID),$itemUomFrom->unit_of_measure__exact($fromUnitID))->fetch();
        if (count($qs) < 1) {
            $result = array(
                "status" => "ERROR",
                "msg" => "{$uomFrom->name} is not a unit of measure of {$item->name}"
            );
            return $result;
        }

        // verify if the given $toUnitID is unit of the given item
        $uomTo = new unit_of_measure();
        $uomTo->get($toUnitID);
        $itemUomTo = new item_unit_of_measure();
        $qs = $itemUomTo->filter($itemUomTo->item__exact($itemID),$itemUomTo->unit_of_measure__exact($toUnitID))->fetch();
        if (count($qs) < 1) {
            $result = array(
                "status" => "ERROR",
                "msg" => "{$uomTo->name} is not a unit of measure of {$item->name}"
            );
            return $result;
        }

        $result = $this->__convert($item, $qty, $uomFrom, $uomTo);

        return $result;
    }

}

class unit_of_measure_type extends Model {

    public function __construct($db = null) {
        $this->fields['name'] = new CharField();
        $this->fields['description'] = new CharField();

        parent::__construct($db);
    }

    public function __toString() {
        return $this->name;
    }
}

class unit_of_measure extends Model {

    public function __construct($db = null) {
        $this->fields['abbreviation'] = new CharField();
        $this->fields['name'] = new CharField();
        $this->fields['description'] = new CharField();
        $this->fields['unit_of_measure_type'] = new ForeignKey(array("modelName"=>"unit_of_measure_type"));

        parent::__construct($db);
    }

    public function __toString() {
        return $this->abbreviation;
    }
}

class unit_of_measure_conversion extends Model {

    public function __construct($db = null) {
        $this->fields['divider'] = new FloatField(array("value"=>1,"readonly"=>true,"displayName"=>"From"));
        $this->fields['unit_of_measure_from'] = new ForeignKey(array("modelName"=>"unit_of_measure","displayName"=>"Unit of Measure"));
        $this->fields['multiplier'] = new FloatField(array("displayName"=>"To"));
        $this->fields['unit_of_measure_to'] = new ForeignKey(array("modelName"=>"unit_of_measure","displayName"=>"Unit of Measure"));
        $this->fields['adder'] = new FloatField(array("hidden"=>true,"canAdd"=>false,"canUpdate"=>false));
        
        parent::__construct($db);
    }

    public function __toString() {
        return "";
    }

    public function verbose() {
        return "Unit Conversion";
    }
}

class inventory_category extends Model {

    public function __construct($db = null) {
        $this->fields['name'] = new CharField(array("required"=>true));
        $this->fields['description'] = new CharField();

        parent::__construct($db);
    }

    public function __toString() {
        return $this->name;
    }

    public function verbose() {
        return "Item Category";
    }

    public function save() {
        // Previous data
        $prev = new inventory_category();
        $prev->get($this->id);

        $this->name = strtoupper($this->name);

        // Check if category already exists
        $count = $this->filter($this->name__exact($this->name))->count();
        if ($prev->name != $this->name && $count > 0) {
            return array("status"=>"ERROR", "msg"=>"Unable to save. Category already exists!");
        }

        return parent::save();
    }

    public function remove($remove = true) {

        // Remove only if not linked to item
        $item = new item();
        $qs = $item->filter($item->inventory_category__exact($this->id))->count();
        if ($count > 0) {
            return array("status"=>"ERROR", "msg"=>"Unable to delete. Category already used in Items!");
        }

        return parent::remove($remove);
    }
}

class item extends Model {

    public function __construct($db = null) {
        $this->fields['image'] = new ImageField(array("uploadTo"=>"item","helpText"=>"Kindly upload an image of the item"));
        $this->fields['inventory_category'] = new ForeignKey(array("modelName"=>"inventory_category","displayName"=>"Category"));
        $this->fields['asset_code'] = new CharField(array("readonly"=>true));
        $this->fields['name'] = new CharField();
        $this->fields['description'] = new CharField();
        $this->fields['quantity'] = new FloatField(array("required"=>true,"canUpdate"=>false,"helpText"=>"Kindly enter the initial count for this new item."));
        $this->fields['unit_of_measure'] = new ForeignKey(array("modelName"=>"unit_of_measure","canUpdate"=>false,"helpText"=>"Kindly choose the unit of measure for this item. Preferebly the smallest unit of measure that you will use."));
        $this->fields['warehouse'] = new ForeignKey(array("modelName"=>"warehouse","canUpdate"=>false,"helpText"=>"Kindly choose the warehouse where this new item will be stored."));
        $this->fields['is_returnable'] = new BooleanField(array("value"=>true,"hidden"=>true));

        parent::__construct($db);
    }

    public function __toString() {
        return $this->name;
    }

    public function smallest_unit() {
        $iuom = new item_unit_of_measure();
        $qs = $iuom->filter($iuom->item__exact($this->id),$iuom->is_smallest__exact(1))->fetch();
        if (count($qs) > 0) {
            $uom = new unit_of_measure();
            $uom->get($qs[0]->unit_of_measure);
            return $uom;
        }
        return null;
    }

    public function has_component() {
        $comp = new item_component();
        $count = $comp->filter($comp->item__exact($this->id))->count();
        return ($count > 0);
    }

    public function is_available($warehouseID, $qty, $uomID) {
        return true;
    }

    public function getNextAssetCode() {
        $result = date("Y") . "-00001";

        // Get last asset code
        $qs = $this->filter()->order_by(
            "-asset_code"
        )->limit(
            0, 1
        )->fetch();

        // Make sure that there is an asset_code
        // else this is the start of asset_code
        if (count($qs) > 0) {
            $lastAssetCode = $qs[0]->asset_code;

            $split = explode("-", $lastAssetCode);
            // Make sure that the asset_code
            // was splitted properly
            if (count($split) == 2) {
                // Add 1 to last asset_code
                $result = date("Y") . "-" . str_pad(($split[1] + 1), 5, "0", STR_PAD_LEFT);
            }
        }

        return $result;
    }

    public function save() {

        $this->name = strtoupper($this->name);

        $isNew = ($this->id == 0);

        if ($isNew) {

            if ($this->asset_code == "") {
                $this->asset_code = $this->getNextAssetCode();
                $this->asset_code = strtoupper($this->asset_code);
            }

            $resultThis = parent::save();
            if ($resultThis["status"] == "ERROR") {
                return $resultThis;
            }
            $this->db->commit();

            // Save default unit of measure based on selected unit
            $iuom = new item_unit_of_measure($this->get_db());
            $iuom->item = $this->id;
            $iuom->unit_of_measure = $this->unit_of_measure;
            $iuom->is_smallest = true;
            $result = $iuom->save();
            if ($result["status"] == "ERROR") {
                return $result;
            }
            
            // Do receive transaction
            $tran = new transaction($this->get_db());
            $tran->action = "received";
            $tran->warehouse = $this->warehouse;
            $tran->status = "processed";
            $tran->remarks = "Initial";
            $result = $tran->save();
            if ($result["status"] == "ERROR") {
                return $result;
            }
            
            $line = new transaction_line($this->get_db());
            $line->transaction = $tran->id;
            $line->item = $this->id;
            $line->quantity = $this->quantity;
            $line->unit_of_measure = $this->unit_of_measure;
            $line->remarks = "Initial";
            $result = $line->save();
            if ($result["status"] == "ERROR") {
                return $result;
            }
            
            return $resultThis;

        } else {
            
            return parent::save();
        }
    }
}

class item_unit_of_measure extends Model {

    public function __construct($db = null) {
        $this->fields['item'] = new ForeignKey(array("modelName"=>"item"));
        $this->fields['unit_of_measure'] = new ForeignKey(array("modelName"=>"unit_of_measure"));
        $this->fields['is_smallest'] = new BooleanField();

        parent::__construct($db);
    }

    public function __toString() {
        return $this->name;
    }
}

class item_component extends Model {

    public function __construct($db = null) {
        $this->fields['item'] = new ForeignKey(array("modelName"=>"item"));
        $this->fields['component'] = new ForeignKey(array("modelName"=>"item","modelParent"=>false));
        $this->fields['quantity'] = new FloatField();
        $this->fields['unit_of_measure'] = new ForeignKey(array("modelName"=>"unit_of_measure","dependsOn"=>"component","linkerObj"=>"item_unit_of_measure","linkerDependeeField"=>"item","linkerDependentField"=>"unit_of_measure"));

        parent::__construct($db);
    }

    public function __toString() {
        $component = new item();
        $component->get($this->component);
        return $component->name;
    }
}

class warehouse extends Model {

    public $warehouse_type_choices = array(
        "physical" => "Physical",
        "transit" => "Transit",
        "no_return" => "No Return",
    );

    public function __construct($db = null) {
        $this->fields['code'] = new CharField(array("required"=>true,"helpText"=>"Assign your warehouse a code name. Example is `WRH` which stands for Warehouse"));
        $this->fields['name'] = new CharField(array("required"=>true));
        $this->fields['description'] = new CharField();
        $this->fields['warehouse_type'] = new ChoicesField(array("required"=>true,"choices"=>$this->warehouse_type_choices,"value"=>"physical"));

        parent::__construct($db);
    }

    public function __toString() {
        return $this->name;
    }

    public function save() {
        $this->code = strtoupper(str_replace(" ","_",$this->code));
        $this->name = strtoupper($this->name);

        return parent::save();
    }
}

class warehouse_line extends Model {

    public function __construct($db = null) {
        $this->fields['item'] = new ForeignKey(array("modelName"=>"item"));
        $this->fields['warehouse'] = new ForeignKey(array("modelName"=>"warehouse"));
        $this->fields['quantity'] = new FloatField();
        $this->fields['unit_price'] = new FloatField();
        $this->fields['expiration_date'] = new DateField();

        parent::__construct($db);
    }

    public function __toString() {
        return "";
    }

    public function get_queryset($queryParams, $start = null, $limit = null) {
        $adminName = get_class($this) . "_admin";
        $admin = new $adminName;
        $filterParams = array();

        if (isset($queryParams["q"]) && $queryParams["q"] != "") {
            $searchFieldsParams = "";
            foreach ($admin->searchFields as $field) {
                $fieldFuncName = $field . "__contains";
                $value = "'" . $queryParams["q"] . "'";
                $searchFieldsParams .= '$this->'.$fieldFuncName.'(' . $value . '),';
            }
            $searchFieldsParams = trim($searchFieldsParams,",");
            $filterParams[] = '_OR(' . $searchFieldsParams . ')';
        }

        unset($queryParams["q"]);
        foreach($queryParams as $key => $value) {
            if ($value != "") {
                $fieldFuncName = $key . "__exact";
                $filterParams[] = '$this->'.$fieldFuncName.'(' . "'" . $value . "'" . ')';
            }
        }

        $params = "";
        foreach ($filterParams as $value) {
            $params .= "{$value},";
        }
        $params = trim($params,",");

        eval('$result = $this->filter('.$params.')->order_by("item")->limit('.$start.','.$limit.')->fetch();');
        return $result;
    }
}

class serial_number extends Model {

    public function __construct($db = null) {
        $this->fields['item'] = new ForeignKey(array("modelName"=>"item"));
        $this->fields['serial_number'] = new CharField(array("readonlyOnUpdate"=>true));
        $this->fields['warehouse'] = new ForeignKey(array("modelName"=>"warehouse","canAdd"=>false,"canUpdate"=>true,"required"=>false));

        parent::__construct($db);
    }

    public function __toString() {
        return $this->serial_number;
    }

    public function save() {

        // Check if serial number wants to be changed
        $prev = new serial_number();
        $prev->get($this->id);
        if ($prev->serial_number != $this->serial_number) {
            // Check if serial number already exists
            $count = $this->filter(
                $this->serial_number__exact($this->serial_number),
                $this->item__exact($this->item)
            )->count();
            if ($count > 0) {
                return array("status"=>"ERROR", "msg"=>"Unable to add/update. Serial number already exists.");
            }
        }

        if ($this->id == 0) {

            // Get quantity of items
            $myItems = new my_items();
            $qty = $myItems->GetItemOverAllQuantity($this->item);

            // Get the count of serial numbers
            $count = $this->filter($this->item__exact($this->item))->count();

            // If serial numbers is less than count of items
            // then save
            if ($count < $qty) {
                $this->warehouse = $this->item__obj()->warehouse;
                return parent::save();
            } else {
                return array("status"=>"ERROR", "msg"=>"Unable to add more serial number. You only have {$qty} `".$this->item__obj()->name."` in your inventory.");
            }
        } else {
            return parent::save();
        }
    }

    public function remove($remove = true) {
        if ($this->item__obj()->warehouse == $this->warehouse) {
            return parent::remove($remove);
        } else {
            $wh = $this->item__obj()->warehouse__obj()->name;
            return array("status"=>"ERROR","msg"=>"You can only delete serial number that is located in `{$wh}`");
        }
    }
}

class transaction extends Model {

    public $action_choices = array(
        "received" => "Received",
        "released" => "Released"
    );

    public $status_choices = array(
        "pending" => "Pending",
        "processed" => "Processed",
        "void" => "Void"
    );

    public $increase_choices = array(
        "none" => "none",
        "increase" => "increase",
        "decrease" => "decrease"
    );

    public function __construct($db = null) {
        $this->fields['tracking_number'] = new CharField(array("readonly"=>true,"helpText"=>"This tracking number is system generated"));
        $this->fields['action'] = new ChoicesField(array("choices"=>$this->action_choices,"required"=>true,"helpText"=>"Select `Received` to add an item to your warehouse otherwise `Released` to deduct an item from your warehouse")); // choices: received, released
        $this->fields['warehouse'] = new ForeignKey(array("modelName"=>"warehouse"));
        $this->fields['status'] = new ChoicesField(array("choices"=>$this->status_choices,"value"=>"processed","required"=>true)); // choices: pending, processed, void
        $this->fields['remarks'] = new TextField();
        $this->fields['external_tracking_number'] = new CharField(array("hidden"=>true,"canAdd"=>false,"canUpdate"=>false));
        $this->fields['estimated_processing_date'] = new DateField(array("hidden"=>true,"canAdd"=>false,"canUpdate"=>false));
        $this->fields['process_date'] = new DateTimeField(array("readonly"=>true,"canAdd"=>false));
        $this->fields['increase'] = new CharField(array("choices"=>$this->increase_choices,"value"=>"none","hidden"=>true,"canAdd"=>false,"canUpdate"=>false)); // choices: none, increase, decrease

        parent::__construct($db);
    }

    public function __toString() {
        return $this->tracking_number;
    }

    public function save() {
        $db = $this->get_db();

        if ($this->id == 0) {
            $this->increase = "none";
            if ($this->status == "processed") {
                if ($this->action == "received") {
                    $this->increase = "increase";
                } elseif ($this->action == "released") {
                    $this->increase = "decrease";
                }
            }

            // save to get the id and construct the tracking number
            parent::save();
            $warehouse = new warehouse();
            $warehouse->get($this->warehouse);
            $this->tracking_number = $warehouse->code . '-' . sprintf("%09d",$this->id);
        } else {
            $transaction = new transaction($db);
            $transaction->get($this->id);

            $this->increase = "none";
            if ($transaction->status != "processed" && $this->status == "processed") {

                // Update if the transaction was changed from not processed to processed
                if ($this->action == "received") {
                    $this->increase = "increase";
                } elseif ($this->action == "released") {
                    $this->increase = "decrease";
                }
                $result = $this->process_lines();
                if ($result["status"] == "ERROR") {
                    return $result;
                }

            } elseif ($transaction->status == "processed" && $this->status != "processed") {

                // Undo if transaction is already processed but changed to not processed
                if ($this->action == "received") {
                    $this->increase = "decrease";
                } elseif ($this->action == "released") {
                    $this->increase = "increase";
                }
                $result = $this->process_lines();
                if ($result["status"] == "ERROR") {
                    return $result;
                }

            }
        }

        if ($this->increase != "none") {
            $this->process_date = date("Y-m-d H:i:s");
        }

        return parent::save();
    }

    public function process_lines() {

        $result = array(
            "status" => "OK",
            "msg" => "Transaction lines successfully processed"
        );
        $lines = $this->transaction_line__set();
        foreach ($lines as $line) {
            $result = $line->process_warehouse($this->increase);
            if ($result["status"] == "ERROR") {
                return $result;
            }
        }
        return $result;
    }
}

class transaction_line extends Model {

    public function __construct($db = null) {
        $this->fields['transaction'] = new ForeignKey(array("modelName"=>"transaction"));
        $this->fields['item'] = new ForeignKey(array("modelName"=>"item"));
        $this->fields['quantity'] = new FloatField();
        $this->fields['unit_of_measure'] = new ForeignKey(array("modelName"=>"unit_of_measure","dependsOn"=>"item","linkerObj"=>"item_unit_of_measure","linkerDependeeField"=>"item","linkerDependentField"=>"unit_of_measure"));
        $this->fields['unit_price'] = new FloatField();
        $this->fields['expiration_date'] = new DateField();
        $this->fields['remarks'] = new CharField();

        parent::__construct($db);
    }

    public function __toString() {
        return $this->id;
    }

    public function process_date() {
        $dt = new DateTimeField(array("value"=>$this->transaction__obj()->process_date));
        return $dt->display_value();
    }

    public function action() {
        $map = array();
        $map["received"] = "<span class='text-success'><i class='fa fa-plus'></i> Added</span>";
        $map["released"] = "<span class='text-danger'><i class='fa fa-minus'></i> Deducted</span>";
        return $map[$this->transaction__obj()->action];
    }

    public function entered_by() {
        return $this->transaction__obj()->create_by__obj()->fullname();
    }

    public function form() {

        // Get related delivery receipt form

        $line = new delivery_receipt_line();
        $qs = $line->filter(_OR(
            $line->transaction_Release__exact($this->transaction),
            $line->transaction_Receive__exact($this->transaction)
        ))->fetch();

        if (count($qs) > 0) {
            $line = $qs[0];
            $code = $line->delivery_receipt__obj()->dr_no;

            return 
            "<a href='../reports/receipt.php?dr&code={$code}' target='_newtab'>
                {$code}
            </a>";
        }

        // Get related return form

        $line = new return_form_line();
        $qs = $line->filter(_OR(
            $line->transaction_Release__exact($this->transaction),
            $line->transaction_Receive__exact($this->transaction)
        ))->fetch();

        if (count($qs) > 0) {
            $line = $qs[0];
            $code = $line->return_form__obj()->return_no;

            return 
            "<a href='../reports/receipt.php?return&code={$code}' target='_newtab'>
                {$code}
            </a>";
        }

        return "None";
    }

    public function save() {
        $db = $this->get_db();

        $transaction = new transaction($db);
        $transaction->get($this->transaction);

        if ($this->id == 0) {
            // Save before processing warehouse since
    		// the id will be used in ProcessWarehouse
            parent::save();
            if ($transaction->status == "processed") {
                $increase = $transaction->increase;
                $result = $this->process_warehouse($increase);

                if ($result["status"] == "ERROR") {
                    $this->quantity = 0;
                    parent::save();
                    return $result;
                }
            }
        }

        return parent::save();
    }

    public function process_warehouse($increase) {
        $db = $this->get_db();

        if ($increase != "none") {
            $inv = new inventory($db);
            $transaction = new transaction($db);
            $item = new item($db);
            $warehouse = new warehouse($db);
            $qty = $this->quantity;
            $uom = new unit_of_measure($db);
            $price = $this->unit_price * $qty;

            $transaction->get($this->transaction);
            $item->get($this->item);
            $warehouse->get($transaction->warehouse);
            $uom->get($this->unit_of_measure);
            $uomSmallest = $item->smallest_unit();
            $uomSmallest->set_db($db);

            if ($uomSmallest === null) {
                $result = array(
                    "status" => "ERROR",
                    "msg" => "Smallest unit of measure is not set for {$item->name}"
                );
                return $result;
            }

            // Convert to smallest unit of measure before saving to warehouse line
            $result = $inv->convert($item->id,$qty,$uom->id,$uomSmallest->id);
            if ($result["status"] == "ERROR") {
                return $result;
            }
            $qty = $result["result"];

            $itemHasComponent = $item->has_component();

            if ($increase == "increase") {
        		// Add new warehouse line on every item that will be added in inventory.
                $tlwls = $this->transaction_line_warehouse_line__set();
                if (count($tlwls) < 1 && $itemHasComponent == false) {
                    // Receiving new item in warehouse. Do nothing if item has component.
                    $wh = new warehouse_line($db);
                    $wh->item = $item->id;
                    $wh->warehouse = $warehouse->id;
                    $wh->quantity = new CalculatedField($wh->quantity,'+',$qty);
                    $wh->unit_price = $price / $qty;
                    $wh->expiration_date = $this->expiration_date;
                    $wh->create_date = $this->create_date;
                    $wh->save();
                } else {

                    // Do only if item is returnable
                    if ($item->is_returnable == true) {

                        // Receiving item in warehouse through void
                        foreach ($tlwls as $tlwl) {
                            $wh = new warehouse_line($db);
                            $wh->get($tlwl->warehouse_line);
                            $wh->quantity = new CalculatedField($wh->quantity,'+',$tlwl->quantity);
                            $wh->save();
                        }
                    } else {
                        $result = array(
                            "status" => "ERROR",
                            "msg" => "Failed to process transaction. {$item->name} is not returnable"
                        );
                        return $result;
                    }
                }
            } elseif ($increase == "decrease") {
                // unit price will be calculated during release.
                $this->unit_price = 0;

                if ($item->is_available($warehouse->id, $qty, $uomSmallest->id)) {
                    // Reset tlwl in case it exists
                    $tlwls = $this->transaction_line_warehouse_line__set();
                    foreach ($tlwls as $tlwl) {
                        $tlwl->set_db($db);
                        $tlwl->quantity = 0;
                        $tlwl->save();
                    }

                    // If item has components then decrease its item component instead
                    if ($itemHasComponent == true) {
                        $result = $this->decrease_item_components($item, $warehouse, $qty);
                    } else {
                        $result = $this->decrease_warehouse_line($item, $warehouse, $qty);
                    }
                } else {
                    $result = array(
                        "status" => "ERROR",
                        "msg" => "Failed to process transaction. {$item->name} is not available"
                    );
                    return $result;
                }
            }
        }
        return $result;
    }

    public function decrease_item_components($item, $warehouse, $qty) {
        $result = array(
            "status" => "OK",
            "msg" => "Transaction successfully processed"
        );

        $inv = new inventory();
        $itemComponents = $item->item_component__set();
        if (count($itemComponents) > 0) {
            foreach ($itemComponents as $itemComponent) {
                $itm = new item($this->get_db());
                $itm->get($itemComponent->component);
                $itmSmallestUnit = $itm->smallest_unit();
                $itmSmallestUnit->set_db($this->get_db());

                $result = $inv->convert($itm->id, ($itemComponent->quantity * $qty), $itemComponent->unit_of_measure, $itmSmallestUnit->id);
                if ($result["status"] == "ERROR") {
                    return $result;
                }
                $whlQty = $result["result"];

                $result = $this->decrease_warehouse_line($itm, $warehouse, $whlQty);
                if ($result["status"] == "ERROR") {
                    return $result;
                }
            }
        }

        return $result;
    }

    public function decrease_warehouse_line($item, $warehouse, $qty) {
        $accumulatedUnitPrice = 0.0;

        $result = array(
            "status" => "OK",
            "msg" => "Transaction successfully processed"
        );

        $db = $this->get_db();

        $wh = new warehouse_line($db);
        $whls = $wh->filter($wh->item__exact($item->id),$wh->warehouse__exact($warehouse->id))->fetch();

        if (count($whls) > 0) {

            $qtyRemaining = $qty;
            $tlwlQuantity = 0.0;

            foreach ($whls as $whl) {

                $whl->set_db($db);
                if ($qtyRemaining > $whl->quantity) {
                    $tlwlQuantity = $whl->quantity;
                    $whl->quantity = 0;
                    $result = $whl->save();
                    if ($result["status"] == "ERROR") {
                        return $result;
                    }
                } else {
                    $tlwlQuantity = $qtyRemaining;
                    $whl->quantity = new CalculatedField($whl->quantity,'-',$tlwlQuantity);
                    $result = $whl->save();
                    if ($result["status"] == "ERROR") {
                        return $result;
                    }
                }
                $accumulatedUnitPrice += $whl->unit_price * $tlwlQuantity;

                // Save transaction line warehouse line for voiding purpose if ever
                $tlwl = new transaction_line_warehouse_line();
                $qs = $tlwl->filter($tlwl->transaction_line__exact($this->id),$tlwl->warehouse_line__exact($whl->id))->fetch();
                if (count($qs) > 0) {
                    $tlwl = $qs[0];
                }
                $tlwl->set_db($db);
                $tlwl->transaction_line = $this->id;
                $tlwl->warehouse_line = $whl->id;
                $tlwl->quantity = $tlwlQuantity;
                $result = $tlwl->save();
                if ($result["status"] == "ERROR") {
                    return $result;
                }

                $qtyRemaining = $qtyRemaining - $tlwlQuantity;
                if ($qtyRemaining < 1) {
                    // Stop the loop since there is nothing to process anymore.
                    break;
                }
            }
        }

        $this->unit_price = $accumulatedUnitPrice / $this->quantity;

        return $result;
    }
}

class transaction_line_warehouse_line extends Model {

    public function __construct($db = null) {
        $this->fields['transaction_line'] = new ForeignKey(array("modelName"=>"transaction_line"));
        $this->fields['warehouse_line'] = new ForeignKey(array("modelName"=>"warehouse_line"));
        $this->fields['quantity'] = new FloatField();

        parent::__construct($db);
    }
}

// Proxy model just to add custom menu on sidebar under Inventory
class my_items extends Model {

    public function __construct($db = null) {
        
        parent::__construct($db);
    }

    public function __returnAdd() {
        return "p=my_items";
    }

    public function __platformAdd() {
        return "item";
    }

    public function GetItemOverAllQuantity($item) {
        $qty = 0;
        
        // Get overall quantity

        $db = new DB();
        $db->connect();
        
        $sql = "
        SELECT SUM(quantity) as `quantity` FROM (
            SELECT 
                item_id, item_image, item, 
                warehouse_id, warehouse, 
                SUM(quantity) quantity, unit_id, unit
            FROM (
                SELECT 
                    i.`id` as `item_id`, i.`image` as `item_image`, i.`name` as `item`, w.id as `warehouse_id`, w.`name` as `warehouse`, whl.quantity, u.`id` as unit_id, u.`abbreviation` as `unit`
                FROM 
                    warehouse_line whl INNER JOIN
                    item i ON whl.item = i.id INNER JOIN
                    warehouse w ON whl.warehouse = w.id INNER JOIN
                    item_unit_of_measure iuom ON iuom.item = i.id AND iuom.is_smallest = 1 INNER JOIN
                    unit_of_measure u ON iuom.unit_of_measure = u.id
                WHERE
                    whl.is_deleted = 0 AND whl.quantity > 0 AND w.warehouse_type != 'no_return' 
                ORDER BY whl.item
            ) `temp`
            GROUP BY 
                item_id, unit_id, warehouse_id
            ) as `temp2`
        WHERE
            item_id = {$item}";

        $query = $db->query($sql);

        if ($query !== false) {
            $result = $db->fetch_assoc($query);
            $qty = $result['quantity'];
        }

        $db->close();

        return $qty;
    }

    public function GetItemQuantity($item, $unit, $warehouse) {
        $qty = 0;
        
        // Get new quantity

        $db = new DB();
        $db->connect();
        
        $sql = "
        SELECT quantity FROM (
            SELECT 
                item_id, item_image, item, 
                warehouse_id, warehouse, 
                SUM(quantity) quantity, unit_id, unit
            FROM (
                SELECT 
                    i.`id` as `item_id`, i.`image` as `item_image`, i.`name` as `item`, w.id as `warehouse_id`, w.`name` as `warehouse`, whl.quantity, u.`id` as unit_id, u.`abbreviation` as `unit`
                FROM 
                    warehouse_line whl INNER JOIN
                    item i ON whl.item = i.id INNER JOIN
                    warehouse w ON whl.warehouse = w.id INNER JOIN
                    item_unit_of_measure iuom ON iuom.item = i.id AND iuom.is_smallest = 1 INNER JOIN
                    unit_of_measure u ON iuom.unit_of_measure = u.id
                WHERE
                    whl.is_deleted = 0 AND whl.quantity > 0 
                ORDER BY whl.item
            ) `temp`
            GROUP BY 
                item_id, unit_id, warehouse_id
            ) as `temp2`
        WHERE
            item_id = {$item} AND
            warehouse_id = {$warehouse} AND
            unit_id = {$unit}";

        $query = $db->query($sql);

        if ($query !== false) {
            $result = $db->fetch_assoc($query);
            $qty = $result['quantity'];
        }

        $db->close();

        return $qty;
    }

    public function count_queryset($queryParams, $start = NULL, $limit = NULL) {
        global $OPERATORS;
        
        $db = new DB();

        $innerWhere = "";
        $outerWhere = "";
        if (is_array($queryParams) && count($queryParams) > 0) {
            $temp = $queryParams;
            if (isset($temp['quantity']) && isset($temp['quantity_operator'])) {
                if (is_numeric($temp['quantity'])) {
                    $prefix = negate($temp['quantity_operator']);
                    $operator = $OPERATORS[$temp['quantity_operator']];
                    $value = valueOnOperator($operator, $temp['quantity']);
                    $outerWhere = "WHERE {$prefix} `quantity` {$operator} {$value} ";
                }
            }

            $itemSet = false;
            if (isset($temp['item']) && isset($temp['item_operator'])) {
                if ($temp['item'] != '') {
                    $prefix = negate($temp['item_operator']);
                    $operator = $OPERATORS[$temp['item_operator']];
                    $value = valueOnOperator($operator, $temp['item']);
                    $innerWhere .= "AND {$prefix} i.`name` {$operator} {$value} ";
                }
            }

            $warehouseSet = false;
            if (isset($temp['warehouse']) && isset($temp['warehouse_operator'])) {
                if ($temp['warehouse'] != '') {
                    $where = ($itemSet) ? "NOT" : "AND";
                    $prefix = negate($temp['warehouse_operator']);
                    $operator = $OPERATORS[$temp['warehouse_operator']];
                    $value = valueOnOperator($operator, $temp['warehouse']);
                    $innerWhere .= "{$where} {$prefix} w.`name` {$operator} {$value} ";
                }
            }
        }

        $sql = "
        SELECT COUNT(item_id) as `count` FROM (
            SELECT 
                item_id, item, warehouse_id, warehouse, 
                SUM(quantity) quantity, unit_id, unit
            FROM (
                SELECT 
                    i.`id` as `item_id`, i.`name` as `item`, w.id as `warehouse_id`, w.`name` as `warehouse`, whl.quantity, u.`id` as unit_id, u.`abbreviation` as `unit`
                FROM 
                    warehouse_line whl INNER JOIN
                    item i ON whl.item = i.id INNER JOIN
                    warehouse w ON whl.warehouse = w.id INNER JOIN
                    item_unit_of_measure iuom ON iuom.item = i.id AND iuom.is_smallest = 1 INNER JOIN
                    unit_of_measure u ON iuom.unit_of_measure = u.id
                WHERE
                    whl.is_deleted = 0 AND (whl.quantity > 0 OR whl.warehouse = i.warehouse) AND w.warehouse_type != 'no_return' 
                    {$innerWhere}
                ORDER BY whl.item
            ) `temp`
            GROUP BY 
                item_id, unit_id, warehouse_id
        ) as `temp2`
        {$outerWhere}
        ORDER BY item ASC
        ";
        
        $db->connect();

        $query = $db->query($sql);

        if ($query === false) {
            $db->close();
            return 0;
        }

        $result = $db->fetch_assoc($query);
        $db->close();

        return $result['count'];
    }

    public function get_queryset($queryParams, $start = NULL, $limit = NULL) {
        global $OPERATORS;

        $db = new DB();
        
        $offset = "";
        if (is_numeric($start) && is_numeric($limit)) {
            $offset = "LIMIT {$start}, {$limit}";
        }

        $innerWhere = "";
        $outerWhere = "";
        if (is_array($queryParams) && count($queryParams) > 0) {
            $temp = $queryParams;
            if (isset($temp['quantity']) && isset($temp['quantity_operator'])) {
                if (is_numeric($temp['quantity'])) {
                    $prefix = negate($temp['quantity_operator']);
                    $operator = $OPERATORS[$temp['quantity_operator']];
                    $value = valueOnOperator($operator, $temp['quantity']);
                    $outerWhere = "WHERE {$prefix} `quantity` {$operator} {$value} ";
                }
            }

            $itemSet = false;
            if (isset($temp['item']) && isset($temp['item_operator'])) {
                if ($temp['item'] != '') {
                    $prefix = negate($temp['item_operator']);
                    $operator = $OPERATORS[$temp['item_operator']];
                    $value = valueOnOperator($operator, $temp['item']);
                    $innerWhere .= "AND {$prefix} i.`name` {$operator} {$value} ";
                }
            }

            $warehouseSet = false;
            if (isset($temp['warehouse']) && isset($temp['warehouse_operator'])) {
                if ($temp['warehouse'] != '') {
                    $where = ($itemSet) ? "NOT" : "AND";
                    $prefix = negate($temp['warehouse_operator']);
                    $operator = $OPERATORS[$temp['warehouse_operator']];
                    $value = valueOnOperator($operator, $temp['warehouse']);
                    $innerWhere .= "{$where} {$prefix} w.`name` {$operator} {$value} ";
                }
            }
        }
        
        $sql = "
        SELECT * FROM (
            SELECT 
                item_id, item_image, item, 
                warehouse_id, warehouse, 
                SUM(quantity) quantity, unit_id, unit
            FROM (
                SELECT 
                    i.`id` as `item_id`, i.`image` as `item_image`, i.`name` as `item`, w.id as `warehouse_id`, w.`name` as `warehouse`, whl.quantity, u.`id` as unit_id, u.`abbreviation` as `unit`
                FROM 
                    warehouse_line whl INNER JOIN
                    item i ON whl.item = i.id INNER JOIN
                    warehouse w ON whl.warehouse = w.id INNER JOIN
                    item_unit_of_measure iuom ON iuom.item = i.id AND iuom.is_smallest = 1 INNER JOIN
                    unit_of_measure u ON iuom.unit_of_measure = u.id
                WHERE
                    whl.is_deleted = 0 AND (whl.quantity > 0 OR whl.warehouse = i.warehouse) AND w.warehouse_type != 'no_return' 
                    {$innerWhere}
                ORDER BY whl.item
            ) `temp`
            GROUP BY 
                item_id, unit_id, warehouse_id
        ) `temp2`
        {$outerWhere}
        ORDER BY item ASC
        {$offset}
        ";

        $db->connect();

        $query = $db->query($sql);

        $list = array();
        if ($query === false) {
            $db->close();
            return $list;
        } else {
            while ($record = $db->fetch_assoc($query)) {
                $list[] = $record;
            }
        }
        
        $db->close();
        return $list;
    }
}

?>
