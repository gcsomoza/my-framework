<?php

class my_items_admin extends ModelAdmin {
    
}

class warehouse_line_admin extends ModelAdmin {
    public function __construct() {
        $this->listColumns = array(
            "warehouse",
            "item",
            "quantity",
        );
    }
}

class transaction_admin extends ModelAdmin {
    public function __construct() {
        $this->inlines = array(
            "transaction_line"
        );

        $this->listColumns = array(
            "tracking_number",
            "action",
            "warehouse",
            "status",
            "remarks",
            "process_date",
            "create_date",
            "create_by",
        );
    }
}

class transaction_line_admin extends ModelAdmin {
    public function __construct() {
        $this->listColumns = array(
            "process_date",
            "action",
            "item",
            "quantity",
            "unit_of_measure",
            "entered_by",
            "remarks",
            "form",
        );
    }
}

class item_admin extends ModelAdmin {
    public function __construct() {
        $this->inlines = array(
            "item_unit_of_measure",
            // "item_component"
            "serial_number",
        );

        $this->searchFields = array(
            "name",
            "description",
            "inventory_category__name"
        );

        // $this->searchFilters = array(
        //     "is_returnable"
        // );

        $this->listColumns = array(
            "image",
            "name",
            "inventory_category",
        );
    }

    public function IsDisplayed() {
        return false;
    }
}

class item_unit_of_measure_admin extends InlineAdmin {
    public function __construct() {
      $this->listColumns = array(
          "item",
          "unit_of_measure",
          "is_smallest",
      );
    }
}

class item_component_admin extends InlineAdmin {

}

class serial_number_admin extends ModelAdmin {
    public function __construct() {
        $this->listColumns = array(
            "serial_number",
            "warehouse",
        );
      }
}

class unit_of_measure_admin extends ModelAdmin {

}

class unit_of_measure_conversion_admin extends ModelAdmin {

}

class inventory_category_admin extends ModelAdmin {

}

class unit_of_measure_type_admin extends ModelAdmin {

}

class warehouse_admin extends ModelAdmin {

}

?>
