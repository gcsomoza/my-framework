<?php

if (isset($_GET['status'])) {
    $status = $_GET['status'];
    $msg = $_GET['msg'];

    $status_icon_map = array(
        "OK" => "alert-success",
        "ERROR" => "alert-warning",
    );

    $status_label_map = array(
        "OK" => "Success!",
        "ERROR" => "Warning!",
    );

    print '<div class="alert '.$status_icon_map[$status].'">
      <strong>'.$status_label_map[$status].'</strong> '.$msg.'
    </div>';
}

if (!isset($_GET['p'])) {
    exit;
} else {
    $object_name = $_GET['p'];
}
$options_link = "action/options.php";

$object_admin_name = $_GET['p'] . '_admin';
$object_admin = new $object_admin_name;

$page = 1;
if (isset($_GET['page'])) {
    $page = $_GET['page'];
}

$return_url = "";
if (isset($_GET['return_url'])) {
    $return_url = $_GET['return_url'];
}

// Check for custom add or update page if
$file = _PAGEDIR_."custom"._DS_.$_GET['a'].".".$_GET['p'].".php";
if (file_exists($file)) {
    include $file;
} else {
?>

<!-- Display add or update controls -->
<form method="POST" action="action/save.php" target="_self" enctype="multipart/form-data">
    <input type="hidden" name="return_url" value="<?php echo $return_url ?>" />
    <input type="hidden" name="page" value="<?php echo $page ?>" />
    <input type="hidden" id="options_link" value="<?php echo $options_link; ?>" />
    <div>
        <div class="container-fluid">

            <?php include _PAGEDIR_."breadcrumbs.php" ?>
            <br />

            <ul class="nav nav-tabs">
                <?php
                $is_inline_active = false;
                $active = "active";
                $active_tab = $_GET['p'];
                if (isset($_GET['tab']) && $_GET['tab'] != $_GET['p']) {
                    $active = "";
                }
                ?>
                <li class="<?php echo $active ?>"><a data-toggle="tab" href="#object"><?php echo ucwords(str_replace("_"," ",$_GET["p"])) ?></a></li>
                <?php
                if ($_GET['a'] == 'update') {
                    $active = '';
                    foreach($object_admin->inlines as $inline) {
                        if (isset($_GET['tab']) && $_GET['tab'] == $inline) {
                            $is_inline_active = true;
                            $active = 'active';
                            $active_tab = $inline;
                        }
                        $inline_object = new $inline;
                        print '<li class="'.$active.'"><a data-toggle="tab" href="#'.$inline.'">'.$inline_object->verbose().'</a></li>';
                        $inline_object = null;
                        $active = '';
                    }
                }
                ?>
              </ul>

            <div class="tab-content">
                <div id="object" class="tab-pane fade <?php if ($active_tab == $_GET['p']) { echo "in active"; } ?>">
                    <hr/>
                    <!-- Input Controls -->
                    <?php

                    if (isset($_GET['p'])) {

                        $a = (isset($_GET['a'])) ? $_GET['a'] : '';
                        $id = (isset($_GET['id'])) ? $_GET['id'] : '';

                        $object = new $_GET['p'];

                        if (($a != '') && ($a == 'update')) {
                            if (($id != '') && is_numeric($id)) {
                                // Retrieve user data
                                $object->get($_GET['id']);
                            }
                        }

                        $fields = $object->get_fields();

                        print "<input type='hidden' id='object' name='object' value='{$_GET['p']}' />";
                        foreach ($fields as $fieldName => $fieldObject) {
                            if ($fieldName == "id") {
                                print "<input type='hidden' id='id' name='id' value='{$fieldObject->get_value()}' />";
                            } elseif ($fieldName != "is_deleted") {
                                $isDisplayed = ($fieldObject->canAdd && $a == "add") || ($fieldObject->canUpdate && $a == "update");
                                if ($isDisplayed) {
                                    // Display input controls
                                    if ($fieldObject instanceof ForeignKey && $fieldObject->dependsOn != "") {
                                        // foreignkey field is dependent on another field to display its value.
                                        $fieldObject->linkerDependeeValue = $fields[$fieldObject->dependsOn]->get_value();
                                    }
                                    print $fieldObject->input_control($object_name,$fieldName,$a);
                                }
                            }
                        }
                    }
                    ?>
                    <!-- Many-to-Many Dual Listbox -->
                    <?php
                    foreach ($object->m2m as $foreign_object_name => $m2m_field) {
                        print "<hr />";
                        // get other object name from m2m index. eg. m2m['group_user']
                        $other_object_name = $m2m_field->otherObjectName;

                        // initialize other object
                        $other_object = new $other_object_name;
                        $other_object_list = $m2m_field->getOptions();

                        // get selected objects
                        $selected_objects = $object->{"{$foreign_object_name}__set"}();
                        $selected_objects_ids = array();
                        foreach ($selected_objects as $key => $selected_objects_item) {
                            $selected_objects_ids[$selected_objects_item->$other_object_name] = 'selected';
                        }

                        // display duallistbox
                        $displayName = ($m2m_field->displayName == '') ? ucwords(str_replace("_"," ",$foreign_object_name)) : $m2m_field->displayName;

                        $helpText = '';
                        if ($m2m_field->helpText != '') {
                            $helpText = "
                                <div style='margin-top: -5px; margin-bottom: 5px;'><i class='fa fa-question-circle'></i> <small>{$m2m_field->helpText}</small></div>
                            ";
                        }

                        print '<h3>'.ucwords(str_replace("_"," ",$displayName)).'</h3>';
                        print $helpText;
                        print '<input type="hidden" name="m2m_'.$foreign_object_name.'[]" />'; // this will handle posting an empty m2m
                        print '<select multiple="multiple" size="10" name="m2m_'.$foreign_object_name.'[]" class="duallistbox">';
                        foreach ($other_object_list as $list_index => $list_item) {
                            $selected = "";
                            if (isset($selected_objects_ids[$list_item->id])) {
                                $selected = $selected_objects_ids[$list_item->id];
                            }
                            print '<option value="'.$list_item->id.'" '.$selected.'>'.$list_item.'</option>';
                        } // end foreach
                        print '</select>';
                    }
                    ?>

                    <div class="row list-footer">
                        <?php
                            $custom_buttons = _PAGEDIR_."custom_buttons"._DS_.$_GET['a'].".".$_GET['p'].".php";
                            if (file_exists($custom_buttons)) {
                                include $custom_buttons;
                            }
                        ?>
                        <button name="save" class="btn btn-primary">Save</button>
                        <?php if($object_admin->CanAdd()) { ?>
                            <button name="save_add_another" class="btn btn-primary">Save and Add Another</button>
                        <?php } // end if ?>
                        <button name="save_continue" class="btn btn-primary">Save and Continue Editing</button>
                    </div>
                </div> <!-- object tab -->

                <?php if ($_GET['a'] == 'update') { include "inlines.php"; } ?>
            </div> <!-- tab-content -->

        </div>
    </div>

    <div class="bottom-space"></div>
</form>

<?php
}
?>
