
<?php
    $models = GetModels();
    foreach ($models as $model => $classes):
        if(IsModulePermitted($model) == true) {
?>

<div class="row">
    <div class="col-sm-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa <?php echo GetModuleIcon($model); ?> fa-lg"></i> <?php echo ucwords($model) ?>
            </div>
            <div class="panel-body">

                <?php foreach ($classes as $class): ?>
                    <?php $cls = $class . "_admin"; ?>
                    <?php $adminObj = new $cls; if (!$adminObj->IsDisplayed()) { continue; } ?>
                    <div class="row">
                        <div class="col-sm-3">
                            <?php $cls = new $class ?>
                            <?php echo $cls->verbose(); ?>
                        </div>
                        <div class="col-sm-2">
                            <a href="?p=<?php echo $cls->__platformUpdate(); ?>&page=1&return_url=<?php echo $cls->__returnUpdate(); ?>" class="display-block"><i class="fa fa-pencil"></i> Change</a>
                        </div>
                        <div class="col-sm-2">
                            <a href="?p=<?php echo $cls->__platformAdd(); ?>&page=1&a=add&return_url=<?php echo $cls->__returnAdd(); ?>" class="display-block"><i class="fa fa-plus"></i> Add</a>
                        </div>
                    </div>
                    <br />
                <?php endforeach; ?>

            </div>
        </div>
    </div>
</div>

<?php
    } // end if
    endforeach;
?>
