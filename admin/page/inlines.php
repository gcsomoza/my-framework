<?php

// $object_admin was declared in add-or-upadte.php

foreach($object_admin->inlines as $inline) {
    $active = "";
    if ($active_tab == $inline) {
        $active = "in active";
    }
    $return_url = _PROTOCOL_ . _HOST_ . _US_ . _APPURL_ . _US_ . "?p={$SelectedPage}&page={$_GET['page']}&a={$_GET['a']}&id={$_GET['id']}&return_url={$_GET['return_url']}&tab={$inline}";
?>

<div id="<?php echo $inline ?>" class="tab-pane fade <?php echo $active; ?>">
    <hr/>

    <!-- Display data table -->
    <div>

        <div class="container-fluid">

            <?php
            // $object is defined in add-or-update.php
            // $object_name is defined in add-or-update.php
            // $active_tab is defined in add-or-update.php
            $object_inline = new $inline;
            $parent_id = $SelectedPage . "__exact";
            $records = $object_inline->filter($object_inline->{$parent_id}($object->id))->fetch();
            ?>

            <?php if($object_admin->CanUpdate()) { ?>
                <div>
                    <div class="text-right">
                        <?php
                            $href = "page/inlines-add-or-update.php?parent={$object_name}&page={$page}&parent_id={$object->id}&inline={$inline}&return_url={$_GET['return_url']}&inline_action=";
                        ?>
                        <a align="right" href="<?php echo $href . "add"; ?>" class="btn btn-sm btn-success iframe"><i class="fa fa-plus"></i> Add New <?php echo $object_inline->verbose(); ?></a>
                    </div>
                </div>
            <?php } // end if ?>

            <br />
            <table class="table table-bordered table-hover">
                <?php

                $adminObj = $inline . "_admin";
                $admin = new $adminObj;
                $columns = $admin->GetListColumns();
                $columnsCount = count($columns) + 1;

                print "<thead>";
                print "<th width='30'><input type='checkbox' onchange='checkall();' class='checker' /></th>";
                foreach ($columns as $column) {
                    $col = $object->get_field_object($column);
                    if ($col instanceof Field) {
                        if ($col->displayName != "") {
                            $column = $col->displayName;
                        } else {
                            $column = ucwords(str_replace("_", " ", $column));
                        }
                        print "<th>{$column}</th>";
                    } else {
                        // field is something else or customly added
                        $column = ucwords(str_replace("_", " ", $column));
                        print "<th>{$column}</th>";
                    }
                }
                print "</thead>";

                if (empty($records)) {
                    print "<tr><td colspan='{$columnsCount}' class='text-center text-warning warning'>There are no records to be displayed.</td></tr>";
                }
                foreach ($records as $record) {
                    $admin->SetObject($record);
                    $columns = $admin->GetListValues();
                    print "<tr>";
                    $setUpdate = true;
                    print "<td><input type='checkbox' value='{$record->id}' name='{$record->id}' class='record_check_{$inline}' /></td>";
                    foreach ($columns as $column => $displayVal) {
                        if ($setUpdate) {
                            $setUpdate = false;
                            print "<td><a class='iframe' href='{$href}update&inline_id={$record->id}' style='display: block;'>{$displayVal}</a></td>";
                        } elseif(strpos($column,"__")) {
                            $temp = explode("__",$column);
                            $function = $temp[0] . "__load";
                            $foreignObj = $record->{$function}();
                            $value = $foreignObj->{$temp[1]};
                            print "<td>{$value}</td>";
                        } else {
                            print "<td>{$displayVal}</td>";
                        }
                    }
                    print "</tr>";
                }

                ?>
            </table>

            <!-- Modal -->
            <div class="modal fade" id="modalDeleteDialog_<?php echo $inline ?>" role="dialog">
                <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete <?php echo ucwords(str_replace("_"," ",$inline)) ?></h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete the selected records?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" onclick="inlineDelete('action/delete.php','<?php echo $inline ?>','<?php echo addslashes($return_url) ?>');">Delete</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
                </div>
            </div>

            <?php if($object_admin->CanDelete()) { ?>
                <div>
                    <div class="text-left">
                        <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modalDeleteDialog_<?php echo $inline ?>"><i class="fa fa-remove"></i> Delete Selected</button>
                    </div>
                </div>
            <?php } // end if ?>
        </div>
    </div>
</div>

<?php
} // end foreach
?>
