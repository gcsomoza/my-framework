<?php

session_start();
include "../settings.php";

if(!isset($_GET['parent']) || $_GET['parent'] == "") {
    print "parent not defined";
    exit;
}
$parent_name = $_GET['parent'];
$field_name = $_GET['field'];

$parent = new $parent_name;

$fieldObject = $parent->get_field_object($field_name);

$object_name = $fieldObject->modelName;
$object = new $object_name;

$return_url = _ACTIONURL_ . "options-addnew.php?field={$field_name}";

$object_admin_name = $object_name . "_admin";
$object_admin = new $object_admin_name;

?>

<html>

<head>

<meta charset="utf-8" >
<meta name="viewport" content="width=device-width, initial-scale=1" >
<title><?php echo _TITLE_ ?></title>

<?php include "../static/css/import.php" ?>
<?php include "../static/lib/import-css.php" ?>

</head>

<body>

<!-- Display add or update controls -->
<form method="POST" action="../action/save.php">
    <input type="hidden" id="return_url" name="return_url" value="<?php echo $return_url; ?>" />

    <div>
        <div class="container-fluid">

            <?php
                print "<h3>{$object->verbose()}</h3>";
                $a = "add";
            ?>

            <hr/>
            <!-- Input Controls -->
            <?php

            $fields = $object->get_fields();

            print "<input type='hidden' id='object' name='object' value='{$object_name}' />";
            print "<input type='hidden' id='add-foreignkey' name='add-foreignkey' value='1' />";
            foreach ($fields as $fieldName => $fieldObject) {

                if ($fieldName == "id") {
                    print "<input type='hidden' id='id' name='id' value='{$fieldObject->get_value()}' />";
                } elseif ($fieldName != "is_deleted") {
                    $isDisplayed = ($fieldObject->canAdd);
                    if ($isDisplayed) {
                        // Display input controls
                        if ($fieldObject instanceof ForeignKey && $fieldObject->dependsOn != "") {
                            // foreignkey field is dependent on another field to display its value.
                            $fieldObject->linkerDependeeValue = $fields[$fieldObject->dependsOn]->get_value();
                        }
                        print $fieldObject->input_control($object_name,$fieldName,$a,true);
                    }
                }
            }
            ?>
            <!-- Many-to-Many Dual Listbox -->
            <?php
            foreach ($object->m2m as $foreign_object_name => $m2m_field) {
                print "<hr />";
                // get other object name from m2m index. eg. m2m['group_user']
                $other_object_name = $m2m_field->otherObjectName;

                // initialize other object
                $other_object = new $other_object_name;
                $other_object_list = $other_object->filter()->fetch();

                // get selected objects
                $selected_objects = $object->{"{$foreign_object_name}__set"}();
                $selected_objects_ids = array();
                foreach ($selected_objects as $key => $selected_objects_item) {
                    $selected_objects_ids[$selected_objects_item->$other_object_name] = 'selected';
                }

                // display duallistbox
                $displayName = ($m2m_field->displayName == '') ? ucwords(str_replace("_"," ",$foreign_object_name)) : $m2m_field->displayName;

                $helpText = '';
                if ($m2m_field->helpText != '') {
                    $helpText = "
                        <div style='margin-top: -5px; margin-bottom: 5px;'><i class='fa fa-question-circle'></i> <small>{$m2m_field->helpText}</small></div>
                    ";
                }

                print '<h3>'.ucwords(str_replace("_"," ",$displayName)).'</h3>';
                print $helpText;
                print '<input type="hidden" name="m2m_'.$foreign_object_name.'[]" />'; // this will handle posting an empty m2m
                print '<select multiple="multiple" size="10" name="m2m_'.$foreign_object_name.'[]" class="duallistbox">';
                foreach ($other_object_list as $list_index => $list_item) {
                    $selected = "";
                    if (isset($selected_objects_ids[$list_item->id])) {
                        $selected = $selected_objects_ids[$list_item->id];
                    }
                    print '<option value="'.$list_item->id.'" '.$selected.'>'.$list_item.'</option>';
                } // end foreach
                print '</select>';
            }
            ?>
        </div>
    </div>

    <div class="bottom-space"></div>

    <div class="add-or-update-inline-footer">
        <div class="form-group">
            <?php if($object_admin->CanUpdate()) { ?>
                <div class="col-sm-offset-6 col-sm-2">
                    <button name="save" class="form-control btn btn-primary">Save</button>
                </div>
            <?php } // end if ?>
            <div class="col-sm-2">
                <button type="button" class="form-control btn btn-default" onclick="top.colorboxClose();">Close</button>
            </div>
        </div>
    </div>
</form>

</body>

<?php include "../static/js/import.php" ?>
<?php include "../static/lib/import-js.php" ?>

<?php display_consolelogs(); ?>

</html>
