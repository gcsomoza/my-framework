<?php

include "../settings.php";

$parent = $_GET['parent'];
$parent_id = $_GET['parent_id'];
$inline = $_GET['inline'];

$file = _PAGEDIR_."custom"._DS_."inlines-add-or-update_".$inline.".php";
if (file_exists($file)) {
    include $file;
} else {

session_start();

$return_url = "p={$parent}&a=update&id={$parent_id}&tab={$inline}";
if (isset($_GET['return_url']) && $_GET['return_url'] != '') {
    $return_url = "p={$parent}&a=update&id={$parent_id}&return_url={$_GET['return_url']}&tab={$inline}";
}

$options_link = _PROTOCOL_ . _HOST_ . _US_ . _APPURL_ . _US_ . "action" . _US_ . "options.php";

$object_admin_name = $parent . "_admin";
$object_admin = new $object_admin_name;

$inline_admin_name = $inline . "_admin";
$inline_admin = new $inline_admin_name;

$page = 1;
if (isset($_GET['page'])) {
    $page = $_GET['page'];
}
?>

<html>

<head>

<meta charset="utf-8" >
<meta name="viewport" content="width=device-width, initial-scale=1" >
<title><?php echo _TITLE_ ?></title>

<?php include "../static/css/import.php" ?>
<?php include "../static/lib/import-css.php" ?>

</head>

<body>

<!-- Display add or update controls -->
<form method="POST" action="../action/save.php" target="_parent">
    <input type="hidden" name="page" value="<?php echo $page; ?>" />

    <input type="hidden" id="object" value="<?php echo $inline; ?>" />
    <input type="hidden" name="return_url" value="<?php echo $return_url; ?>" />
    <input type="hidden" id="options_link" value="<?php echo $options_link; ?>" />
    <div>
        <div class="container-fluid">

            <?php
                $object = new $inline;
                print "<h3>{$object->verbose()}</h3>"
            ?>

            <hr/>
            <!-- Input Controls -->
            <?php

            if (isset($_GET['inline'])) {

                $a = (isset($_GET['inline_action'])) ? $_GET['inline_action'] : '';
                $id = (isset($_GET['inline_id'])) ? $_GET['inline_id'] : '';

                if (($a != '') && ($a == 'update')) {
                    if (($id != '') && is_numeric($id)) {
                        // Retrieve user data
                        $object->get($id);
                    }
                }

                $fields = $object->get_fields();

                print "<input type='hidden' id='object' name='object' value='{$inline}' />";
                foreach ($fields as $fieldName => $fieldObject) {
                    if ($fieldName == "id") {
                        print "<input type='hidden' id='id' name='id' value='{$fieldObject->get_value()}' />";
                    } elseif ($fieldObject instanceof ForeignKey && $fieldObject->modelName == $parent & $fieldObject->modelParent == true) {
                        $fieldObject->set_value($parent_id);
                        $fieldObject->readonly = true;
                        print $fieldObject->input_control($inline,$fieldName,$a,true);
                    } elseif ($fieldName != "is_deleted") {
                        $isDisplayed = ($fieldObject->canAdd && $a == "add") || ($fieldObject->canUpdate && $a == "update");
                        if ($isDisplayed) {
                            // Display input controls
                            if ($fieldObject instanceof ForeignKey && $fieldObject->dependsOn != "") {
                                // foreignkey field is dependent on another field to display its value.
                                $fieldObject->linkerDependeeValue = $fields[$fieldObject->dependsOn]->get_value();
                            }
                            print $fieldObject->input_control($inline,$fieldName,$a,true);
                        }
                    }
                }
            }
            ?>
            <!-- Many-to-Many Dual Listbox -->
            <?php
            
            foreach ($object->m2m as $foreign_object_name => $m2m_field) {
                print "<hr />";
                // get other object name from m2m index. eg. m2m['group_user']
                $other_object_name = $m2m_field->otherObjectName;

                // initialize other object
                $other_object_list = $m2m_field->getOptions();

                // get selected objects
                $selected_objects = $object->{"{$foreign_object_name}__set"}();
                $selected_objects_ids = array();
                foreach ($selected_objects as $key => $selected_objects_item) {
                    $selected_objects_ids[$selected_objects_item->$other_object_name] = 'selected';
                }

                // display duallistbox
                $displayName = ($m2m_field->displayName == '') ? ucwords(str_replace("_"," ",$foreign_object_name)) : $m2m_field->displayName;

                $helpText = '';
                if ($m2m_field->helpText != '') {
                    $helpText = "
                        <div style='margin-top: -5px; margin-bottom: 5px;'><i class='fa fa-question-circle'></i> <small>{$m2m_field->helpText}</small></div>
                    ";
                }

                print '<h3>'.ucwords(str_replace("_"," ",$displayName)).'</h3>';
                print $helpText;
                print '<input type="hidden" name="m2m_'.$foreign_object_name.'[]" />'; // this will handle posting an empty m2m
                print '<select multiple="multiple" size="10" name="m2m_'.$foreign_object_name.'[]" class="duallistbox">';
                foreach ($other_object_list as $list_index => $list_item) {
                    $selected = "";
                    if (isset($selected_objects_ids[$list_item->id])) {
                        $selected = $selected_objects_ids[$list_item->id];
                    }
                    print '<option value="'.$list_item->id.'" '.$selected.'>'.$list_item.'</option>';
                } // end foreach
                print '</select>';
            }
            ?>
        </div>
    </div>

    <div class="bottom-space"></div>

    <div class="add-or-update-inline-footer">
        <div class="form-group">
            <div class="col-sm-offset-8 col-sm-2">
                <button name="save" class="form-control btn btn-primary">Save</button>
            </div>
            <div class="col-sm-2">
                <button type="button" class="form-control btn btn-default" onclick="top.colorboxClose();">Close</button>
            </div>
        </div>
    </div>
</form>

</body>

<?php include "../static/js/import.php" ?>
<?php include "../static/lib/import-js.php" ?>

<?php display_consolelogs(); ?>

</html>

<?php 
} // end if file exists
?>
