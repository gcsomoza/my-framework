<?php
// Functions used in this page will be found in include/utilities.php
?>

<div class="nav-side-menu">
    <div class="brand">&nbsp;</div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

        <div class="menu-list">

            <ul id="menu-content" class="menu-content collapse out">
                <li class="<?php echo Active($SelectedPage,"home") ?>">
                    <a href="?p=home" class="display-block">
                        <i class="fa fa-home fa-lg"></i> Home
                    </a>
                </li>

                <?php
                    $models = GetModels();
                    foreach ($models as $model => $classes):
                        if(IsModulePermitted($model) == true) {
                ?>

                    <li  data-toggle="collapse" data-target="#<?php echo $model ?>" class="collapsed">
                        <a href="#"><i class="fa <?php echo GetModuleIcon($model); ?> fa-lg"></i> <?php echo ucwords($model) ?> <span class="arrow"></span></a>
                    </li>
                    <ul class="sub-menu <?php echo ActiveCollapsible($SelectedPage, $classes) ?>" id="<?php echo $model ?>">

                        <?php foreach ($classes as $class): ?>
                            <?php $cls = $class . "_admin"; ?>
                            <?php $adminObj = new $cls; if (!$adminObj->IsDisplayed()) { continue; } ?>
                            <li class="<?php echo Active($SelectedPage, $class); ?>">
                                <a href="?p=<?php echo $class; ?>&page=1" class="display-block">
                                    <i class="fa <?php echo $adminObj->GetMenuIcon(); ?> submenu-icon-pl"></i>
                                    <?php $cls = new $class ?>
                                    <?php echo $cls->verbose(); ?>
                                </a>
                            </li>
                        <?php endforeach; ?>

                    </ul>

                <?php
                    } // end if
                    endforeach;
                ?>

            </ul>
     </div>
</div>
