<?php
$objectAdminName = $SelectedPage . "_admin";
$object = new $SelectedPage;
$objectAdmin = new $objectAdminName;

$queryParams = $_GET;
unset($queryParams["p"]);
unset($queryParams["status"]);
unset($queryParams["msg"]);
unset($queryParams["page"]);

$paginator = new paginator();

$itemsPerPage = _ITEMS_PER_PAGE_;
$total = $object->count_queryset($queryParams);

$page = 1;
if (isset($_GET["page"])) {
    $page = $_GET["page"];
}
$totalPage = $paginator->num_pages($total,$itemsPerPage);
$start = $paginator->offset($page, $itemsPerPage);
$limit = $itemsPerPage;

$records = $object->get_queryset($queryParams,$start,$limit);

// Build queryString
$queryString = "";
foreach ($queryParams as $key => $value) {
    $queryString .= $key . '=' . $value . '&';
}
$queryString = trim($queryString, '&');

// Build pagination
$pagination = $paginator->pagination($SelectedPage, $page,$totalPage,$itemsPerPage,$queryString);

// Check if there is an alert message to be displayed
if (isset($_GET['status'])) {
    $status = $_GET['status'];
    $msg = $_GET['msg'];

    $status_icon_map = array(
        "OK" => "alert-success",
        "ERROR" => "alert-warning",
    );

    $status_label_map = array(
        "OK" => "Success!",
        "ERROR" => "Warning!",
    );

    print '<div class="alert '.$status_icon_map[$status].'">
      <strong>'.$status_label_map[$status].'</strong> '.$msg.'
    </div>';
}

$return_url = '';
if ($queryString != '') {
    $return_url = $_DELIMETER . str_replace('&', $_DELIMETER, $queryString);
}
?>

<!-- Display data table -->
<div>

    <div class="container-fluid">

        <?php if ($displayBreadcrumbs) { include _PAGEDIR_."breadcrumbs.php"; } ?>

        <div style="display: block;">
            <?php if (!empty($objectAdmin->searchFields) || !empty($objectAdmin->searchFilters)) { ?>
            <form method="GET">
                <div style="float: left; margin-bottom: 10px;">
                    <div class="form-inline">
                        <input type="hidden" name="p" class="form-control filter" value="<?php echo $_GET['p'] ?>" />
                        <input type="hidden" name="page" class="form-control filter" value="<?php echo $page ?>" />
                        <?php if (!empty($objectAdmin->searchFields)) { if(!isset($queryParams['q'])) { $queryParams['q'] = ''; } ?>
                            <input type="text" name="q" class="form-control filter" placeholder="Enter search keyword..." value="<?php echo $queryParams['q'] ?>" />
                        <?php } ?>
                        <?php if (!empty($objectAdmin->searchFilters)) {
                            foreach ($objectAdmin->searchFilters as $searchFilter) {
                                if(!isset($queryParams[$searchFilter])) { $queryParams[$searchFilter] = ''; }
                                $objectField = $object->get_field_object($searchFilter);
                                print $objectField->filter_control($searchFilter,$queryParams[$searchFilter]);
                            }
                        } ?>
                        <button type="submit" class="btn btn-default filter"><i class="fa fa-search"></i> Search</button>
                    </div>
                </div>
            </form>
            <?php } ?>
<form method="POST" action="action/delete.php" target="_self">
<input type="hidden" name="obj" value="<?php echo $SelectedPage ?>" />
            <?php if($objectAdmin->CanAdd() && $displayBreadcrumbs) { ?>
                <span style="float: right; margin-bottom: 10px;">
                    <a align="right" href="?p=<?php echo $SelectedPage ?>&page=<?php echo $page ?>&a=add&return_url=p=<?php echo $SelectedPage.$return_url ?>" class="btn btn-success"><i class="fa fa-plus"></i> Add New <?php echo $object->verbose(); ?></a>
                </span>
            <?php } // end if ?>
        </div>

        <br />
        <br />
        <table class="table table-bordered table-hover">
            <?php

            $admin = $objectAdmin;
            $columns = $admin->GetListColumns();
            $columnsCount = count($columns) + 1;

            print "<thead>";
            print "<th width='30'><input type='checkbox' onchange='checkall();' class='checker' /></th>";
            foreach ($columns as $column) {
                $col = $object->get_field_object($column);
                if ($col instanceof Field) {
                    if ($col->displayName != "") {
                        $column = $col->displayName;
                    } else {
                        $column = ucwords(str_replace("_", " ", $column));
                    }
                    print "<th>{$column}</th>";
                } else {
                    // field is something else or customly added
                    $column = ucwords(str_replace("_", " ", $column));
                    print "<th>{$column}</th>";
                }
            }
            print "</thead>";

            if (empty($records)) {
                print "<tr><td colspan='{$columnsCount}' class='text-center text-warning warning'>There are no records to be displayed.</td></tr>";
            }
            foreach ($records as $record) {
                $admin->SetObject($record);
                $columns = $admin->GetListValues();
                if ($displayBreadcrumbs) {
                    print "<tr style='cursor: pointer;' onclick='window.location.href=\"?p={$SelectedPage}&page={$page}&a=update&id={$record->id}&return_url=p={$SelectedPage}{$return_url}\"'>";
                } else {
                    print "<tr>";
                }
                $setUpdate = true;
                print "<td class='td-checkbox'><input type='checkbox' value='{$record->id}' name='{$record->id}' class='record_check' /></td>";
                foreach ($columns as $column => $displayVal) {
                    if ($setUpdate) {
                        $setUpdate = false;
                        print "<td>{$displayVal}</td>";
                    } elseif(strpos($column,"__")) {
                        $temp = explode("__",$column);
                        $function = $temp[0] . "__load";
                        $foreignObj = $record->{$function}();
                        $value = $foreignObj->{$temp[1]};
                        print "<td>{$value}</td>";
                    } else {
                        print "<td>{$displayVal}</td>";
                    }
                }
                print "</tr>";
            }

            ?>
        </table>

        <?php echo $pagination; ?>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalDeleteDialog" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete <?php echo ucwords(str_replace("_"," ",$SelectedPage)) ?></h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete the selected records?</p>
            </div>
            <div class="modal-footer">
                <button name="delete" class="btn btn-danger">Delete</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="bottom-space"></div>

<?php if (!$displayBreadcrumbs) { ?>

    <div class="add-or-update-inline-footer">
        <div class="form-group">
            <div class="col-sm-offset-10 col-sm-2">
                <button type="button" class="form-control btn btn-default" onclick="top.colorboxClose();">Close</button>
            </div>
        </div>
    </div>

<?php } ?>

<?php if($objectAdmin->CanDelete() && $displayBreadcrumbs) { ?>
    <div class="list-footer">
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalDeleteDialog">Delete Selected</button>
    </div>
<?php } // end if ?>

</form>
