<a href='../reports/receipt.php?dr&code=<?php echo $object->dr_no; ?>' target='_newtab' class="btn btn-default">
    Print
</a>

<?php
$user = new user();
$user->get($_SESSION['auth']['userid']);
if ($object->is_approved == 0 && $user->can_approve()) {
?>
<a href='action/approve_reject_dr.php?dr_id=<?php echo $object->id ?>&approve=1&return_url=<?php echo $_GET['return_url'] ?>' class="btn btn-success">
    Approve
</a>
<a href='action/approve_reject_dr.php?dr_id=<?php echo $object->id ?>&approve=0&return_url=<?php echo $_GET['return_url'] ?>' class="btn btn-danger">
    Reject
</a>
<?php
}
?>