<h3>

<?php // 1st part of breadcrumb is always the home page. ?>
<a href="?p=home">Home</a>
<i class="fa fa-angle-double-right"></i>

<?php $bcObj = new $SelectedPage;  ?>

<?php // Display 2nd part of breadcrumb. ?>
<?php // 2nd part is linked if the current page is the list of the object otherwise ?>
<?php // 2nd part is only displayed as label. ?>
<?php 
    if(!isset($_GET['a'])) {
        print "<span>{$bcObj->verbose()}</span>";
?>
<?php } else { 
        // If return_url is set then it will
        // attempt to display where it come from instead of
        // its true object.
        $bPlatform = "p={$SelectedPage}";
        $bVerbose = $bcObj->verbose();
        if (isset($_GET['return_url']) && $_GET['return_url'] != '') {
            $temp = $_GET['return_url'];
            $temp = explode("=", $temp);
            $temp = $temp[1];
            $temp = explode($_DELIMETER, $temp);
            $temp = $temp[0];
            $temp = str_replace("_"," ",$temp);
            $temp = ucwords($temp);
            
            $bPlatform = str_replace($_DELIMETER, '&', $_GET['return_url']);
            $bVerbose = $temp;
        }
        print "<a href='?{$bPlatform}&page={$_GET['page']}'>{$bVerbose}</a>";
?>
    <i class="fa fa-angle-double-right"></i>
<?php } // endif ?>

<?php // Display last part of breadcrumb. Either "add" or "update" ?>
<?php if(isset($_GET['a'])) { ?>
    <span><?php echo ucwords(str_replace("_"," ",$_GET['a'])) ?></span>
<?php } // endif ?>

</h3>
