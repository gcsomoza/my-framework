<?php
$objectAdminName = $SelectedPage . "_admin";
$object = new $SelectedPage;
$objectAdmin = new $objectAdminName;

$queryParams = $_GET;
unset($queryParams["p"]);
unset($queryParams["status"]);
unset($queryParams["msg"]);
unset($queryParams["page"]);

$paginator = new paginator();

$itemsPerPage = _ITEMS_PER_PAGE_;
$total = $object->count_queryset($queryParams);

$page = 1;
if (isset($_GET["page"])) {
    $page = $_GET["page"];
}
$totalPage = $paginator->num_pages($total,$itemsPerPage);
$start = $paginator->offset($page, $itemsPerPage);
$limit = $itemsPerPage;

$records = $object->get_queryset($queryParams,$start,$limit);

// Build queryString
$queryString = "";
foreach ($queryParams as $key => $value) {
    $queryString .= $key . '=' . $value . '&';
}
$queryString = trim($queryString, '&');

// Build pagination
$pagination = $paginator->pagination($SelectedPage,$page,$totalPage,$itemsPerPage,$queryString);

// Check if there is an alert message to be displayed
if (isset($_GET['status'])) {
    $status = $_GET['status'];
    $msg = $_GET['msg'];

    $status_icon_map = array(
        "OK" => "alert-success",
        "ERROR" => "alert-warning",
    );

    $status_label_map = array(
        "OK" => "Success!",
        "ERROR" => "Warning!",
    );

    print '<div class="alert '.$status_icon_map[$status].'">
      <strong>'.$status_label_map[$status].'</strong> '.$msg.'
    </div>';
}

$item = "";
$symbolItem = "symbol-comparison";
if (isset($_GET['item'])) {
    $item = $_GET['item'];
}
if (isset($_GET['item_operator'])) {
    $symbolItem = $SYMBOLS[$_GET['item_operator']];
}

$warehouse = "";
$symbolWh = "symbol-comparison";
if (isset($_GET['warehouse'])) {
    $warehouse = $_GET['warehouse'];
}
if (isset($_GET['warehouse_operator'])) {
    $symbolWh = $SYMBOLS[$_GET['warehouse_operator']];
}

$quantity = "";
$symbolQty = "symbol-equal";
if (isset($_GET['quantity'])) {
    $quantity = $_GET['quantity'];
}
if (isset($_GET['quantity_operator'])) {
    $symbolQty = $SYMBOLS[$_GET['quantity_operator']];
}

$return_url = '';
if ($queryString != '') {
    $return_url = $_DELIMETER . str_replace('&', $_DELIMETER, $queryString);
}
?>

<div class="container-fluid">
    <?php include _PAGEDIR_."breadcrumbs.php" ?>
<form method="GET">
    <input type="hidden" name="p" value="my_items" />
    <input type="hidden" name="page" value="<?php echo $page ?>" />
    
    <?php if($objectAdmin->CanAdd()) { ?>
        <span style="float: right; margin-bottom: 10px;">
            <a align="right" href="?p=item&page=<?php echo $page ?>&a=add&return_url=p=my_items<?php echo $return_url ?>" class="btn btn-success"><i class="fa fa-plus"></i> Create New Item</a>
        </span>
    <?php } // end if ?>

    <span style="float: right; margin-bottom: 10px;">
        <button class="btn btn-default"><i class="fa fa-search"></i> Search</button>
        &nbsp;
    </span>

    <span style="float: right; margin-bottom: 10px;">
        <button id="myItemsClearFilters" type="button" class="btn btn-default"><i class="fa fa-eraser"></i> Clear Filters</button>
        &nbsp;
    </span>

    <table class="table" style="border: 3px solid #777 !important;">
        <?php

        print "<thead style='background-color: rgba(0, 0, 0, 0.25);'>";
        print "<th width='110px'>Image<div>
            <div class='input-group fade'>
                <div class='input-group-btn'>
                    <button type='button' class='btn btn-default dropdown-toggle'
                    data-toggle='dropdown'>
                        <img src='' style='width: 25px; height: 20px'></img>
                    </button>
                </div>
                <input type='text' class='form-control' value='' />
            </div>
        </div></th>";
        print "<th width='130px'>Actions<div>
            <div class='input-group fade'>
                <div class='input-group-btn'>
                    <button type='button' class='btn btn-default dropdown-toggle'
                    data-toggle='dropdown'>
                        <img src='' style='width: 25px; height: 20px'></img>
                    </button>
                </div>
                <input type='text' class='form-control' value='' />
            </div>
        </div></th>";

        // Item

        print "<th width='250px'>Item<div>
            <div class='input-group'>
                <div class='input-group-btn'>
                    <button type='button' class='btn btn-default dropdown-toggle'
                    data-toggle='dropdown'>
                        <img id='symbol-item' data-symbol='{$symbolItem}' src='' style='width: 25px; height: 20px'></img>
                    </button>
                    <ul class='dropdown-menu'>
                        <li onclick='selectSymbol(this, \"#symbol-item\")'><a style='cursor: pointer;'><img data-operator='contains' class='symbol symbol-comparison' src='' style='width: 25px; height: 25px'></img> Contains</a></li>
                        <li onclick='selectSymbol(this, \"#symbol-item\")'><a style='cursor: pointer;'><img data-operator='!contains' class='symbol symbol-comparison-not' src='' style='width: 25px; height: 25px'></img> Does not contains</a></li>
                        <li onclick='selectSymbol(this, \"#symbol-item\")'><a style='cursor: pointer;'><img data-operator='equals' class='symbol symbol-equal' src='' style='width: 25px; height: 25px'></img> Equals</a></li>
                        <li onclick='selectSymbol(this, \"#symbol-item\")'><a style='cursor: pointer;'><img data-operator='!equals' class='symbol symbol-equal-not' src='' style='width: 25px; height: 25px'></img> Does not equals</a></li>
                    </ul>
                </div>
                <input type='text' id='symbol-item-value' name='item' class='form-control' value='{$item}' placeholder='Enter to search...' autocomplete='off' />
                <input type='hidden' id='symbol-item-operator' name='item_operator' />
            </div>
        </div></th>";

        // Warehouse

        print "<th width='230px'>Warehouse<div>
            <div class='input-group'>
                <div class='input-group-btn'>
                    <button type='button' class='btn btn-default dropdown-toggle'
                    data-toggle='dropdown'>
                        <img id='symbol-wh' data-symbol='{$symbolWh}' src='' style='width: 25px; height: 20px'></img>
                    </button>
                    <ul class='dropdown-menu'>
                        <li onclick='selectSymbol(this, \"#symbol-wh\")'><a style='cursor: pointer;'><img data-operator='contains' class='symbol symbol-comparison' src='' style='width: 25px; height: 25px'></img> Contains</a></li>
                        <li onclick='selectSymbol(this, \"#symbol-wh\")'><a style='cursor: pointer;'><img data-operator='!contains' class='symbol symbol-comparison-not' src='' style='width: 25px; height: 25px'></img> Does not contains</a></li>
                        <li onclick='selectSymbol(this, \"#symbol-wh\")'><a style='cursor: pointer;'><img data-operator='equals' class='symbol symbol-equal' src='' style='width: 25px; height: 25px'></img> Equals</a></li>
                        <li onclick='selectSymbol(this, \"#symbol-wh\")'><a style='cursor: pointer;'><img data-operator='!equals' class='symbol symbol-equal-not' src='' style='width: 25px; height: 25px'></img> Does not equals</a></li>
                    </ul>
                </div>
                <input type='text' id='symbol-wh-value' name='warehouse' class='form-control' value='{$warehouse}' placeholder='Enter to search...' autocomplete='off' />
                <input type='hidden' id='symbol-wh-operator' name='warehouse_operator' />
            </div>
        </div></th>";

        // Quantity

        print "<th width='130px'>Quantity<div>
        <div class='input-group'>
            <div class='input-group-btn'>
                <button type='button' class='btn btn-default dropdown-toggle'
                    data-toggle='dropdown'>
                        <img id='symbol-qty' data-symbol='{$symbolQty}' src='' style='width: 25px; height: 20px'></img>
                    </button>
                    <ul class='dropdown-menu'>
                        <li onclick='selectSymbol(this, \"#symbol-qty\")'><a style='cursor: pointer;'><img data-operator='equals' class='symbol symbol-equal' src='' style='width: 25px; height: 25px'></img> Equals</a></li>
                        <li onclick='selectSymbol(this, \"#symbol-qty\")'><a style='cursor: pointer;'><img data-operator='!equals' class='symbol symbol-equal-not' src='' style='width: 25px; height: 25px'></img> Does not equals</a></li>
                        <li onclick='selectSymbol(this, \"#symbol-qty\")'><a style='cursor: pointer;'><img data-operator='lt' class='symbol symbol-lt' src='' style='width: 25px; height: 25px'></img> Less than</a></li>
                        <li onclick='selectSymbol(this, \"#symbol-qty\")'><a style='cursor: pointer;'><img data-operator='lte' class='symbol symbol-lte' src='' style='width: 25px; height: 25px'></img> Less than or equal to</a></li>
                        <li onclick='selectSymbol(this, \"#symbol-qty\")'><a style='cursor: pointer;'><img data-operator='gt' class='symbol symbol-gt' src='' style='width: 25px; height: 25px'></img> Greater than</a></li>
                        <li onclick='selectSymbol(this, \"#symbol-qty\")'><a style='cursor: pointer;'><img data-operator='gte' class='symbol symbol-gte' src='' style='width: 25px; height: 25px'></img> Greater than or equal to</a></li>
                    </ul>
                </div>
                <input type='number' id='symbol-qty-value' name='quantity' class='form-control' value='{$quantity}' placeholder='' autocomplete='off' />
                <input type='hidden' id='symbol-qty-operator' name='quantity_operator' />
            </div>
        </div></th>";
        print "<th width='100px'>Unit<div>
            <div class='input-group fade'>
                <div class='input-group-btn'>
                    <button type='button' class='btn btn-default dropdown-toggle'
                    data-toggle='dropdown'>
                        <img src='' style='width: 25px; height: 20px'></img>
                    </button>
                </div>
                <input type='text' class='form-control' value='' />
            </div>
        </div></th>";
        print "</thead>";

        if (empty($records)) {
            print "<tr><td colspan='6' class='text-center text-warning warning'>There are no records to be displayed.</td></tr>";
        }
        $bgColor1 = "background-color: rgba(217,237,247,0.30);";
        $bgColor2 = "background-color: rgba(60,118,61,0.20);";
        $bgColor = $bgColor2;

        $borderTop1 = "border-top: 3px solid #777 !important;";

        $prevItemID = 0;
        foreach ($records as $record) {
            
            if ($bgColor == $bgColor1 && $prevItemID != $record['item_id'])
                $bgColor = $bgColor2;
            elseif ($bgColor == $bgColor2 && $prevItemID != $record['item_id']) {
                $bgColor = $bgColor1;
            }

            $borderTop = "";
            if ($prevItemID != $record['item_id']) {
                $borderTop = $borderTop1;
            }
            
            // if ($borderTop == $borderTop1 && $prevItemID != $record['item_id'])
            //     $borderTop = $borderTop2;
            // elseif ($bgColor == $borderTop2 && $prevItemID != $record['item_id']) {
            //     $borderTop = $borderTop1;
            // }

            $prevItemID = $record['item_id'];

            $record_json = json_encode($record);

            print "<tr style='{$bgColor}{$borderTop}'>";
            $image = new ImageField(array("value"=>$record['item_image']));
            $img = $image->display_value("item", $record['item_id'], 'image');
            print "<td>{$img}</td>";
?>
            <td>
                <input type="button" class="form-control btn btn-success btnAdditionalStock" style="color: black;" value="Additional Stock" 
                    data-imgTagID="<?php echo "#img-" . $record['item_id'] ?>" 
                    data-warehouseID="<?php echo $record['warehouse_id'] ?>" 
                    data-warehouse="<?php echo $record['warehouse'] ?>" 
                    data-itemID="<?php echo $record['item_id'] ?>" 
                    data-item="<?php echo $record['item'] ?>" 
                    data-unitID="<?php echo $record['unit_id'] ?>" 
                    data-unit="<?php echo $record['unit'] ?>" />

                <input type="button" class="form-control btn btn-warning btnAdjustment" style="color: black;" value="Adjustment" 
                    data-imgTagID="<?php echo "#img-" . $record['item_id'] ?>" 
                    data-warehouseID="<?php echo $record['warehouse_id'] ?>" 
                    data-warehouse="<?php echo $record['warehouse'] ?>" 
                    data-itemID="<?php echo $record['item_id'] ?>" 
                    data-item="<?php echo $record['item'] ?>" 
                    data-quantity="<?php echo $record['quantity'] ?>" 
                    data-unitID="<?php echo $record['unit_id'] ?>" 
                    data-unit="<?php echo $record['unit'] ?>" />

                <a href="<?php echo 'list.php?p=transaction_line&page=1&item=' . $record['item_id'] . '&transaction__warehouse=' . $record['warehouse_id']; ?>&transaction__status=processed" class="form-control btn btn-default iframe" style="color: black;" >Transactions</a>
            </td>
<?php
            print "<td><a href='?p=item&page={$page}&a=update&id={$record['item_id']}&return_url=p=my_items{$return_url}'>{$record['item']}</a></td>";
            print "<td>{$record['warehouse']}</td>";

            $qtyStyle = 'color: black;';
            if ($record['quantity'] < 1) {
                $qtyStyle = 'color: red;';
            }
            print "<td style='{$qtyStyle}'>{$record['quantity']}</td>";

            print "<td>{$record['unit']}</td>";
            print "</tr>";
        }

        ?>
    </table>

    <?php echo $pagination; ?>
</form>

<!-- Additional Stock Modal -->
<div class="modal fade" id="ModalAdditionalStock" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4>
                <img
                    class="img-rounded" 
                    style="border: 1px solid gray; width: 84px; height: 84px; max-width: 84px; max-height: 84px;"
                ></img>
                Additional Stock
            </h4>
        </div>
        <form method="POST" action="api/?method=AddItem">
            <div class="modal-body">
                <input type="hidden" name="return_url" class="form-control" />

                <input type="hidden" name="warehouse_id" class="form-control" />
                <input type="hidden" name="item_id" class="form-control" />
                <input type="hidden" name="unit_id" class="form-control" />

                <div class="form-group">
                    <label>Location</label>
                    <input type="text" id="warehouseName" class="form-control" />
                </div>
                
                <div class="form-group">
                    <label>Item</label>
                    <input type="text" id="itemName" class="form-control" />
                </div>

                <div class="form-group">
                    <label>Quantity <span style="color: red; font-size: 14px;">*</span></label>
                    <input type="number" name="quantity" class="form-control" placeholder="Enter quantity" required autocomplete="off" />
                </div>

                <div class="form-group">
                    <label>Unit of Measure</label>
                    <input type="text" id="unitName" class="form-control" />
                </div>

                <div class="form-group">
                    <label>Remarks <span style="color: red; font-size: 14px;">*</span></label>
                    <textarea name="remarks" rows="5" class="form-control" placeholder="Enter remarks" required></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
    </div>
</div>

<!-- Adjustment Modal -->
<div class="modal fade" id="ModalAdjustment" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4>
                <img
                    class="img-rounded" 
                    style="border: 1px solid gray; width: 84px; height: 84px; max-width: 84px; max-height: 84px;"
                ></img>
                Adjustment
            </h4>
        </div>
        <form method="POST" action="api/?method=Adjustment">
            <div class="modal-body">
                <input type="hidden" name="return_url" class="form-control" />

                <input type="hidden" name="warehouse_id" class="form-control" />
                <input type="hidden" name="item_id" class="form-control" />
                <input type="hidden" name="unit_id" class="form-control" />

                <div class="form-group">
                    <label>Location</label>
                    <input type="text" id="warehouseName" class="form-control" />
                </div>
                
                <div class="form-group">
                    <label>Item</label>
                    <input type="text" id="itemName" class="form-control" />
                </div>

                <div class="form-group">
                    <label>Current Quantity</label>
                    <input type="number" id="quantity" name="quantity_old" class="form-control" />
                </div>

                <div class="form-group">
                    <label>New Quantity <span style="color: red; font-size: 14px;">*</span></label>
                    <input type="number" name="quantity" class="form-control" placeholder="Enter quantity" required autocomplete="off" />
                </div>

                <div class="form-group">
                    <label>Unit of Measure</label>
                    <input type="text" id="unitName" class="form-control" />
                </div>

                <div class="form-group">
                    <label>Remarks <span style="color: red; font-size: 14px;">*</span></label>
                    <textarea name="remarks" rows="5" class="form-control" placeholder="Enter remarks" required></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
    </div>
</div>

</div>
