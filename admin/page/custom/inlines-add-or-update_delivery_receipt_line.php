<?php

$return_url = "p={$parent}&a=update&id={$parent_id}&tab={$inline}";
if (isset($_GET['return_url']) && $_GET['return_url'] != '') {
    $return_url = "p={$parent}&a=update&id={$parent_id}&return_url={$_GET['return_url']}&tab={$inline}";
}

$options_link = _PROTOCOL_ . _HOST_ . _US_ . _APPURL_ . _US_ . "action" . _US_ . "options.php";
$options_link_availableQty = _PROTOCOL_ . _HOST_ . _US_ . _APPURL_ . _US_ . "action" . _US_ . "options-availableQty.php";

$object_admin_name = $parent . "_admin";
$object_admin = new $object_admin_name;

$page = 1;
if (isset($_GET['page'])) {
    $page = $_GET['page'];
}
?>

<html>

<head>

<meta charset="utf-8" >
<meta name="viewport" content="width=device-width, initial-scale=1" >
<title><?php echo _TITLE_ ?></title>

<?php include "../static/css/import.php" ?>
<?php include "../static/lib/import-css.php" ?>

</head>

<body>

<!-- Display add or update controls -->
<form id="frmDRL" method="POST" action="../action/save.php" target="_parent">
    <input type="hidden" name="page" value="<?php echo $page; ?>" />

    <input type="hidden" id="object" value="<?php echo $inline; ?>" />
    <input type="hidden" name="return_url" value="<?php echo $return_url; ?>" />
    <input type="hidden" id="options_link" value="<?php echo $options_link; ?>" />
    <input type="hidden" id="options_link_availableQty" value="<?php echo $options_link_availableQty; ?>" />
    <div>
        <div class="container-fluid">

            <?php
                $object = new $inline;
                print "<h3>{$object->verbose()}</h3>"
            ?>

            <hr/>
            <!-- Input Controls -->
            <?php

            if (isset($_GET['inline'])) {
                
                $a = (isset($_GET['inline_action'])) ? $_GET['inline_action'] : '';
                $id = (isset($_GET['inline_id'])) ? $_GET['inline_id'] : '';

                if (($a != '') && ($a == 'update')) {
                    if (($id != '') && is_numeric($id)) {
                        // Retrieve user data
                        $object->get($id);
                    }
                }

                $fields = $object->get_fields();
                
                print "<input type='hidden' id='object' name='object' value='{$inline}' />";
                foreach ($fields as $fieldName => $fieldObject) {
                    if ($fieldName == "id") {
                        print "<input type='hidden' id='id' name='id' value='{$fieldObject->get_value()}' />";
                    } elseif ($fieldObject instanceof ForeignKey && $fieldObject->modelName == $parent & $fieldObject->modelParent == true) {
                        $fieldObject->set_value($parent_id);
                        $fieldObject->readonly = true;
                        print $fieldObject->input_control($inline,$fieldName,$a,true);
                    } elseif ($fieldName != "is_deleted") {
                        $isDisplayed = ($fieldObject->canAdd && $a == "add") || ($fieldObject->canUpdate && $a == "update");
                        if ($isDisplayed) {
                            // Display input controls
                            if ($fieldObject instanceof ForeignKey && $fieldObject->dependsOn != "") {
                                // foreignkey field is dependent on another field to display its value.
                                $fieldObject->linkerDependeeValue = $fields[$fieldObject->dependsOn]->get_value();
                            }
                            print $fieldObject->input_control($inline,$fieldName,$a,true);
                        }
                    }
                }
            }
            ?>
            <!-- Many-to-Many Dual Listbox -->
            <?php
            foreach ($object->m2m as $foreign_object_name => $m2m_field) {
                print "<hr />";
                // get other object name from m2m index. eg. m2m['group_user']
                $other_object_name = $m2m_field->otherObjectName;

                // initialize other object
                // fetch all serial number of selected item in selected warehouse
                $sn = new serial_number();
                $existingOptions = array();
                $other_object_list = $object->item__obj()->serial_number__set(
                    $sn->warehouse__exact($object->warehouse_From)
                );

                foreach ($other_object_list as $other_obj) {
                    $existingOptions[$other_obj->id] = $other_obj->id;
                }

                // fetch the selected serial number and add it to the $other_object_list
                $drlsnObj = new delivery_receipt_line_serial_number();
                $drlsns = $drlsnObj->filter(
                    $drlsnObj->delivery_receipt_line__exact($object->id)
                )->fetch();
                
                foreach ($drlsns as $drlsn) {
                    $objID = $drlsn->serial_number__obj()->id;
                    if (!isset($existingOptions[$objID])) {
                        $other_object_list[] = $drlsn->serial_number__obj();
                    }
                }

                // get selected objects
                // get the selected serial number from the list of serial number
                $selected_objects = $object->{"{$foreign_object_name}__set"}();
                $selected_objects_ids = array();
                foreach ($selected_objects as $key => $selected_objects_item) {
                    $selected_objects_ids[$selected_objects_item->$other_object_name] = 'selected';
                }

                // display duallistbox
                $displayName = ($m2m_field->displayName == '') ? ucwords(str_replace("_"," ",$foreign_object_name)) : $m2m_field->displayName;

                $helpText = '';
                if ($m2m_field->helpText != '') {
                    $helpText = "
                        <div style='margin-top: -5px; margin-bottom: 5px;'><i class='fa fa-question-circle'></i> <small>{$m2m_field->helpText}</small></div>
                    ";
                }

                print '<h3>'.ucwords(str_replace("_"," ",$displayName)).'</h3>';
                print $helpText;
                print '<input type="hidden" name="m2m_'.$foreign_object_name.'[]" />'; // this will handle posting an empty m2m
                print '<select multiple="multiple" size="10" id="m2m_'.$foreign_object_name.'" name="m2m_'.$foreign_object_name.'[]" class="duallistbox">';
                foreach ($other_object_list as $list_index => $list_item) {
                    $selected = "";
                    if (isset($selected_objects_ids[$list_item->id])) {
                        $selected = $selected_objects_ids[$list_item->id];
                    }
                    print '<option value="'.$list_item->id.'" '.$selected.'>'.$list_item.'</option>';
                } // end foreach
                print '</select>';
            }
            ?>
        </div>
    </div>

    <div class="bottom-space"></div>

    <div class="add-or-update-inline-footer">
        <div class="form-group">
            <div class="col-sm-8">
                <div id="divInvalidQtySn" style="font-size: 24px;" class="alert-danger text-center fade out"></div>
            </div>
            <div class="col-sm-2">
                <?php if ($a == "add") { ?>
                <button name="save" class="form-control btn btn-primary">Save</button>
                <?php } ?>
            </div>
            <div class="col-sm-2">
                <button type="button" class="form-control btn btn-default" onclick="top.colorboxClose();">Close</button>
            </div>
        </div>
    </div>
</form>

</body>

<?php include "../static/js/import.php" ?>
<?php include "../static/lib/import-js.php" ?>

<?php display_consolelogs(); ?>

</html>