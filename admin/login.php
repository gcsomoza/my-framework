<!DOCTYPE html>

<?php

include "settings.php";

$username = "";
$password = "";
if(!empty($_POST)) {
    $username = $_POST["username"];
    $password = $_POST["password"];
}
?>

<html>

<head>

<meta charset="utf-8" >
<title><?php echo _TITLE_ ?></title>

<?php include "static/css/import.php" ?>
<?php include "static/lib/import-css.php" ?>

</head>

<body>

    <div class="container">
        <div class="row login-mt">
            <div class="col-sm-12 col-md-offset-3 col-md-6">

                <div class="panel panel-default">

                    <div class="panel-heading">
                        <h4>Login</h4>
                    </div>

                    <div class="panel-body">
                        <form method="POST" action="action/login.php">
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" id="username" name="username" placeholder="Username" />
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password" />
                            </div>
                            <div class="form-group">
                                <input type="submit" class="form-control btn btn-primary" id="submit" value="Login" />
                            </div>
                        </form>
                    </div>

                    <?php
                        if(isset($_GET["invalid"])) {
                    ?>

                    <div class="panel-footer">
                        <div class="alert alert-warning text-center">
                            <strong>Warning!</strong> Invalid username or password.
                        </div>
                    </div>

                    <?php
                        }
                    ?>

                </div>
            </div>
        </div>
    </div>

</body>

<?php include "static/js/import.php" ?>
<?php include "static/lib/import-js.php" ?>

</html>
