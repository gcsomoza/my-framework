<?php

$files = ScanDirectory(_STATICDIR_."css"._DS_);
$url = _STATICURL_."css"._US_;
foreach ($files as $file) {
    $ext = pathinfo($file, PATHINFO_EXTENSION);
    if ($ext == "css") {
        $href = _PROTOCOL_ . _HOST_ . _US_ . _APPURL_ . _US_ . "static/css/" . $file;
        print "<link rel='stylesheet' type='text/css' href='{$href}'>\n";
    }
}

?>
