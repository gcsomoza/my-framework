$(document).ready(function(){

    $("#mainBody").addClass("in");

    $(".chosen-select").chosen({width: '100%'});

    function unhideChosenSelect() {
        // Chosen sets chosen-select elements to display: none.
        // Can't use the default validation that way.
        $(".chosen-select").attr('style', 'display: block; opacity: 0; margin-bottom: -12px; height: 0px; width: 0px;');
    }

    $('.datepicker').datetimepicker({
        useCurrent: false,
        format: 'MMM DD, YYYY'
    });

    $('.datetimepicker').datetimepicker({
        useCurrent: false,
        format: 'MMM DD, YYYY h:mm A'
    });

    $('.duallistbox').bootstrapDualListbox({
        moveOnSelect: false,
    });

    $('.daterange').daterangepicker({
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });

    $('.daterange').on('keydown', function(e) {
        e.preventDefault();
        return false;
    });

    // Apply selected dates to daterangepicker
    $('.daterange').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(
            picker.startDate.format('MM/DD/YYYY')
            + ' - ' +
            picker.endDate.format('MM/DD/YYYY')
        );
    });

    // Clear daterangepicker
    $('.daterange').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $(".iframe").colorbox({
        iframe:true,
        width:"90%",
        height:"90%",
        overlayClose:false,
        closeButton:false
    });

    $(".td-checkbox").click(function(e) {
        e.stopPropagation();
    });

    $(".dependent").each(function() {
        
        options_link         = $("#options_link").val();
        dependee_selectid    = $(this).data("dependson");
        dependent_selectid   = $(this).attr('id');
        linkerObj            = $(this).data("linkerobj");
        linkerDependeeField  = $(this).data("linkerdependeefield");
        linkerDependentField = $(this).data("linkerdependentfield");

        dependee_id = "#" + dependee_selectid;
        
        // Add dependent element data attributes to dependee element
        dependee = $(dependee_id);
        dependee.data("ajxDependentSelectedID", dependent_selectid);
        dependee.data("ajxLinkerObj", linkerObj);
        dependee.data("ajxLinkerDependeeField", linkerDependeeField);
        dependee.data("ajxLinkerDependentField", linkerDependentField);
        
        $(dependee_id).chosen().change(function() {
            var self = $(this);

            // Get parameters to be passed in
            // getting options api.
            var dependent_selectid = self.data("ajxDependentSelectedID");
            var linkerObj = self.data("ajxLinkerObj");
            var linkerDependeeField = self.data("ajxLinkerDependeeField");
            var linkerDependentField = self.data("ajxLinkerDependentField");

            if (false) {
                console.log(
                    "action/options.php?linkerObj="+linkerObj+
                    "&linkerDependeeField="+linkerDependeeField+
                    "&linkerDependeeValue="+$(this).val()+
                    "&linkerDependentField="+linkerDependentField
                );
            }
            
            // fetch possible options for dependent select control
            $('#'+dependent_selectid+'-spinner').addClass('in');
            dependee_value = $(this).val();
            $.post(
                options_link,
                {
                    linkerObj: linkerObj,
                    linkerDependeeField: linkerDependeeField,
                    linkerDependeeValue: dependee_value,
                    linkerDependentField: linkerDependentField
                },
                function(data){
                    var $el = $('#'+dependent_selectid+' option:gt(0)');
                    $el.remove();
                    $el = $('#'+dependent_selectid);
                    eval('var newOptions = ' + data + ';')
                    $.each(newOptions, function(value,text) {
                        $el.append($("<option></option>").attr("value", value).text(text));
                    });
                    $('#'+dependent_selectid+'-spinner').removeClass('in');
                    $el.trigger("chosen:updated");
                }
            );
        });

        $(dependee_id).chosen().change();
    });

    $('.chosen-select').chosen();

    $('.record_check').click(function(e){
        e.stopPropagation();
    });

    $('.btnAdditionalStock').click(function(e) {
        var self = $(this);

        var imgTagID = self.attr("data-imgTagID");

        var warehouseID = self.attr("data-warehouseID");
        var warehouse = self.attr("data-warehouse");

        var itemID = self.attr("data-itemID");
        var item = self.attr("data-item");

        var unitID = self.attr("data-unitID");
        var unit = self.attr("data-unit");

        var modalAdditionalStock = $('#ModalAdditionalStock');

        modalAdditionalStock.find('input[name="return_url"]').val(getUrlVars());

        modalAdditionalStock.find("img").attr("src", $(imgTagID).attr("src"));

        modalAdditionalStock.find('input[name="warehouse_id"]').val(warehouseID);
        modalAdditionalStock.find('input[name="item_id"]').val(itemID);
        modalAdditionalStock.find('input[name="unit_id"]').val(unitID);

        modalAdditionalStock.find('#warehouseName').val(warehouse);
        modalAdditionalStock.find('#itemName').val(item);
        modalAdditionalStock.find('#unitName').val(unit);

        modalAdditionalStock.find('textarea').val('');

        var inputQty = modalAdditionalStock.find('input[name="quantity"]');
        inputQty.val('');
        inputQty.focus();

        modalAdditionalStock.modal("toggle");
    });

    $('.btnAdjustment').click(function(e) {
        var self = $(this);

        var imgTagID = self.attr("data-imgTagID");

        var warehouseID = self.attr("data-warehouseID");
        var warehouse = self.attr("data-warehouse");

        var itemID = self.attr("data-itemID");
        var item = self.attr("data-item");

        var quantity = self.attr("data-quantity");

        var unitID = self.attr("data-unitID");
        var unit = self.attr("data-unit");

        var modal = $('#ModalAdjustment');

        modal.find('input[name="return_url"]').val(getUrlVars());

        modal.find("img").attr("src", $(imgTagID).attr("src"));

        modal.find('input[name="warehouse_id"]').val(warehouseID);
        modal.find('input[name="item_id"]').val(itemID);
        modal.find('input[name="unit_id"]').val(unitID);

        modal.find('#warehouseName').val(warehouse);
        modal.find('#itemName').val(item);
        modal.find('#quantity').val(quantity);
        modal.find('#unitName').val(unit);

        modal.find('textarea').val('');

        var inputQty = modal.find('input[name="quantity"]');
        inputQty.val('');
        inputQty.focus();

        modal.modal("toggle");
    });

    $('.btnTransactions').click(function(e) {
        var self = $(this);

        var warehouseID = self.attr("data-warehouseID");

        var itemID = self.attr("data-itemID");

        var unitID = self.attr("data-unitID");

        alert(warehouseID + " " + itemID + " " + unitID);
    });

    // $('.dropdownInventoryAction').click(function() {
    //     var self = $(this);
    //     var action = self.data("action");
    //     var record = self.data("record");

    //     var quantityBox = self.parent().parent().parent().parent().find("input");
    //     var alertBox = self.parent().parent().parent().parent().parent().find(".alert");
    //     var quantityCol = self.parent().parent().parent().parent().parent().parent().find('td').eq(4);

    //     var quantity = quantityBox.val();
    //     if (isNaN(quantity) === false && quantity != '' && quantity > 0) {
    //         var url = "api/?method=" + action;
    //         if (action == "DeductItem") {

    //             quantityBox.val("");
    //             quantityBox.focus();
    
    //             alertBox.html("<span class='alert-danger'>Too much!</span>");
                
    //             if (parseFloat(quantity) > parseFloat(quantityCol.text())) {
    //                 return
    //             }
    //         }
            
    //         $.post(
    //             url,
    //             {
    //                 data: JSON.stringify(record),
    //                 quantity: quantity
    //             },
    //             function(data){
    //                 data = JSON.parse(data);
    //                 if (data["status"] == "OK") {

    //                     quantityBox.val("");
    //                     quantityBox.focus();

    //                     var msg = ""
    //                     if (action == "AddItem") {
    //                         msg = "<span class='alert-success'>Added!</span>";
    //                     } else if (action == "DeductItem") {
    //                         msg = "<span class='alert-warning'>Deducted!</span>";
    //                     }
    //                     alertBox.html(msg);
                        
    //                     var qty = 0
    //                     if (isNaN(data['qty']) == false) {
    //                         qty = data['qty'];
    //                     }
    //                     quantityCol.text(qty);

    //                     if (quantityCol.text() == '') {
    //                         quantityCol.text('0');
    //                     }

    //                 } else {
    //                     var msg = "<span class='alert-danger'>"+data['msg']+"</span>";
    //                     alertBox.html(msg);

    //                     quantityBox.val("");
    //                     quantityBox.focus();
    //                 }
    //             }
    //         );
    //     } else {

    //         quantityBox.val("");
    //         quantityBox.focus();

    //         alertBox.html("<span class='alert-danger'>Invalid input!</span>");
    //     }
    // });

    symbolLoadIcon('symbol-comparison');
    symbolLoadIcon('symbol-comparison-not');
    symbolLoadIcon('symbol-equal');
    symbolLoadIcon('symbol-equal-not');
    symbolLoadIcon('symbol-lt');
    symbolLoadIcon('symbol-lte');
    symbolLoadIcon('symbol-gt');
    symbolLoadIcon('symbol-gte');

    loadSelectedSymbol($('#symbol-item'));
    loadSelectedSymbol($('#symbol-wh'));
    loadSelectedSymbol($('#symbol-qty'));

    $('#myItemsClearFilters').click(function (e) {
        $("#symbol-item-value").val("");
        $("#symbol-item").data("symbol", "symbol-comparison");
        loadSelectedSymbol($('#symbol-item'));

        $("#symbol-wh-value").val("");
        $("#symbol-wh").data("symbol", "symbol-comparison");
        loadSelectedSymbol($('#symbol-wh'));

        $("#symbol-qty-value").val("");
        $("#symbol-qty").data("symbol", "symbol-equal");
        loadSelectedSymbol($('#symbol-qty'));
    });

    // Update options of serial numbers in Delivery Receipt Line
    // whenever selecting new item from the choices.
    var m2m_drline = $("#m2m_delivery_receipt_line_serial_number");
    if (typeof m2m_drline.html() != 'undefined') {

        $('#frmDRL').submit(function(e) {

            var ok = evaluateDRLInput();
            if (!ok) {
                e.preventDefault();
            }
            return ok;
        });

        $("#item").change(function() {
            
            var options_link = $("#options_link").val();
            var options_link_availableQty = $("#options_link_availableQty").val();

            var self = $(this);
            var valueItem = $(this).val();
            var valueWarehouseFrom = $("#warehouse_From").val();
            
            $.post(
                options_link,
                {
                    linkerObj: 'serial_number',
                    linkerDependeeField: 'item,warehouse',
                    linkerDependeeValue: valueItem+','+valueWarehouseFrom,
                    linkerDependentField: 'serial_number'
                },
                function(data){
                    var $el = $('#m2m_delivery_receipt_line_serial_number option');
                    $el.remove();
                    $el = $('#m2m_delivery_receipt_line_serial_number');
                    eval('var newOptions = ' + data + ';')
                    $.each(newOptions, function(value,text) {
                        $el.append($("<option></option>").attr("value", value).text(text));
                    });
                    $('#serial_number-spinner').removeClass('in');
                    $el.bootstrapDualListbox('refresh');
                }
            );

            // Popullate available textbox
            $.post(
                options_link_availableQty,
                {
                    form: 'dr',
                    item: valueItem,
                    warehouse: valueWarehouseFrom
                },
                function(data){
                    var $el = $('#available').val(data);
                }
            );
        });
    }

    // Update options of serial numbers in Return Form Line
    // whenever selecting new delivery receipt line from the choices.
    var m2m_rfline = $("#m2m_return_form_line_serial_number");
    if (typeof m2m_rfline.html() != 'undefined') {

        $('#frmRFL').submit(function(e) {
            var ok = evaluateRFLInput();
            if (!ok) {
                e.preventDefault();
            }
            return ok;
        })

        $("#delivery_receipt_line").change(function() {
            
            var options_link = $("#options_link").val();
            var options_link_availableQty = $("#options_link_availableQty").val();

            var self = $(this);
            var value = $(this).val();

            $.post(
                options_link,
                {
                    linkerObj: 'delivery_receipt_line_serial_number',
                    linkerDependeeField: 'delivery_receipt_line',
                    linkerDependeeValue: value,
                    linkerDependentField: 'serial_number'
                },
                function(data){
                    var $el = $('#m2m_return_form_line_serial_number option');
                    $el.remove();
                    $el = $('#m2m_return_form_line_serial_number');
                    eval('var newOptions = ' + data + ';')
                    $.each(newOptions, function(value,text) {
                        $el.append($("<option></option>").attr("value", value).text(text));
                    });
                    $('#serial_number-spinner').removeClass('in');
                    $el.bootstrapDualListbox('refresh');
                }
            );

            // Popullate available textbox
            $.post(
                options_link_availableQty,
                {
                    form: 'rf',
                    delivery_receipt_line: value
                },
                function(data){
                    var $el = $('#available').val(data);
                }
            );
        });
    }

    unhideChosenSelect();
});

// Evaluates the input from Return Form Line.
// Checks whether the number of serial numbers selected
// is not greater than the specified quantity.
function evaluateRFLInput() {
    var result = false;
    var msg = "";

    var available = $("#available").val();
    var quantity = $("#quantity").val();
    var countSn = $("#m2m_return_form_line_serial_number :selected").length;

    if ((isNaN(quantity) == false && quantity != '') &&
        (isNaN(available) == false && available != '')) {
        
        available = parseFloat(available);
        quantity = parseFloat(quantity);

        // Check availability.
        if (available > 0) {
            // Check if quantity is within the available.
            if (quantity <= available &&
                result == false) {
                result = true
            } else {
                msg = "Cannot save! Quantity is greater than available.";
            }
        } else {
            msg = "Cannot save! Selected item already returned.";
        }

        // Selected item have serial numbers.
        if ($('#m2m_return_form_line_serial_number option').length > 0 &&
            result == false) {
            
            // Quantity should be equal to selected serial numbers.
            if (countSn == quantity &&
                result == false) {
                result = true;
            } else {
                msg = "Cannot save! Select "+ quantity +" `Serial Numbers` first.";
            }
        }
    } else {
        msg = "Invalid quantity entered!";
    }

    if (result) {
        $("#divInvalidQtySn").removeClass("in");
        $("#divInvalidQtySn").addClass("out");
    } else {
        $("#divInvalidQtySn").html(msg);
        $("#divInvalidQtySn").removeClass("out");
        $("#divInvalidQtySn").addClass("in");
    }
    
    return result;
}

// Evaluates the input from Delivery Receipt Line.
// Checks whether the number of serial numbers selected
// is not greater than the specified quantity.
function evaluateDRLInput() {
    var result = false;
    var msg = "";

    var available = $("#available").val();
    var quantity = $("#quantity").val();
    var countSn = $("#m2m_delivery_receipt_line_serial_number :selected").length;

    if ((isNaN(quantity) == false && quantity != '') &&
        (isNaN(available) == false && available != '')) {
        
        available = parseFloat(available);
        quantity = parseFloat(quantity);

        // Check availability.
        if (available > 0) {
            // Check if quantity is within the available.
            if (quantity <= available &&
                result == false) {
                result = true
            } else {
                msg = "Cannot save! Quantity is greater than available.";
            }
        } else {
            msg = "Cannot save! Selected item is not available.";
        }

        // Selected item have serial numbers.
        if ($('#m2m_delivery_receipt_line_serial_number option').length > 0 &&
            result == false) {
            
            // Quantity should be equal to selected serial numbers.
            if (countSn == quantity &&
                result == false) {
                result = true;
            } else {
                msg = "Cannot save! Select "+ quantity +" `Serial Numbers` first.";
            }
        }
    } else {
        msg = "Invalid quantity entered!";
    }

    if (result) {
        $("#divInvalidQtySn").removeClass("in");
        $("#divInvalidQtySn").addClass("out");
    } else {
        $("#divInvalidQtySn").html(msg);
        $("#divInvalidQtySn").removeClass("out");
        $("#divInvalidQtySn").addClass("in");
    }
    
    return result;
}

function setSymbolValue(from, to) {
    to.val(from.data('operator'));
}

function copySrc(from, to) {
    to.attr('src', from.attr('src'));
}

function loadSelectedSymbol(e) {
    copySrc($('#'+e.data('symbol')) ,e);
    setSymbolValue($('#'+e.data('symbol')), $('#'+e.attr('id')+'-operator'));
}

function selectSymbol(e, id) {
    copySrc($(e).find('a').find('img'), $(id));
    setSymbolValue($(e).find('a').find('img'), $(id+'-operator'));
}

function symbolLoadIcon(name) {
    copySrc($('#'+name), $('.'+name));
}

function checkall(post_class_name) {
    var postClassName = "";
    if (typeof post_class_name !== "undefined") {
        postClassName = '_' + post_class_name;
    }
    if ($(".checker" + postClassName).prop('checked') == true) {
        $(".record_check" + postClassName).prop('checked', true);
    } else {
        $(".record_check" + postClassName).prop('checked', false);
    }
}

function colorboxClose() {
    $.colorbox.close()
}

var inlineDeleteSent = false;
function inlineDelete(action,obj_name,return_url_val) {
    if (inlineDeleteSent == false) {
        post_data = { obj:obj_name, return_url:return_url_val, inline_delete:1 };
        $('input:checkbox.record_check_' + obj_name).each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if(sThisVal != "") {
                post_data[sThisVal] = sThisVal;
            }
        });
        $.post(action, post_data, function(redirect_to){
            window.location.href = redirect_to;
        });
        inlineDeleteSent = true;
    }
}

function readURL(input,img_id) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(img_id).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(".imagefield").click(function(e) {
    // Get the modal
    var modal = document.getElementById('ImageViewer');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = this;
    var modalImg = document.getElementById("ImageViewerImage");
    var modalObj = document.getElementById("ImageViewerObject");
    var modalID = document.getElementById("ImageViewerID");
    var modalField = document.getElementById("ImageViewerField");

    modalImg.src = this.src;
    modalObj.value = $(this).data("object");
    modalID.value = $(this).data("id");
    modalField.value = $(this).data("field");

    $("#alertImageUpload").html("");

    $('#ImageViewer').modal("toggle");

    e.stopPropagation();
})

// Using <a> will propagate to click event of table row in list.php.
$('.divHref').click(function(e) {
    var url = $(this).data("href");
    var win = window.open(url, '_blank');
    win.focus();

    e.stopPropagation();
});

function updateForeignKeyOptions(field,value,text) {
    // Append the newly added option and then select it.
    $("#"+field).append($('<option>',{
        value: value,
        text: text,
        selected: true
    }));
    $("#"+field).trigger("chosen:updated");
    
    // Close the iframe
    colorboxClose();
}

function startImageUpload() {
    $("#alertImageUpload").html("");
    document.getElementById('loaderImageUpload').style.visibility = 'visible';
    return true;
}

function stopImageUpload(status, msg) {
    var ico = "warning";
    var txt = msg;
    var htm = "";
    if (status == "ERROR") {
        ico = "danger";
    } else if (status == "OK") {
        ico = "success";

        // Preview image on original record
        var imgID = 'img-' + document.getElementById("ImageViewerID").value;
        document.getElementById(imgID).src = document.getElementById('ImageViewerImage').src;
    }
    htm = "<div class='alert alert-"+ico+" text-center'>"+msg+"</div>";

    document.getElementById('loaderImageUpload').style.visibility = 'hidden';

    $("#alertImageUpload").html(htm);
}

// Read a page's GET URL variables and return them as string.
function getUrlVars()
{
    // var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

    var result = "";
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        // vars.push(hash[0]);
        // vars[hash[0]] = hash[1];

        // status and msg should be removed.
        if (hash[0] != "status" && hash[0] != "msg") {
            result += hashes[i] + "^_^";
        }
    }
    return result.slice(0, -3);
}