<?php

$dir = _STATICDIR_."lib"._DS_;
$folders = ScanDirectory($dir);

foreach ($folders as $folder) {

    $files = ScanDirectory($dir._DS_.$folder._DS_."css"._DS_);
    // $url = _STATICURL_."lib"._US_.$folder._DS_."css"._DS_;
    $url = _PROTOCOL_ . _HOST_ . _US_ . _APPURL_ . _US_ . "static/lib"._US_.$folder._US_."css"._US_;
    foreach ($files as $file) {
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        if ($ext == "css") {
            $href = $url . $file;
            print "<link rel='stylesheet' type='text/css' href='{$href}'>\n";
        }
    }
}

?>
