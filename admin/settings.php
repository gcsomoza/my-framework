<?php

include "include/extras.php";
include "include/admin.php";
include "include/model.php";
include "include/context.php";
include "include/paginator.php";
include "include/uploader.php";

/* Anything about dates */
date_default_timezone_set("Asia/Manila");
define("_DATEFORMAT_", "M d, Y");
define("_DATETIMEFORMAT_", "M d, Y h:i A");
define("_MYSQL_DATEFORMAT_", "Y-m-d");
define("_MYSQL_DATETIMEFORMAT_", "Y-m-d H:i:s");

/* Anything about paths */
define("_DS_", DIRECTORY_SEPARATOR);
define("_US_", '/');

define("_APP_", GetApp());

$_DELIMETER = '^_^';

// Refine title
$title = _APP_;
$temp = explode(_DS_, _APP_);
$n = count($temp);
if ($n > 0) {
    $title = $temp[$n - 1];
}
$title = ucwords(str_replace("_"," ",str_replace("-"," ",$title)));

define("_TITLE_", $title);

define("_HOST_", $_SERVER['HTTP_HOST']);
define("_PROTOCOL_", strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,strpos( $_SERVER["SERVER_PROTOCOL"],'/'))).'://');

define("_APPPATH_", _APP_ . _DS_ . 'admin');
define("_APPURL_", _APP_ . _US_ . 'admin');

$request_scheme = $_SERVER['REQUEST_SCHEME'] . '://';
$request_scheme = ($request_scheme == '://') ? "http://" : $request_scheme;

define("_DIR_", $_SERVER['DOCUMENT_ROOT'] . _APPPATH_ . _DS_);
define("_URL_", $request_scheme . $_SERVER['HTTP_HOST'] . _APPURL_ . _US_);

define("_STATICDIR_", _DIR_ . "static" . _DS_);
define("_STATICURL_", _URL_ . "static" . _US_);

define("_ACTIONDIR_", _DIR_ . "action" . _DS_);
define("_ACTIONURL_", _URL_ . "action" . _US_);

define("_MEDIADIR_", _DIR_ . "media" . _DS_);
define("_MEDIAURL_", _URL_ . "media" . _US_);

define("_PAGEDIR_", _DIR_ . "page" . _DS_);
define("_PAGEURL_", _URL_ . "page" . _US_);

define("_MODELSDIR_", _DIR_ . "models" . _DS_);
define("_MODELSURL_", _URL_ . "models" . _US_);

define("_ITEMS_PER_PAGE_", 10);

IncludeModels();

/* Main menu icon definition */
$modules = array(
    "authorization" => array(
        "icon" => "fa-lock",
    ),
);

include "include/utilities.php";

?>
