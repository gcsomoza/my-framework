<?php

include "settings.php";

$modules = GetModelsForCreateDB();

// Create schema
$db = new DB();
$db->connect_nodb();

$create_schema = "--------------------<br/>Database <strong>`"._DBNAME_."`</strong> ";
$result = $db->create_schema(_DBNAME_);
if($result === false) {
    $create_schema .= " {$db->error()}.";
} else {
    $create_schema .= " successfully created.";
}
$create_schema .= "<br/><br/>";
print $create_schema;

$db->close();

//* Create tables
foreach($modules as $module) {
    foreach($module as $model) {
        if (property_exists($model,"maintainable")) {
            $obj = new $model;
            print $obj->create_table();
        }
    }
}
//*/

?>
