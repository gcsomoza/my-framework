<!DOCTYPE html>
<?php

session_start();

include "settings.php";

$SYMBOLS = array(
    "contains" => "symbol-comparison",
    "!contains" => "symbol-comparison-not",
    "equals" => "symbol-equal",
    "!equals" => "symbol-equal-not",
    "lt" => "symbol-lt",
    "lte" => "symbol-lte",
    "gt" => "symbol-gt",
    "gte" => "symbol-gte",
);

$OPERATORS = array(
    "contains" => "LIKE",
    "!contains" => "LIKE",
    "equals" => "=",
    "!equals" => "=",
    "lt" => "<",
    "lte" => "<=",
    "gt" => ">",
    "gte" => ">=",
);

if(!isset($_SESSION['auth'])) {
    session_destroy();
    header("Location: login.php");
    exit;
}

$displayBreadcrumbs = false;

?>

<html>

<head>

<meta charset="utf-8" >
<meta name="viewport" content="width=device-width, initial-scale=1" >
<title><?php echo _TITLE_ ?></title>

<?php include "static/css/import.php" ?>
<?php include "static/lib/import-css.php" ?>

</head>

<body id="mainBody" class="fade">

    <!--nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="?p=home"><?php echo _TITLE_ ?></a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li><a href=""><span class="glyphicon glyphicon-user"></span> Welcome <?php echo $_SESSION['auth']['username'] ?>! </a></li>
            <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
        </ul>
        </div>
    </nav-->

    <?php
        $SelectedFolder = _PAGEDIR_;
        $SelectedPage = "home";

        if(isset($_GET['p']) && $_GET['p'] != '') {
            $SelectedPage = $_GET['p'];
        }

        if(isset($_GET['a'])) {
            $customPage = $SelectedFolder."custom"._DS_."add-or-update-".$SelectedPage.".php";
            if(file_exists($customPage)) {
                $SelectedPageDir = $customPage;
            } else {
                $SelectedPageDir = $SelectedFolder . "add-or-update.php";
            }
        } else {
            $customPage = $SelectedFolder."custom"._DS_."list-".$SelectedPage.".php";
            if(file_exists($customPage)) {
                $SelectedPageDir = $customPage;
            } else {
                $SelectedPageDir = $SelectedFolder . "list.php";
            }
        }
        if (!file_exists($SelectedPageDir) || $SelectedPage == 'home') {
            $SelectedPageDir = _PAGEDIR_ . "home.php";
        }
    ?>

    <div class="row index-mt">
        <!--div class="col-sm-2">
            <?php // include _PAGEDIR_."sidebar.php"; ?>
        </div-->
        <div class="col-sm-12">
            <?php include $SelectedPageDir; ?>
        </div>
    </div>

    <!-- The Modal -->
    <div id="myModal" class="modal">
        <!-- The Close Button -->
        <span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>

        <!-- Modal Content (The Image) -->
        <img class="modal-content" id="img01">

        <!-- Modal Caption (Image Text) -->
        <div id="caption"></div>
    </div>

    <!-- Image Viewer Modal -->
    <div class="modal fade" id="ImageViewer" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="ImageViewerTitle"></h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="action/upload.php" target="iframeImageUpload" enctype="multipart/form-data" onsubmit="startImageUpload()">
                    <input type="hidden" id="ImageViewerObject" name="object" />
                    <input type="hidden" id="ImageViewerID" name="id" />
                    <input type="hidden" id="ImageViewerField" name="field" />
                    <img class="img-responsive" id="ImageViewerImage" style="height: 300px; width: 300px; margin: auto;">
                    <br />
                    <div class="input-group col-sm-offset-2 col-sm-8">
                        <input type="file" name="imageToUpload" class="form-control" onchange="readURL(this, '#ImageViewerImage');" />
                        <div class="input-group-btn">
                            <input type="submit" class="btn btn-primary" value="Upload" />
                        </div>
                    </div>
                    <br />
                    <div id="alertImageUpload"></div>
                </form>
                <div id="loaderImageUpload" class="text-center" style="visibility:hidden;">
                    <img src="static/images/loader.gif"></img>
                </div>
                <iframe id="iframeImageUpload" name="iframeImageUpload" src="#" style="height: 0px; border: 0px solid black;"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        </div>
    </div>

    <img id="symbol-comparison" data-operator='contains' src="<?php echo _MEDIAURL_  ?>symbols/comparison.png" style="height: 0px; width: 0px;"></img>
    <img id="symbol-comparison-not" data-operator='!contains' src="<?php echo _MEDIAURL_  ?>symbols/comparison-not.png" style="height: 0px; width: 0px;"></img>
    <img id="symbol-equal" data-operator='equals' src="<?php echo _MEDIAURL_  ?>symbols/equal.png" style="height: 0px; width: 0px;"></img>
    <img id="symbol-equal-not" data-operator='!equals' src="<?php echo _MEDIAURL_  ?>symbols/equal-not.png" style="height: 0px; width: 0px;"></img>
    <img id="symbol-lt" data-operator='lt' src="<?php echo _MEDIAURL_  ?>symbols/lt.png" style="height: 0px; width: 0px;"></img>
    <img id="symbol-lte" data-operator='lte' src="<?php echo _MEDIAURL_  ?>symbols/lte.png" style="height: 0px; width: 0px;"></img>
    <img id="symbol-gt" data-operator='gt' src="<?php echo _MEDIAURL_  ?>symbols/gt.png" style="height: 0px; width: 0px;"></img>
    <img id="symbol-gte" data-operator='gte' src="<?php echo _MEDIAURL_  ?>symbols/gte.png" style="height: 0px; width: 0px;"></img>

</body>

<?php include "static/js/import.php" ?>
<?php include "static/lib/import-js.php" ?>

<?php display_consolelogs(); ?>

</html>
