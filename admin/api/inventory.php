<?php

class InventoryApi {

    private function transactItem($r, $action) {

        // $record = json_decode($r['data'], true);
        $quantity = $r['quantity'];

        if ($quantity > 0) {
            // Check if given item has serial number

            $sn = new serial_number();
            $count = $sn->filter($sn->item__exact($record['item_id']))->count();
            $hasSerialNumber = ($count > 0);


            // You can only add item with serial numbers on
            // its default location.

            if ($action == "received") {
                $item = new item();
                $item->get($record['item_id']);
                if ($hasSerialNumber) {
                    if ($item->warehouse != $record['warehouse_id']) {
                        $wh = $item->warehouse__obj()->name;
                        return array("status"=>"ERROR", "msg"=>"Do add item in <strong>`{$wh}`</strong>.");
                    }
                }
            }

            // Do not release item with serial numbers.
            // It is recommended to do it through
            // delivery receipt or return form.

            if ($action == "released") {
                if ($hasSerialNumber) {
                    return array("status"=>"ERROR", "msg"=>"Cannot deduct item with serial numbers.");
                }
            }

            // Do inventory transaction

            $tran = new transaction();

            $tran->action = $action;
            $tran->warehouse = $r['warehouse_id'];
            $tran->status = "processed";
            $tran->remarks = $r['remarks'];
            $result = $tran->save();
            if ($result["status"] == "ERROR") {
                return $result;
            }

            $line = new transaction_line();

            $line->transaction = $tran->id;
            $line->item = $r['item_id'];
            $line->quantity = $quantity;
            $line->unit_of_measure = $r['unit_id'];
            $line->remarks = $r['remarks'];
            $result = $line->save();
            if ($result["status"] == "ERROR") {
                return $result;
            }

            $myItem = new my_items();
            $qty = $myItem->GetItemQuantity($r['item_id'], $r['unit_id'], $r['warehouse_id']);
        }

        // Return to previous admin page
        if (isset($r['return_url'])) {
            $return_url = str_replace("^_^", "&", $r['return_url']);
            header("Location: ../?{$return_url}&status=OK&msg=Item successfully {$action}.");
            exit;
        }

        return array("status"=>"OK", "msg"=>"", "qty"=>$qty);
    }

    public function AddItem($r) {
        $action = "received";
        return $this->transactItem($r, $action);
    }

    public function DeductItem($r) {
        $action = "released";
        return $this->transactItem($r, $action);
    }

    public function Adjustment($r) {
        $r['remarks'] = trim($r['remarks'] . " (Adjustment)");
        
        if ($r['quantity'] > $r['quantity_old']) {

            $r['quantity'] = $r['quantity'] - $r['quantity_old'];
            $this->AddItem($r);

        } elseif ($r['quantity'] < $r['quantity_old']) {

            $r['quantity'] = $r['quantity_old'] - $r['quantity'];
            $this->DeductItem($r);

        } else {
            // No action will be taken since there is no changes made
            // in quantity.

            // Return to previous admin page

            if (isset($r['return_url'])) {
                $return_url = str_replace("^_^", "&", $r['return_url']);
                header("Location: ../?{$return_url}&status=OK&msg=Additional stock successfully added.");
                exit;
            }
        }
    }
}

?>