<?php

session_start();

include "../settings.php";
include "inventory.php";

$inv = new InventoryApi();

$func = $_REQUEST["method"];
unset($_REQUEST["method"]);

if (method_exists($inv, $func)) {
    print json_encode($inv->{$func}($_REQUEST));
} else {
    die("Invalid method.");
}

?>