<?php

include "settings.php";

$models = GetModels();
$actions = array(
    "add",
    "remove",
    "update",
);

foreach ($models as $model => $classes) {
    foreach ($classes as $class) {
        $adminObjName = $class . "_admin";
        $adminObj = new $adminObjName;
        if(!$adminObj->IsInline()) {
            foreach ($actions as $action) {
                $permissionName = "can {$action} {$class}";
                $p = new permission();
                $count = $p->filter($p->name__exact($permissionName))->count();
                print "<span style='color: blue;'>" . $permissionName . "</span>: ";
                if($count == 0) {
                    $p->name = $permissionName;
                    $p->model = $class;
                    $p->module = $model;
                    $result = $p->save();
                    println($result["status"] . ": " . $result["msg"]);
                } else {
                    println("already added");
                }
            }
        }
    }
}

?>
