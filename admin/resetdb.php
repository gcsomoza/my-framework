<?php

include "settings.php";

$error = false;

$sqls = array(
    "TRUNCATE item",
    "TRUNCATE item_unit_of_measure",
    "TRUNCATE serial_number",
    "TRUNCATE warehouse_line",
    "TRUNCATE `transaction`",
    "TRUNCATE transaction_line",
    "TRUNCATE transaction_line_warehouse_line",
    "TRUNCATE delivery_receipt",
    "TRUNCATE delivery_receipt_line",
    "TRUNCATE delivery_receipt_line_serial_number",
    "TRUNCATE return_form",
    "TRUNCATE return_form_line",
    "TRUNCATE return_form_line_serial_number",
);

$db = new DB();

$db->connect();

foreach ($sqls as $sql) {
    
    $query = $db->query($sql);

    if ($query === false) {
        $error = true;
        println("ERROR! {$sql}. " . $db->error());
        break;
    }
}

$db->close();

if (!$error) {
    println("SUCCESS! Database reset successful");
}

?>