<?php

include "../settings.php";

if(isset($_POST)) {

    $username = $_POST["username"];
    $password = $_POST["password"];
    
    $user = new user();

    if ($user->login($username, $password)) {
        $qs = $user->filter($user->username__exact($username))->fetch();
        $user = $qs[0];

        session_start();
        $gu = new group_user();
        $qs = $gu->filter($gu->user__exact($user->id))->fetch();
        $groups = array();
        foreach ($qs as $group_user) {
            $groups[] = $group_user->group;
        }
        $_SESSION['auth'] = array(
            "userid" => $user->id,
            "username" => $username,
            "superuser" => $user->is_superuser,
            "groups" => $groups
        );
        header("Location: ../");
        exit;

    } else {
        header("Location: ../login.php?invalid");
        exit;
    }
}

header("Location: ../login.php");

?>
