<?php

// This script accepts POST parameter obj and id to remove an object's data in the database.
// If hard was given as GET parameter then this script will perform delete

include "../settings.php";

$result = array(
    'status' => 'ERROR',
    'msg' => 'There are no records to delete. Select atleast 1.'
);

if (isset($_POST['obj'])) {
    $object = $_POST['obj'];
    $inline_delete = isset($_POST['inline_delete']);
    $return_url = (isset($_POST['return_url'])) ? $_POST['return_url'] : "";
    $params = $_POST;

    unset($params['obj']);
    unset($params['delete']);
    unset($params['return_url']);
    unset($params['inline_delete']);

    foreach($params as $obj_id) {
        $obj = new $object;
        $obj->get($obj_id);
        if(isset($_GET['hard'])) {
            $result = $obj->delete();
        } else {
            $result = $obj->remove();
        }
    }
}

// this was posted through javascript function called inlineDelete
if ($inline_delete) {
    print $return_url . "&status={$result['status']}&msg={$result['msg']}";
    exit;
}

if ($return_url != "") {
    $redirect_to = "?p={$object}";
    header("Location: {$return_url}&status={$result['status']}&msg={$result['msg']}");
} else {
    $redirect_to = "?p={$object}";
    header("Location: ../{$redirect_to}&status={$result['status']}&msg={$result['msg']}");
}
?>
