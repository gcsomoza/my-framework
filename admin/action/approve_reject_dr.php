<?php

error_reporting(E_ALL);

session_start();

include "../settings.php";

$result = array(
    "status" => "OK",
    "msg" => "Approve or reject delivery receipt.",
);

$dr_id = $_GET['dr_id'];
$approve = $_GET['approve'];

$dr = new delivery_receipt();
$dr->get($dr_id);

if ($approve == 1) {
    $result = $dr->approve();
} else {
    $result = $dr->reject();
}

$return_url = $_GET['return_url'];
$return_url = str_replace($_DELIMETER, '&', $return_url);

header("Location: ../?{$return_url}&status={$result['status']}&msg={$result['msg']}");

?>