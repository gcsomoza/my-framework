<?php

include "../settings.php";

if (isset($_REQUEST)) {
    $linkerObj = $_REQUEST['linkerObj'];
    $linkerDependeeField = $_REQUEST['linkerDependeeField'];
    $linkerDependeeValue = $_REQUEST['linkerDependeeValue'];
    $linkerDependentField = $_REQUEST['linkerDependentField'];

    $obj = new $linkerObj;

    $fields = explode(',', $linkerDependeeField);
    $values = explode(',', $linkerDependeeValue);
    $n = count($fields);

    $filters = array();
    for ($i=0; $i < $n; $i++) { 
        $field = $fields[$i];
        $value = $values[$i];
        
        if (strpos($field,"__") !== false) {
            $temp = explode("__", $field);
            $tempN = count($temp);

            $field = $temp[$tempN - 1];

            $parentName = $temp[$tempN - 2];
            $parentObj = new $parentName;
            $parentObj->get($value);
            
            $value = $parentObj->{$field};
        }

        $func = $field . "__exact";
        $filters[] = $obj->{$func}($value);
    }

    // My custom filters
    if ($linkerObj == "delivery_receipt_line_serial_number" &&
        $linkerDependeeField == "delivery_receipt_line" &&
        $linkerDependentField = "serial_number") {
        $filters[] = $obj->is_returned__exact('0');

    } elseif ($linkerObj == "warehouse_line" &&
        $linkerDependeeField == "warehouse" &&
        $linkerDependentField = "item") {
        $filters[] = $obj->quantity__gt(0);
    } elseif ($linkerObj == "warehouse" &&
        $linkerDependeeField == "delivery_receipt" &&
        $linkerDependentField = "warehouse") {
        $filters = array();

        $dr = new delivery_receipt();
        $dr->get($values[0]);
        $filters[] = _NOT($obj->id__exact($dr->warehouse_To));
    }

    call_user_func_array(array($obj,'filter'), $filters);
    $qs = $obj->fetch();

    print "{";
    $options = "";
    $existingOptions = array();
    foreach ($qs as $record) {
        if ($record->get_field_object($linkerDependentField) instanceof ForeignKey) {
            $dependentObjLoad = "{$linkerDependentField}__load";
            $dependentObj = $record->{$dependentObjLoad}();
        } else {
            // The given $linkerDependentField is not an object.
            // It's a field of $record.
            $dependentObj = $record;
        }
        // Prevent duplicate of options
        if (!isset($existingOptions[$dependentObj->id]) &&
            $dependentObj->is_deleted == '0') {
            $options .= "'{$dependentObj->id}':'{$dependentObj}',";
            $existingOptions[$dependentObj->id] = true;
        }
    }
    $options = trim($options,",");
    print $options;
    print "}";
}

?>
