<?php

    session_start();

    include "../settings.php";

    $result = array(
        'status' => 'ERROR',
        'msg' => 'Invalid parameters was provided!'
    );

    if (isset($_REQUEST['object']) && $_REQUEST['id'] && $_REQUEST['field']) {

        $obj = new $_REQUEST['object'];
        $obj->get($_REQUEST['id']);
        $field = $_REQUEST['field'];
    
        $key = "imageToUpload";
    
        // check if an uploaded field
        if(isset($_FILES[$key]) && $_FILES[$key]['name'] != '') {
            
            $FILE = $_FILES[$key];
            $fieldObj = $obj->get_field_object($field);
    
            // set the destination of uploaded file
            $uploadTo = $fieldObj->uploadTo;
            $uploadTo = ($uploadTo == '') ? '' : "/{$uploadTo}";
    
            // set the filename of the uploaded file
            $filename = $obj->{$field};
            if($filename  != "") {
                $filename = $fieldObj->basename();
            }
    
            // upload the file
            $uploadResult = upload($FILE, '../media'.$uploadTo, $filename);
            if($uploadResult['status'] == "OK") {
                $obj->{$field} = $fieldObj->uploadTo . _DS_ . $uploadResult['filename'];
                $result = $obj->save();
            } else {
                // error in upload
                $result = $uploadResult;
            }
        } else {
            $result['msg'] = 'There is nothing to upload!';
        }
    }
?>

<script language="javascript" type="text/javascript">
   window.top.window.stopImageUpload('<?php echo $result["status"] ?>', '<?php echo $result["msg"]?>');
</script>