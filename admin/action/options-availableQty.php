<?php

$qty = 0;
switch ($_REQUEST['form']) {
    case 'dr':
        include "../settings.php";
    
        $item = $_REQUEST['item'];
        $warehouse = $_REQUEST['warehouse'];
    
        $whl = new warehouse_line();
        $qs = $whl->filter(
            $whl->item__exact($item),
            $whl->warehouse__exact($warehouse),
            $whl->quantity__gt('0')
        )->fetch();
    
        foreach ($qs as $rec) {
            $qty += $rec->quantity;
        }

        break;

    case 'rf':
        include "../settings.php";
        
        $id = $_REQUEST['delivery_receipt_line'];
        $drl = new delivery_receipt_line();
        $drl->get($id);

        $qty = $drl->quantity - $drl->getReturned();

        break;
    
    default:
        break;
}

echo json_encode($qty);

?>