<?php

session_start();

include "../settings.php";

function __rollback($db) {
    if ($db instanceof DB) {
        $db->rollback();
        $db->close();
    }
}
// println('$_POST');
// println($_POST);
// header("Location: " . $_POST['return_url'] . "&value=999&text=TEST");
// exit;
if(isset($_POST) && !empty($_POST)) {

    $object = $_POST["object"];

    $page = 1;
    if (isset($_POST["page"])) {
        $page = $_POST["page"];
        unset($_POST["page"]);
    }
    $add_foreignkey = isset($_POST["add-foreignkey"]);

    $return_url = (isset($_POST["return_url"])) ? $_POST["return_url"] : "";
    if ($return_url != '') {
        $return_url = str_replace($_DELIMETER, '&', $return_url);
    }

    $save = isset($_POST["save"]);
    $save_add_another = isset($_POST["save_add_another"]);
    $save_continue = isset($_POST["save_continue"]);
    
    unset($_POST["object"]);
    unset($_POST["save"]);
    unset($_POST["save_add_another"]);
    unset($_POST["save_continue"]);
    unset($_POST["return_url"]);
    unset($_POST["add-foreignkey"]);
    
    $db = new DB();
    $db->connect();
    // $db->start_transaction();

    $obj = new $object($db);

    if (isset($_POST["id"]) && $_POST["id"] != "0") {
        $obj->get($_POST["id"]);
    }
    unset($_POST["id"]);

    $m2m_fields = array();
    foreach ($_POST as $key => $value) {
        if (strpos($key, 'm2m_') !== false) {
            // save many-to-many to be processed later
            $m2m_fields[str_replace("m2m_", "", $key)] = $value;
        } else {
            // process field

            // check if an uploaded field
            if(isset($_FILES[$key]) && $_FILES[$key]['name'] != '') {

                $FILE = $_FILES[$key];
                $fieldObj = $obj->get_field_object($key);

                // set the destination of uploaded file
                $uploadTo = $fieldObj->uploadTo;
                $uploadTo = ($uploadTo == '') ? '' : "/{$uploadTo}";

                // set the filename of the uploaded file
                $filename = $obj->{$key};
                if($filename  != "") {
                    $filename = $fieldObj->basename();
                }

                // upload the file
                $uploadResult = upload($FILE, '../media'.$uploadTo, $filename);
                if($uploadResult['status'] == "OK") {
                    $obj->{$key} = $fieldObj->uploadTo . _DS_ . $uploadResult['filename'];
                } else {
                    // error in upload
                    $result = $uploadResult;
                    __rollback($db);
                    goto redirect;
                }
            } else {
                $obj->{$key} = $value;
            }
        }
    }

    $result = $obj->save();
    if ($result["status"] == "ERROR") {
        __rollback($db);
        goto redirect;
    }

    // process many-to-many
    foreach ($m2m_fields as $foreign_object_name => $other_object_ids) {
        // get m2m_field from object
        $m2m_field = $obj->m2m[$foreign_object_name];

        // get other object column name
        $other_object_id_name = $m2m_field->otherObjectIdName;
        // get object column name
        $object_id_name = $m2m_field->objectIdName;

        // fetch already selected other objects
        $selected_other_objects = $obj->{"{$foreign_object_name}__set"}();
        // this will be use to remove other objects that are no longer selected
        $selected_other_objects_temp = array();
        foreach ($selected_other_objects as $key => $selected_other_object) {
            $selected_other_objects_temp[$selected_other_object->{$other_object_id_name}] = $key;
        }

        // save each given other object ids
        foreach ($other_object_ids as $key => $other_object_id) {
            if ($other_object_id == "") {
                continue;
            }
            $foreign_object = new $foreign_object_name($db);
            $foreign_object->{$other_object_id_name} = $other_object_id;
            $foreign_object->{$object_id_name} = $obj->id;

            // check if the other object id is already selected
            if(isset($selected_other_objects_temp[$other_object_id])) {
                // remove the selected other object in the list.
                // the remaining other object in the list will be removed later.
                unset($selected_other_objects[$selected_other_objects_temp[$other_object_id]]);
            } else {
                // save foreign object
                $result = $foreign_object->save();
                if ($result["status"] == "ERROR") {
                    __rollback($db);
                    goto redirect;
                }
            }
        }

        // remove other objects that are no longer selected
        foreach ($selected_other_objects as $key => $selected_other_object) {
            $selected_other_object->__setdb($db);
            $result = $selected_other_object->delete();
            if ($result["status"] == "ERROR") {
                __rollback($db);
                goto redirect;
            }
        }
    } // end of processing many-to-many

    $db->commit();
    $db->close();
    
    $result = array(
        "status" => "OK",
        "msg" => ucwords(str_replace("_"," ",$object)) . " was successfully saved"
    );

redirect:
    
    if ($return_url != "" && $add_foreignkey) {
        switch ($return_url) {
            case 'JSON':
                print json_encode($result);
                exit;
                break;

            default:
                if($add_foreignkey) {
                    $location = "Location: " . $return_url . "&page={$page}&value={$obj->id}&text={$obj}";
                } else {
                    $location = "Location: " . $return_url . "&page={$page}&status={$result['status']}&msg={$result['msg']}";
                }
                break;
        };
    } else {
        $redirect_to = "";
        if ($save) {
            if ($return_url != "") {
                $redirect_to = "?{$return_url}&page={$page}";
            } else {
                $redirect_to = "?p={$object}&page={$page}";
            }
        } elseif ($save_add_another) {
            $redirect_to = "?p={$object}&page={$page}&a=add&return_url={$return_url}";
        } elseif ($save_continue) {
            $redirect_to = "?p={$object}&page={$page}&a=update&id={$obj->id}&return_url={$return_url}";
        }
        $location = "Location: ../{$redirect_to}&status={$result['status']}&msg={$result['msg']}";
    }
    header($location);
} // end if

?>
