<?php

class Context {

    private $context;

    public function __construct() {
        if(isset($_SESSION['context']))
            $this->context = $_SESSION['context'];
        else
            $this->context = array();
    }

    public function __get($name) {
        if (isset($this->context[$name]))
            return $this->context[$name];
        else
            return "";
    }
}

$context = new Context();

?>
