<?php

class paginator {

  // This class will help you to get the total number of pages and
  // the offset (or you can say `start`) that will be used on every given page number

  public function num_pages($total, $items_per_page) {
    return ceil($total / $items_per_page);
  }

  public function offset($page, $items_per_page) {
    return ($page - 1) * $items_per_page;
  }

  public function pagination() {
	  $numArgs = func_num_args();

	  $adjacents = 7;
	  
	  $targetpage = func_get_arg(0);
	  $page = func_get_arg(1);
	  $totalPage = func_get_arg(2);
	  $itemsPerPage = func_get_arg(3);

	  $queryString = "";
	  if ($numArgs > 4) {
		$queryString = func_get_arg(4);
	  }

      $targetpage = "?p={$targetpage}";
      if ($page == 0) { $page = 1; }
      $prev = $page - 1;
      $next = $page + 1;
      $lastpage = $totalPage;
      $lpm1 = $lastpage - 1;
      $pagination = "";
      if($lastpage > 1)
      {
      	$pagination .= "<div><ul class=\"pagination pagination-sm\">";
      	//previous button
      	if ($page > 1)
      		$pagination.= "<li><a href=\"{$targetpage}&page={$prev}&{$queryString}\">&laquo;</a></li>";
      	else
      		$pagination.= "<li><span class=\"page-disabled\">&laquo;</span></li>";

      	//pages
      	if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
      	{
      		for ($counter = 1; $counter <= $lastpage; $counter++)
      		{
      			if ($counter == $page)
      				$pagination.= "<li><span class=\"page-current\">$counter</span></li>";
      			else
      				$pagination.= "<li><a href=\"$targetpage&page=$counter&{$queryString}\">$counter</a></li>";
      		}
      	}
      	elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
      	{
      		//close to beginning; only hide later pages
      		if($page < 1 + ($adjacents * 2))
      		{
      			for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
      			{
      				if ($counter == $page)
      					$pagination.= "<li><span class=\"page-current\">$counter</span></li>";
      				else
      					$pagination.= "<li><a href=\"$targetpage&page=$counter&{$queryString}\">$counter</a></li>";
      			}
      			$pagination.= "...";
      			$pagination.= "<li><a href=\"$targetpage&page=$lpm1&{$queryString}\">$lpm1</a></li>";
      			$pagination.= "<li><a href=\"$targetpage&page=$lastpage&{$queryString}\">$lastpage</a></li>";
      		}
      		//in middle; hide some front and some back
      		elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
      		{
      			$pagination.= "<li><a href=\"$targetpage&page=1&{$queryString}\">1</a></li>";
      			$pagination.= "<li><a href=\"$targetpage&page=2&{$queryString}\">2</a></li>";
      			$pagination.= "...";
      			for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
      			{
      				if ($counter == $page)
      					$pagination.= "<li><span class=\"page-current\">$counter</span></li>";
      				else
      					$pagination.= "<li><a href=\"$targetpage&page=$counter&{$queryString}\">$counter</a></li>";
      			}
      			$pagination.= "...";
      			$pagination.= "<li><a href=\"$targetpage&page=$lpm1&{$queryString}\">$lpm1</a></li>";
      			$pagination.= "<li><a href=\"$targetpage&page=$lastpage&{$queryString}\">$lastpage</a></li>";
      		}
      		//close to end; only hide early pages
      		else
      		{
      			$pagination.= "<li><a href=\"$targetpage&page=1&{$queryString}\">1</a></li>";
      			$pagination.= "<li><a href=\"$targetpage&page=2&{$queryString}\">2</a></li>";
      			$pagination.= "...";
      			for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
      			{
      				if ($counter == $page)
      					$pagination.= "<li><span class=\"page-current\">$counter</span></li>";
      				else
      					$pagination.= "<li><a href=\"$targetpage&page=$counter&{$queryString}\">$counter</a></li>";
      			}
      		}
      	}

      	//next button
      	if ($page < $counter - 1)
      		$pagination.= "<li><a href=\"$targetpage&page=$next&{$queryString}\">&raquo;</a></li>";
      	else
      		$pagination.= "<li><span class=\"page-disabled\">&raquo;</span></li>";
      	$pagination.= "</ul></div>\n";
      }
      return $pagination;
  }

}

?>
