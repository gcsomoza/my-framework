<?php

class ModelAdmin {

    public $object;
    protected $menuIcon = "fa-circle";

    public $searchFields = array();
    public $searchFilters = array();

    public $listColumns = array();
    public $inlines = array();

    public function IsDisplayed() {
        if(isset($_SESSION['auth'])) {
            $auth = $_SESSION['auth'];
            if($auth["superuser"]) {
                return true;
            }

            $up = new user_permission();
            $count = $up->filter(
                $up->user__exact($auth["userid"]),
                $up->permission__model__exact($this->GetModel())
            )->count();
            if($count > 0) {
                return true;
            }

            $gp = new group_permission();
            $count = $gp->filter(
                $gp->group__in($auth["groups"]),
                $gp->permission__model__exact($this->GetModel())
            )->count();

            if($count > 0) {
                return true;
            }
        }
        return false;
    }

    public function IsInline() {
        return false;
    }

    private function IsPermitted($action) {
        if(isset($_SESSION['auth'])) {
            $action = strtolower($action);
            $action = "can {$action} " . $this->GetModel();
            $auth = $_SESSION['auth'];
            if($auth["superuser"]) {
                return true;
            }

            $up = new user_permission();
            $count = $up->filter(
                $up->user__exact($auth["userid"]),
                $up->permission__name__exact($action)
            )->count();
            if($count > 0) {
                return true;
            }

            $gp = new group_permission();
            $count = $gp->filter(
                $gp->group__in($auth["groups"]),
                $gp->permission__name__exact($action)
            )->count();

            if($count > 0) {
                return true;
            }
        }
        return false;
    }

    public function CanAdd() {
        return $this->IsPermitted("add");
    }

    public function CanUpdate() {
        return $this->IsPermitted("update");
    }

    public function CanDelete() {
        return $this->IsPermitted("remove");
    }

    // Returns the font-awesome icon for the given module.
    public function GetMenuIcon() {
        return $this->menuIcon;
    }

    // Returns the model name it represents
    public function GetModel() {
        return str_replace("_admin", "", get_class($this));
    }

    public function GetListColumns() {
        if (empty($this->listColumns)) {
            $class = str_replace("_admin", "", get_class($this));
            $obj = new $class;
            $fields = $obj->get_fields();
            foreach($fields as $field => $value) {
                if ($field == 'id' || $field == 'is_deleted') {
                    break;
                }
                if ($value instanceof Field) {
                    if ($value->hidden == false) {
                        $this->listColumns[] = $field;
                    }
                }
            }
        }
        return $this->listColumns;
    }

    public function GetListValues() {

        if ($this->object instanceof Model) {
            $result = array();
            $columns = $this->listColumns;
            foreach ($columns as $column) {
                $displayVal = "";
                $col = $this->object->get_field_object($column);
                if ($col instanceof Field) {
                    $displayVal = $col->display_value(get_class($this->object), $this->object->id, $column);
                } elseif (method_exists($this->object, $column)) {
                    $value = $this->object->{$column}();
                    $type = gettype($value);
                    switch ($type) {
                        case 'boolean':
                            $field = new BooleanField(array("value"=>$value));
                            $displayVal = $field->display_value();
                            break;
                        default:
                            $displayVal = $value;
                            break;
                    }
                }
                $result[$column] = $displayVal;
            }
            return $result;
        }
        return null;
    }

    public function SetObject($obj) {
        $this->object = $obj;
    }
}

class InlineAdmin extends ModelAdmin {

    public function IsInline() {
        return true;
    }

    public function IsDisplayed() {
        return false;
    }
}

?>
