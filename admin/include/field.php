<?php

class Field {

    public $canAdd;
    public $canUpdate;
    public $hidden;
    public $displayName;
    public $required;
    public $requiredOnAdd;
    public $requiredOnUpdate;
    public $readonly;
    public $readonlyOnAdd;
    public $readonlyOnUpdate;
    public $helpText;
    protected $value;
    protected $mysql_datatype;

    public function __construct($properties = array()) {
        $this->canAdd = true;
        $this->canUpdate = true;
        $this->hidden = false;
        $this->displayName = '';
        $this->required = false;
        $this->requiredOnAdd = false;
        $this->requiredOnUpdate = false;
        $this->readonly = false;
        $this->readonlyOnAdd = false;
        $this->readonlyOnUpdate = false;
        $this->helpText = '';
        $this->value = '';
        $this->mysql_datatype = 'VARCHAR(200)';

        $this->set_properties($properties);
    }

    public function set_properties($properties = array()) {
        if(!empty($properties)) {
            foreach ($properties as $key => $value) {
                if(property_exists($this, $key)) {
                    $this->{$key} = $value;
                }
            }
        }
    }

    public function set_value($value) {
        if (!($value instanceof CalculatedField)) {
            $value = str_replace("”", '"', $value);
            $value = str_replace("’", "'", $value);
        }
        $this->value = $value;
    }

    public function get_value() {
        $value = $this->value;
        if (!($value instanceof CalculatedField)) {
            $value = str_replace('"', '”', $value);
            $value = str_replace("'", "’", $value);
        }
        return $value;
    }

    public function datatype() {
        return $this->mysql_datatype;
    }

    public function display_value() {
        $value = $this->value;
        if (!($value instanceof CalculatedField)) {
            $value = str_replace('"', '”', $value);
            $value = str_replace("'", "’", $value);
        }
        return $value;
    }

    public function input_control_attr($attr) {
        $result = "";
        switch ($attr) {
            case 'hidden':
                $result = ($this->hidden == true) ? 'display: none' : '';
                break;
            case 'required':
                $result = ($this->required == true) ? 'required' : '';
                break;
            case 'requiredOnAdd':
                $result = ($this->requiredOnAdd == true) ? 'required' : '';
                break;
            case 'requiredOnUpdate':
                $result = ($this->requiredOnUpdate == true) ? 'required' : '';
                break;
            case 'readonly':
                $result = ($this->readonly == true) ? 'readonly' : '';
                break;
            case 'readonlyOnAdd':
                $result = ($this->readonlyOnAdd == true) ? 'readonly' : '';
                break;
            case 'readonlyOnUpdate':
                $result = ($this->readonlyOnUpdate == true) ? 'readonly' : '';
                break;
            default:
                $result = "";
                break;
        }
        return $result;
    }

    public function input_control($object, $name, $action, $inline = false) {
        $displayName = ($this->displayName == '') ? ucwords(str_replace("_"," ",$name)) : $this->displayName;

        $helpText = '';
        if ($this->helpText != '') {
            $helpText = "
                <i class='fa fa-question-circle'></i> <small>{$this->helpText}</small>
            ";
        }

        $required = $this->input_control_attr('required');
        if($required != 'required') {
            if($action == 'add') {
                $required = $this->input_control_attr('requiredOnAdd');
            } elseif ($action == 'update') {
                $required = $this->input_control_attr('requiredOnUpdate');
            }
        }

        $readonly = $this->input_control_attr('readonly');
        if($readonly != 'readonly') {
            if($action == 'add') {
                $readonly = $this->input_control_attr('readonlyOnAdd');
            } elseif ($action == 'update') {
                $readonly = $this->input_control_attr('readonlyOnUpdate');
            }
        }

        $displayNone = $this->input_control_attr('hidden');

        $requiredIndicator = ($required == 'required') ? '<span style="color: red;">*</span>' : '';
        $inputControl ='
        <div class="form-group" style="'.$displayNone.'">
            <label for="'.$name.'">'.$displayName.': '.$requiredIndicator.'</label>
            <input type="text" autocomplete="off" class="form-control" id="'.$name.'" name="'.$name.'" value="'.$this->get_value().'" '.$required.' '.$readonly.'/>
            '.$helpText.'
        </div>';
        return $inputControl;
    }

    public function filter_control($controlName,$selectedValue) {
        return "";
    }
}

class CharField extends Field {}

class TextField extends Field {

    public function input_control($object, $name, $action, $inline = false) {
        $displayName = ($this->displayName == '') ? ucwords(str_replace("_"," ",$name)) : $this->displayName;

        $helpText = '';
        if ($this->helpText != '') {
            $helpText = "
                <i class='fa fa-question-circle'></i> <small>{$this->helpText}</small>
            ";
        }

        $required = $this->input_control_attr('required');
        if($required != 'required') {
            if($action == 'add') {
                $required = $this->input_control_attr('requiredOnAdd');
            } elseif ($action == 'update') {
                $required = $this->input_control_attr('requiredOnUpdate');
            }
        }

        $readonly = $this->input_control_attr('readonly');
        if($readonly != 'readonly') {
            if($action == 'add') {
                $readonly = $this->input_control_attr('readonlyOnAdd');
            } elseif ($action == 'update') {
                $readonly = $this->input_control_attr('readonlyOnUpdate');
            }
        }

        $displayNone = $this->input_control_attr('hidden');

        $requiredIndicator = ($required == 'required') ? '<span style="color: red;">*</span>' : '';
        $inputControl ='
        <div class="form-group" style="'.$displayNone.'">
            <label for="'.$name.'">'.$displayName.': '.$requiredIndicator.'</label>
            <textarea class="form-control" rows="4" id="'.$name.'" name="'.$name.'" '.$required.' '.$readonly.'>'.$this->get_value().'</textarea>
            '.$helpText.'
        </div>';
        return $inputControl;
    }
}

class ImageField extends Field {

    public $uploadTo;
    public $noImage;

    public function __construct($properties = array()) {
        parent::__construct();

        $this->uploadTo = '';
        $this->noImage = 'noimage.png';

        $this->set_properties($properties);
    }

    public function basename() {
        if($this->value != '') {
            $basename = pathinfo($this->value, PATHINFO_BASENAME);
            $string = $basename;
            $string = explode('.', $string);
            array_pop($string);
            $string = implode('.', $string);
            return $string;
        }
        return '';
    }

    public function get_image() {
        if($this->value == '') {
            return $this->noImage;
        } else {
            if (file_exists(_MEDIADIR_ . $this->value)) {
                return $this->value;
            }
            return $this->noImage;
        }
    }

    public function display_value() {

        // include object and id as data in img tag
        
        $object = "";
        $id = "";
        $field = "";

        $n = func_num_args();
        if ($n == 3) {
            $object = func_get_arg(0);
            $id = func_get_arg(1);
            $field = func_get_arg(2);
        }
        $url = _MEDIAURL_ . $this->get_image();
        return "<img src='{$url}' id='img-{$id}' data-object='{$object}' data-id='{$id}' data-field='{$field}' class='img-rounded imagefield' style='border: 1px solid gray; width: 84px; height: 84px; max-width: 84px; max-height: 84px;' />";
    }

    public function input_control($object, $name, $action, $inline = false) {
        $displayName = ($this->displayName == '') ? ucwords(str_replace("_"," ",$name)) : $this->displayName;

        $helpText = '';
        if ($this->helpText != '') {
            $helpText = "
                <i class='fa fa-question-circle'></i> <small>{$this->helpText}</small>
            ";
        }

        $required = $this->input_control_attr('required');
        if($required != 'required') {
            if($action == 'add') {
                $required = $this->input_control_attr('requiredOnAdd');
            } elseif ($action == 'update') {
                $required = $this->input_control_attr('requiredOnUpdate');
            }
        }

        $readonly = $this->input_control_attr('readonly');
        if($readonly != 'readonly') {
            if($action == 'add') {
                $readonly = $this->input_control_attr('readonlyOnAdd');
            } elseif ($action == 'update') {
                $readonly = $this->input_control_attr('readonlyOnUpdate');
            }
        }

        $displayNone = $this->input_control_attr('hidden');

        $requiredIndicator = ($required == 'required') ? '<span style="color: red;">*</span>' : '';
        $inputControl ="
        <div class='control-group' style='margin-bottom: 15px; {$displayNone};'>
            <label class='control-label' for='{$name}' >{$displayName}: {$requiredIndicator}</label>
            <div class='controls'>
                <table style='margin-top: 5px; margin-bottom: 5px;'>
                    <tr>
                        <td>
                            <img id='{$name}-img' src='" . _MEDIAURL_ . "{$this->get_image()}' class='img-rounded' style='border: 1px solid gray; width: 84px; height: 84px; max-width: 84px; max-height: 84px;' />
                        </td>
                        <td valign='bottom' style='padding-left: 20px;'>
                            <label for='{$name}-chk' style='cursor: pointer;'>
                                <input type='hidden' name='{$name}' value='{$this->get_value()}' />
                                <input type='checkbox' id='{$name}-chk' name='{$name}' value='' />
                                check to remove image
                            </label>
                        </td>
                    </tr>
                </table>
                <input type='file' name='{$name}' class='form-control' onchange='readURL(this,\"#{$name}-img\")' >
            </div>
            {$helpText}
        </div>";
        return $inputControl;
    }
}

class ChoicesField extends Field {

    public $choices;

    public function __construct($properties = array()) {
        parent::__construct();

        $this->choices = array();

        $this->set_properties($properties);
    }

    public function display_value() {
        return $this->choices[$this->value];
    }

    public function input_control($object, $name, $action, $inline = false) {
        $displayName = ($this->displayName == '') ? ucwords(str_replace("_"," ",$name)) : $this->displayName;

        $helpText = '';
        if ($this->helpText != '') {
            $helpText = "
                <i class='fa fa-question-circle'></i> <small>{$this->helpText}</small>
            ";
        }

        $required = $this->input_control_attr('required');
        if($required != 'required') {
            if($action == 'add') {
                $required = $this->input_control_attr('requiredOnAdd');
            } elseif ($action == 'update') {
                $required = $this->input_control_attr('requiredOnUpdate');
            }
        }

        $readonly = $this->input_control_attr('readonly');
        if($readonly != 'readonly') {
            if($action == 'add') {
                $readonly = $this->input_control_attr('readonlyOnAdd');
            } elseif ($action == 'update') {
                $readonly = $this->input_control_attr('readonlyOnUpdate');
            }
        }

        $control = '<input type="text" class="form-control" id="'.$name.'" name="'.$name.'" value="'.$this->get_value().'" '.$required.' '.$readonly.'/>';
        if (is_array($this->choices) && !empty($this->choices)) {
            $control = '<select class="form-control" id="'.$name.'" name="'.$name.'" '.$required.' '.$readonly.'>';
            $control .= '<option value="">--- Select '.ucwords(str_replace("_"," ",$name)).' ---</option>';
            foreach($this->choices as $value => $display) {
                $selected = ($value == $this->get_value()) ? "selected" : "";
                $control .= '<option value="'.$value.'" '.$selected.'>'.$display.'</option>';
            }
            $control .= '</select>';
        }

        $displayNone = $this->input_control_attr('hidden');

        $requiredIndicator = ($required == 'required') ? '<span style="color: red;">*</span>' : '';
        $inputControl ='
        <div class="form-group" style="'.$displayNone.'">
            <label for="'.$name.'">'.$displayName.': '.$requiredIndicator.'</label>
            '.$control.'
            '.$helpText.'
        </div>';
        return $inputControl;
    }

    public function filter_control($selectedValue,$controlName) {
        return "";
    }
}

class PasswordField extends Field {
    public function __construct($properties = array()) {
        parent::__construct();
        $this->requiredOnAdd = true;

        $this->set_properties($properties);
    }

    public function input_control($object, $name, $action, $inline = false) {
        $displayName = ($this->displayName == '') ? ucwords(str_replace("_"," ",$name)) : $this->displayName;

        $helpText = '';
        if ($this->helpText != '') {
            $helpText = "
                <i class='fa fa-question-circle'></i> <small>{$this->helpText}</small>
            ";
        }

        $required = $this->input_control_attr('required');
        if($required != 'required') {
            if($action == 'add') {
                $required = $this->input_control_attr('requiredOnAdd');
            } elseif ($action == 'update') {
                $required = $this->input_control_attr('requiredOnUpdate');
            }
        }

        $readonly = $this->input_control_attr('readonly');
        if($readonly != 'readonly') {
            if($action == 'add') {
                $readonly = $this->input_control_attr('readonlyOnAdd');
            } elseif ($action == 'update') {
                $readonly = $this->input_control_attr('readonlyOnUpdate');
            }
        }

        $displayNone = $this->input_control_attr('hidden');

        $requiredIndicator = ($required == 'required') ? '<span style="color: red;">*</span>' : '';
        $inputControl ='
        <div class="form-group" style="'.$displayNone.'">
            <label for="'.$name.'">'.$displayName.': '.$requiredIndicator.'</label>
            <input type="password" class="form-control" id="'.$name.'" name="'.$name.'" value="" '.$required.' '.$readonly.'/>
            '.$helpText.'
        </div>';
        return $inputControl;
    }
}

class IntegerField extends Field {
    public function __construct($properties = array()) {
        parent::__construct();

        $this->value = 0;
        $this->mysql_datatype = 'INTEGER';

        $this->set_properties($properties);
    }

    public function input_control($object, $name, $action, $inline = false) {
        $displayName = ($this->displayName == '') ? ucwords(str_replace("_"," ",$name)) : $this->displayName;

        $helpText = '';
        if ($this->helpText != '') {
            $helpText = "
                <i class='fa fa-question-circle'></i> <small>{$this->helpText}</small>
            ";
        }

        $required = $this->input_control_attr('required');
        if($required != 'required') {
            if($action == 'add') {
                $required = $this->input_control_attr('requiredOnAdd');
            } elseif ($action == 'update') {
                $required = $this->input_control_attr('requiredOnUpdate');
            }
        }

        $readonly = $this->input_control_attr('readonly');
        if($readonly != 'readonly') {
            if($action == 'add') {
                $readonly = $this->input_control_attr('readonlyOnAdd');
            } elseif ($action == 'update') {
                $readonly = $this->input_control_attr('readonlyOnUpdate');
            }
        }

        $displayNone = $this->input_control_attr('hidden');

        $requiredIndicator = ($required == 'required') ? '<span style="color: red;">*</span>' : '';
        $inputControl ='
        <div class="form-group" style="'.$displayNone.'">
            <label for="'.$name.'">'.$displayName.': '.$requiredIndicator.'</label>
            <input type="number" autocomplete="off" class="form-control" id="'.$name.'" name="'.$name.'" value="'.$this->get_value().'" '.$required.' '.$readonly.'/>
            '.$helpText.'
        </div>';
        return $inputControl;
    }
}

class FloatField extends Field {
    public function __construct($properties = array()) {
        parent::__construct();

        $this->value = 0.0;
        $this->mysql_datatype = 'FLOAT';

        $this->set_properties($properties);
    }

    public function input_control($object, $name, $action, $inline = false) {
        $displayName = ($this->displayName == '') ? ucwords(str_replace("_"," ",$name)) : $this->displayName;

        $helpText = '';
        if ($this->helpText != '') {
            $helpText = "
                <i class='fa fa-question-circle'></i> <small>{$this->helpText}</small>
            ";
        }

        $required = $this->input_control_attr('required');
        if($required != 'required') {
            if($action == 'add') {
                $required = $this->input_control_attr('requiredOnAdd');
            } elseif ($action == 'update') {
                $required = $this->input_control_attr('requiredOnUpdate');
            }
        }

        $readonly = $this->input_control_attr('readonly');
        if($readonly != 'readonly') {
            if($action == 'add') {
                $readonly = $this->input_control_attr('readonlyOnAdd');
            } elseif ($action == 'update') {
                $readonly = $this->input_control_attr('readonlyOnUpdate');
            }
        }

        $displayNone = $this->input_control_attr('hidden');

        $requiredIndicator = ($required == 'required') ? '<span style="color: red;">*</span>' : '';
        $inputControl ='
        <div class="form-group" style="'.$displayNone.'">
            <label for="'.$name.'">'.$displayName.': '.$requiredIndicator.'</label>
            <input type="number" autocomplete="off" class="form-control" id="'.$name.'" name="'.$name.'" value="'.$this->get_value().'" '.$required.' '.$readonly.'/>
            '.$helpText.'
        </div>';
        return $inputControl;
    }
}

class BooleanField extends Field {
    public function __construct($properties = array()) {
        parent::__construct();

        $this->value = false;
        $this->mysql_datatype = 'BOOLEAN';

        $this->set_properties($properties);
    }

    public function set_value($value) {
        $this->value = false;
        if ($value == 1) {
            $this->value = true;
        } elseif ($value == "on") {
            $this->value = true;
        } elseif ($value == "off") {
            $this->value = false;
        }
    }

    public function get_value() {
        if ($this->value == true) {
            return true;
        } else {
            return false;
        }
    }

    public function display_value() {
        if ($this->value === true) {
            return "<i class='fa fa-check text-success'></i>";
        } else {
            return "<i class='fa fa-times text-danger'></i>";
        }
    }

    public function input_control($object, $name, $action, $inline = false) {
        $displayName = ($this->displayName == '') ? ucwords(str_replace("_"," ",$name)) : $this->displayName;
        $checked = ($this->value === true) ? "checked" : "";

        $helpText = '';
        if ($this->helpText != '') {
            $helpText = "
                <i class='fa fa-question-circle'></i> <small>{$this->helpText}</small>
            ";
        }

        $required = $this->input_control_attr('required');
        if($required != 'required') {
            if($action == 'add') {
                $required = $this->input_control_attr('requiredOnAdd');
            } elseif ($action == 'update') {
                $required = $this->input_control_attr('requiredOnUpdate');
            }
        }

        $readonly = $this->input_control_attr('readonly');
        if($readonly != 'readonly') {
            if($action == 'add') {
                $readonly = $this->input_control_attr('readonlyOnAdd');
            } elseif ($action == 'update') {
                $readonly = $this->input_control_attr('readonlyOnUpdate');
            }
        }

        $displayNone = $this->input_control_attr('hidden');

        $requiredIndicator = ($required == 'required') ? '<span style="color: red;">*</span>' : '';
        $inputControl ='
        <div class="checkbox" style="'.$displayNone.'">
            <input type="hidden" name="'.$name.'" value="off" />
            <label for="'.$name.'"><input id="'.$name.'" name="'.$name.'" type="checkbox" '.$checked.' '.$required.' '.$readonly.'/> <strong>'.$displayName.'</strong> '.$requiredIndicator.'</label>
        </div><div style="margin-top: -10px; margin-bottom: 15px;">'.$helpText.'</div>';
        return $inputControl;
    }

    public function filter_control($controlName, $selectedValue) {
        $control = "<select name='{$controlName}' class='form-control filter'>";
        $controlDisplayName = ucwords(str_replace("_"," ",$controlName));
        $control .= "<option value=''>--Select {$controlDisplayName}--</option>";
        $options = array(
            "1",
            "0"
        );
        $optionsMap = array(
            "1" => "Yes",
            "0" => "No"
        );
        foreach($options as $option) {
            $selected = ($option == $selectedValue) ? "selected" : "";
            $control .= "<option value='{$option}' {$selected}>{$optionsMap[$option]}</option>";
        }
        $control .= "</select>";

        return $control;
    }
}

class DateField extends Field {
    public function __construct($properties = array()) {
        parent::__construct();

        $this->value = 'NULL';
        $this->mysql_datatype = 'DATE';

        $this->set_properties($properties);
    }

    public function get_value() {
        if($this->value == "" || $this->value == "NULL") {
            return "NULL";
        }
        return date(_MYSQL_DATEFORMAT_,strtotime($this->value));
    }

    public function display_value() {
        if ($this->value != "") {
            return date(_DATEFORMAT_, strtotime($this->value));
        }
        return "";
    }

    public function input_control($object, $name, $action, $inline = false) {
        $displayName = ($this->displayName == '') ? ucwords(str_replace("_"," ",$name)) : $this->displayName;

        $helpText = '';
        if ($this->helpText != '') {
            $helpText = "
                <i class='fa fa-question-circle'></i> <small>{$this->helpText}</small>
            ";
        }

        $required = $this->input_control_attr('required');
        if($required != 'required') {
            if($action == 'add') {
                $required = $this->input_control_attr('requiredOnAdd');
            } elseif ($action == 'update') {
                $required = $this->input_control_attr('requiredOnUpdate');
            }
        }

        $readonly = $this->input_control_attr('readonly');
        if($readonly != 'readonly') {
            if($action == 'add') {
                $readonly = $this->input_control_attr('readonlyOnAdd');
            } elseif ($action == 'update') {
                $readonly = $this->input_control_attr('readonlyOnUpdate');
            }
        }

        $value = ($this->get_value() != "NULL") ? date(_DATEFORMAT_, strtotime($this->get_value())) : "";

        $displayNone = $this->input_control_attr('hidden');

        $requiredIndicator = ($required == 'required') ? '<span style="color: red;">*</span>' : '';
        $inputControl ='
        <div class="form-group" style="'.$displayNone.'">
            <label for="'.$name.'">'.$displayName.': '.$requiredIndicator.'</label>
            <div class="input-group date datepicker">
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                <input type="text" class="form-control" id="'.$name.'" name="'.$name.'" value="'.$value.'" '.$required.' '.$readonly.'/>
            </div>
            '.$helpText.'
        </div>';
        return $inputControl;
    }
}

class DateTimeField extends Field {
    public function __construct($properties = array()) {
        parent::__construct();

        $this->value = 'NULL';
        $this->mysql_datatype = 'DATETIME';

        $this->set_properties($properties);
    }

    public function get_value() {
        if($this->value == "" || $this->value == "NULL") {
            return "NULL";
        }
        return date(_MYSQL_DATETIMEFORMAT_,strtotime($this->value));
    }

    public function display_value() {
        if ($this->value != "") {
            return date(_DATETIMEFORMAT_, strtotime($this->value));
        }
        return "";
    }

    public function input_control($object, $name, $action, $inline = false) {
        $displayName = ($this->displayName == '') ? ucwords(str_replace("_"," ",$name)) : $this->displayName;

        $helpText = '';
        if ($this->helpText != '') {
            $helpText = "
                <i class='fa fa-question-circle'></i> <small>{$this->helpText}</small>
            ";
        }

        $required = $this->input_control_attr('required');
        if($required != 'required') {
            if($action == 'add') {
                $required = $this->input_control_attr('requiredOnAdd');
            } elseif ($action == 'update') {
                $required = $this->input_control_attr('requiredOnUpdate');
            }
        }

        $readonly = $this->input_control_attr('readonly');
        if($readonly != 'readonly') {
            if($action == 'add') {
                $readonly = $this->input_control_attr('readonlyOnAdd');
            } elseif ($action == 'update') {
                $readonly = $this->input_control_attr('readonlyOnUpdate');
            }
        }

        $value = ($this->get_value() != "NULL") ? date(_DATETIMEFORMAT_, strtotime($this->get_value())) : "";

        $displayNone = $this->input_control_attr('hidden');

        $requiredIndicator = ($required == 'required') ? '<span style="color: red;">*</span>' : '';
        $inputControl ='
        <div class="form-group" style="'.$displayNone.'">
            <label for="'.$name.'">'.$displayName.': '.$requiredIndicator.'</label>
            <div class="input-group date datetimepicker">
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    <span class="glyphicon glyphicon-time"></span>
                </span>
                <input type="text" class="form-control" id="'.$name.'" name="'.$name.'" value="'.$value.'" '.$required.' '.$readonly.'/>
            </div>
            '.$helpText.'
        </div>';
        return $inputControl;
    }

    public function filter_control($controlName,$selectedValue) {
        return "
        <input type='text' class='daterange form-control' name='{$controlName}' value='$selectedValue' placeholder='Select date here..' />
        ";
    }
}

class ForeignKey extends Field {
    public $modelName;
    public $modelParent;

    // Another foreign key value is dependent on this foreign key value.
    // Will be use in filtering options. Ex. 1 item has particular set of unit of measures.
    public $dependsOn;
    public $linkerObj;
    public $linkerDependeeField;
    public $linkerDependeeValue;
    public $linkerDependentField;

    public function __construct($properties = array()) {
        parent::__construct();

        $this->required = true;
        $this->value = 0;
        $this->mysql_datatype = 'INTEGER';
        $this->modelName = "";
        $this->modelParent = true;
        $this->dependsOn = "";
        $this->linkerObj = "";
        $this->linkerDependeeField = "";
        $this->linkerDependeeValue = "";
        $this->linkerDependentField = "";

        $this->set_properties($properties);
    }

    public function get_model_name() {
        return $this->modelName;
    }

    public function get_object() {
        if ($this->value == '0') {
            return new $this->modelName;
        } else {
            $obj = new $this->modelName;
            $obj->get($this->value);
            return $obj;
        }
    }

    public function display_value() {
        $obj = $this->get_object();
        return $obj;
    }

    public function input_control($object, $name, $action, $inline = false) {
        $displayName = ($this->displayName == '') ? ucwords(str_replace("_"," ",$name)) : $this->displayName;

        $helpText = '';
        if ($this->helpText != '') {
            $helpText = "
                <i class='fa fa-question-circle'></i> <small>{$this->helpText}</small>
            ";
        }

        $required = $this->input_control_attr('required');
        if($required != 'required') {
            if($action == 'add') {
                $required = $this->input_control_attr('requiredOnAdd');
            } elseif ($action == 'update') {
                $required = $this->input_control_attr('requiredOnUpdate');
            }
        }

        $readonly = $this->input_control_attr('readonly');
        if($readonly != 'readonly') {
            if($action == 'add') {
                $readonly = $this->input_control_attr('readonlyOnAdd');
            } elseif ($action == 'update') {
                $readonly = $this->input_control_attr('readonlyOnUpdate');
            }
        }

        $dependsOn = "";
        $options = "";
        $modelName = $this->modelName;
        if ($this->dependsOn == "" || $this->linkerObj == "" || $this->linkerDependeeField == "" || $this->linkerDependentField == "") {
            // all options will be fetched and displayed as options
            $model = new $modelName;
            $qs = $model->fetchOptions($this->get_value());
            foreach ($qs as $record) {
                if ($record->id != 0) {
                    $selected = ($record->id == $this->get_value()) ? "selected" : "";
                    $options .= "<option value='{$record->id}' {$selected}>{$record}</option>";
                }
            }
            
        } else {
            // no options will be displayed. parent object must be selected first.
            $dependsOn = "dependent";
            
            // fetch options for the currently selected dependee value
            $linkerObj = $this->linkerObj;
            $linkerDependeeField = $this->linkerDependeeField;
            $linkerDependeeValue = $this->linkerDependeeValue;
            $linkerDependentField = $this->linkerDependentField;

            $obj = new $linkerObj;

            $filterValue = $linkerDependeeValue;
            $filter = $linkerDependeeField . "__exact";

            // Check if $linkerDependeeField has parent
            if (strpos($linkerDependeeField,'__') !== false) {
                $temp = explode("__", $linkerDependeeField);
                $tempN = count($temp);

                $field = $temp[$tempN - 1];

                $parentName = $temp[$tempN - 2];
                $parentObj = new $parentName;
                $parentObj->get($linkerDependeeValue);
                
                $filterValue = $parentObj->{$field};

                $filter = $field . "__exact";
            }
            
            $qs = $obj->fetchOptions('', $filter, $filterValue);
            $existingOptions = array();
            $inOptions = false;
            foreach ($qs as $record) {
                if ($record->get_field_object($linkerDependentField) instanceof ForeignKey) {
                    $dependentObjLoad = "{$linkerDependentField}__load";
                    $dependentObj = $record->{$dependentObjLoad}();
                } else {
                    // The given $linkerDependentField is not an object.
                    // It's a field of $record.
                    $dependentObj = $record;
                }
                // Prevent duplicate of options
                if (!isset($existingOptions[$dependentObj->id])) {
                    $selected = ($dependentObj->id == $this->get_value()) ? "selected" : "";
                    if ($inOptions == false && $dependentObj->id == $this->get_value()) {
                        $inOptions = true;
                    }
                    $options .= "<option value='{$dependentObj->id}' {$selected}>{$dependentObj}</option>";
                    $existingOptions[$dependentObj->id] = true;
                }
            }
            if (!$inOptions) {
                $obj = new $this->modelName;
                $obj->get($this->get_value());
                if ($obj->id != 0) {
                    $options .= "<option value='{$obj->id}' selected>{$obj}</option>";
                }
            }
        }

        $href = "page/fk-add.php?parent={$object}&field={$name}";
        $a = '<a align="right" href="'.$href.'" class="iframe">here</a>';

        $addnew = '';
        if(!$inline) {
            $addnew = '<div><small>If the '.$displayName.' you want is not in options add new '.$a.'</small></div>';
        }

        $displayNone = $this->input_control_attr('hidden');

        $requiredIndicator = ($required == 'required') ? '<span style="color: red;">*</span>' : '';
        $inputControl ='
        <div class="form-group" style="'.$displayNone.'">
            <label for="'.$name.'">'.$displayName.': '.$requiredIndicator.'</label> <i id="'.$name.'-spinner" class="fa fa-refresh fa-spin fade "></i>
            <select class="foreignkey form-control chosen-select '.$dependsOn.'" data-dependsOn="'.$this->dependsOn.'" data-linkerObj="'.$this->linkerObj.'" data-linkerDependeeField="'.$this->linkerDependeeField.'" data-linkerDependentField="'.$this->linkerDependentField.'" id="'.$name.'" name="'.$name.'" value="'.$this->get_value().'" '.$required.' '.$readonly.'>
                <option value="" selected>--- Select '.ucwords(str_replace("_"," ",$modelName)).' ---</option>
                '.$options.'
            </select>
            '.$helpText.'
            '.$addnew.'
        </div>';
        return $inputControl;
    }
}

class M2MField extends Field {

    // For example, if your dealing with user object and its m2m to group_user then
    // other object is group and obviously your current object is user.

    // By default, the column in group_user table that represents the group and user
    // will have the same name as their model.

    // In case that the column name is different from the object name then you
    // assign a different name using $otherObjectIdName and $currentObjectIdName.

    public $otherObjectName = ""; // represents the object were the current object connects
    public $otherObjectIdName = ""; // column name of other object which stands for its id

    public $objectName = ""; // represents the current object
    public $objectIdName = ""; // column name of current object which stands for its id

    // Another foreign key value is dependent on this foreign key value.
    // Will be use in filtering options. Ex. 1 item has particular set of unit of measures.
    public $dependsOn;
    public $linkerObj;
    public $linkerDependeeField;
    public $linkerDependeeValue;
    public $linkerDependentField;

    public function __construct($properties = array()) {
        parent::__construct();

        $this->set_properties($properties);

        if ($this->otherObjectIdName == "") {
            $this->otherObjectIdName = $this->otherObjectName;
        }

        if ($this->objectIdName == "") {
            $this->objectIdName = $this->objectName;
        }


        $this->dependsOn = "";
        $this->linkerObj = "";
        $this->linkerDependeeField = "";
        $this->linkerDependeeValue = "";
        $this->linkerDependentField = "";
    }

    public function getOptions() {
        $other_object = new $this->otherObjectName;
        return $other_object->filter()->fetch();
    }
}

?>
