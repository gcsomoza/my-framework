<?php

include "db.class.php";
include "field.php";

function _OR() {
    $expressions = func_get_args();
    return new Logic($expressions,'OR');
}

function _AND() {
    $expressions = func_get_args();
    return new Logic($expressions,'AND');
}

function _NOT($expression) {
    return new Not($expression);
}

class Logic {
    # This class handles the logical operation of WHERE clause in
    # SELECT statement.
    # This class is a collection of expressions evaluated by the provided logic.

    public $logic = 'AND'; # Either `AND` or `OR`
    public $expressions = array();

    public function __construct($expressions, $logic = 'AND') {
        $this->logic = $logic;
        $this->expressions = $expressions;
    }

    public function get() {
        $logic = $this->logic;
        $logical_expression = "";
        foreach ($this->expressions as $expression) {
            $logical_expression .= "{$expression->get()} {$logic} ";
        }
        $logical_expression = trim($logical_expression, " {$logic} ");
        return "({$logical_expression})";
    }
}

class Not {
    # This class handles the negation of an expression

    public $expression; # Either a Logic class or an Expression class

    public function __construct($expression) {
        $this->expression = $expression;
    }

    public function get() {
        return " not ({$this->expression->get()})";
    }
}

class Expression {
    # This class will handle the information about the expression that will
    # be used in SELECT statement's WHERE clause

    public $table;
    public $field;
    public $operator;
    public $value;

    public function __construct($table, $field, $operator, $value) {
        $this->table = $table;
        $this->field = $field;
        $this->operator = $operator;
        $this->value = $value;
    }

    public function get() {
        $table = $this->table;
        $field = $this->field;
        $operator = $this->operator;
        $value = $this->value;

        $expression = '';
        switch ($this->operator) {
            case 'year':
                $expression = "EXTRACT('year' FROM `{$table}`.`{$field}`) = '{$value}'";
            break;

            case 'month':
                $expression = "EXTRACT('month' FROM `{$table}`.`{$field}`) = '{$value}'";
            break;

            case 'day':
                $expression = "EXTRACT('day' FROM `{$table}`.`{$field}`) = '{$value}'";
            break;

            case 'week_day':
                $expression = "EXTRACT('dayofweek' FROM `{$table}`.`{$field}`) = '{$value}'";
            break;

            case 'IN':
                $expression = "`{$table}`.`{$field}` {$operator} {$value}";
            break;

            default:
                if ($value != 'NULL')
                    $value = "'{$value}'";
                $expression = "`{$table}`.`{$field}` {$operator} {$value}";
                break;
        }

        return $expression;
    }
}

class CalculatedField {
    # This class handles the creation of expression for calculated field.
    # This will be primarily used before updating an object.
    #
    # An example of calculated field in SQL query is:
    # `item`.`quantity` = `item`.`quantity` + 1
    # An example usage of CalculatedField class is:
    #
    # $item->quantity = new CalculatedField($previous_value,'+',1);
    # $item->save();

    public $previous_value;
    public $operator; # Must be the following * (multiply), / (diveded by), + (plus), or - (minus)
    public $value;

    public function __construct($previous_value, $operator, $value) {
        $this->previous_value = $previous_value;
        $this->operator = $operator;
        $this->value = $value;
    }

    public function assumed_value() {
        # In case that the CalculatedField was called before updating an object,
        # this function will return an assumed value.
        # The real value will still be in the database and it will be fetched after updating an object.

        switch ($this->operator) {
            case '*':
                return ($this->previous_value * $this->value);
            break;

            case '/':
                return ($this->previous_value / $this->value);
            break;

            case '+':
                return ($this->previous_value + $this->value);
            break;

            case '-':
                return ($this->previous_value - $this->value);
            break;

            default:
                throw new Exception("Invalid operation was given in CalculatedField while getting the data in object.", 1);
            break;
        }
    }
}

class ModelDatabase {
    # This class handles the creation of query that will be used primarily in
    # select statement's WHERE clause.
    #
    # This class also handles anything which is related to database access.

    # Holds the table name where the current object will be joined.
    #
    # It will be used to construct FROM clause whenever the filter() function
    # will be called.
    #
    # This variable will be populated on every expression that will be made from
    # this object.
    #
    # Usage: $this->joins[$table] = $given_field
    protected $joins = array();

    protected $fields = array();
    public $modified = array();
    protected $loaded = array();

    protected $select = "";
    protected $orders = array();
    protected $query_start = -1;
    protected $query_limit = -1;

    public $db;
    public $perform_db = true; // set to true if you want to perform db connect, start_transaction, rollback, commit, close.
    public $db_assigned = false; // set to true if $db was assigned to this model from outside.

    public function get_db() {
        // if ($this->db_assigned == true) {
        //     return $this->db;
        // }
        // return null;
        $this->db;
    }

    # This variable will hold the foreign key with their foregn table name as array key.
    # This represents Many-to-Many relationship.
    # Usage: $m2m[<foreign_table_name>] = new M2MField($properties)
    public $m2m = array();

    public function set_db($db = null) {
        // If db is given then db functions will not be performed for this model.
        // db functions are connect, start_transaction, rollback, commit, close.
        if ($db instanceof DB) {
            $this->db = $db;
            $this->perform_db = false;
            $this->db_assigned = true;
        } else {
            $this->db = new DB();
        }
    }

    public function __setdb($db = null) {
        // If db is given then db functions will not be performed for this model.
        // db functions are connect, start_transaction, rollback, commit, close.
        if ($db instanceof DB) {
            $this->db = $db;
            $this->perform_db = false;
            $this->db_assigned = true;
        } else {
            $this->db = new DB();
        }
    }

    public function __construct($db = null) {
        // If db is given then db functions will not be performed for this model.
        // db functions are connect, start_transaction, rollback, commit, close.
        if ($db instanceof DB) {
            $this->db = $db;
            $this->perform_db = false;
        } else {
            $this->db = new DB();
        }
    }

    public function __call($function, $value) {
        $function = explode("__", $function);
        $functionCount = count($function);
        if ($functionCount == 3) {
            $value = $value[0];
            $table = $function[0];
            $field = $function[1];
            $operator = $function[2];

            $given_field = $table; // given field
            $temp = $this->fields[$given_field];
            if ($temp instanceof ForeignKey) {
                // Sometimes the field name is not the same to its model name.
                // We get the model name that this given field represents
                $table = $temp->modelName;
            }
            $this->joins[$table] = $given_field;

        } elseif ($functionCount == 2) {
            $table = strtolower(get_class($this));
            $field = $function[0];
            $operator = $function[1];

            if ($operator == 'set') {
                # Retrieve the list of given foreign field
                return $this->field_set($table, $field, $value);
            } elseif ($operator == 'load' || $operator == 'obj') {
                # Load the foreign key object data
                if (!isset($this->loaded[$field])) {
                    $this->loaded[$field] = $this->foreign_object_preload($field);
                }
                return $this->loaded[$field];
            }

            $value = $value[0];

        } else {
            throw new Exception("Only one foreign table can be processed.", 1);
        }

        $expression = $this->construct_select_expression($table, $field, $operator, $value);

        return $expression;
    }

    public function field_set($table, $foreign_table, $args) {
        # Retrieve the list of given foreign field

        $sql = "SELECT * FROM `{$foreign_table}` WHERE `{$foreign_table}`.`{$table}` = '{$this->id}'";

        $where_clause = "";
        foreach ($args as $expression) {
            # $expression can be instanceof Expression class or instanceof Logic class or instanceof Not class.
            # Both class can return expression as string by calling the get() function.
            $where_clause .= "{$expression->get()} AND ";
        }
        $where_clause = $where_clause . " `{$foreign_table}`.`is_deleted` = 0 ";

        if ($where_clause != "")
            $sql .= " AND {$where_clause}";

        $perform = $this->perform_db;
        $db = $this->db;
        $result = $this->execute_query_list($sql,$perform,$db);

        $list = array();
        if($result['status'] == 'OK') {
            $result_list = $result['result'];
            foreach ($result_list as $record) {
                $object_name = $foreign_table;
                $object = new $object_name;
                foreach ($object->get_fields() as $field => $value) {
                    $object->{$field} = $record[$field];
                }
                $list[] = $object;
            }
        }

        return $list;
    }

    public function foreign_object_preload($foreign_object) {
        $foreign_object_table = $this->fields[$foreign_object]->modelName;
        $obj = new $foreign_object_table($this->get_db());
        $obj->get($this->{$foreign_object});
        return $obj;
    }

    public function construct_select_expression($table, $field, $operator, $value) {
        $expression = "";

        switch ($operator) {
            case 'in':
                $operator = 'IN';
                if(is_array($value)) {
                    $temp = "";
                    foreach ($value as $val) {
                        $temp .= "'{$val}',";
                    }
                    $temp = trim($temp, ",");
                    $value = $temp;
                }
                $value = "({$value})";
            break;

            case 'exact':
                if($value == null) {
                    $operator = 'IS';
                    $value = 'NULL';
                }
                else {
                    $operator = '=';
                }
            break;

            case 'iexact':
                $operator = 'ILIKE';
            break;

            case 'contains':
                $operator = 'LIKE';
                $value = "%{$value}%";
            break;

            case 'icontains':
                $operator = 'ILIKE';
                $value = "%{$value}%";
            break;

            case 'gt':
                $operator = '>';
            break;

            case 'gte':
                $operator = '>=';
            break;

            case 'lt':
                $operator = '<';
            break;

            case 'lte':
                $operator = '<=';
            break;

            case 'startswith':
                $operator = 'LIKE';
                $value = "{$value}%";
            break;

            case 'istartswith':
                $operator = 'ILIKE';
                $value = "{$value}%";
            break;

            case 'endswith':
                $operator = 'LIKE';
                $value = "%{$value}";
            break;

            case 'iendswith':
                $operator = 'ILIKE';
                $value = "%{$value}";
            break;

            case 'year':
                $operator = 'year';
            break;

            case 'month':
                $operator = 'month';
            break;

            case 'day':
                $operator = 'day';
            break;

            case 'week_day':
                $operator = 'week_day';
            break;

            default:
                throw new Exception("Invalid expression operator was given", 1);
                break;
        }

        $expression = new Expression($table, $field, $operator, $value);

        return $expression;
    }

    protected function execute_query($sql,$action,$perform, $db) {
        $db->connect($perform);
        $db->start_transaction($perform);

        $query = $db->query($sql);

        if ($query === false) {
            $result = array(
                'status' => 'ERROR',
                'msg' => $db->errno() . ": " . $db->error() . ", SQL: " . $sql
            );
            $db->rollback($perform);
            $db->close($perform);
            return $result;
        }

        $result = array(
            'status' => 'OK',
            'msg' => str_replace("_"," ",ucwords(get_class($this))) . " successfully {$action}.",
        );

        if ($action == "added") {
            # Return newly inserted id.
            $result['id'] = $db->insert_id();
        }

        $db->commit($perform);
        $db->close($perform);

        return $result;
    }

    protected function execute_query_get($sql,$perform,$db) {
        $db->connect($perform);
        $db->start_transaction($perform);

        $query = $db->query($sql);

        if ($query === false) {
            $result = array(
                'status' => 'ERROR',
                'msg' => $db->errno() . ": " . $db->error() . ", SQL: " . $sql
            );
            $db->rollback($perform);
            $db->close($perform);
            return $result;
        }
        else {
            $found = false;
            while ($row = $db->fetch_assoc($query)) {
                $found = true;
                $fields = $this->fields;
                foreach ($fields as $field => $value) {
                    $this->{$field} = $row[$field];
                }
            }
            $db->free_result($query);

            # Record does not exists.
            if (!$found) {
                $result = array(
                    'status' => 'ERROR',
                    'msg' => str_replace("_"," ",ucwords(get_class($this))) . " does not exist."
                );
                $db->rollback($perform);
                $db->close($perform);
                return $result;
            }

            # Reset modified fields
            $this->modified = array();
        }

        $result = array(
            'status' => 'OK',
            'msg' => str_replace("_"," ",ucwords(get_class($this))) . " successfully fetched.",
        );

        $db->commit($perform);
        $db->close($perform);

        return $result;
    }

    protected function execute_query_list($sql,$perform,$db) {
        $db->connect($perform);
        $db->start_transaction($perform);

        $query = $db->query($sql);

        if ($query === false) {
            $result = array(
                'status' => 'ERROR',
                'msg' => $db->errno() . ": " . $db->error() . ", SQL: " . $sql
            );
            $db->rollback($perform);
            $db->close($perform);
            return $result;
        }
        else {
            $found = false;
            $query_result = array();
            while ($row = $db->fetch_assoc($query)) {
                $found = true;
                $query_result[] = $row;
            }
            $db->free_result($query);

            # Record does not exists.
            if (!$found) {
                $result = array(
                    'status' => 'ERROR',
                    'msg' => str_replace("_"," ",ucwords(get_class($this))) . " does not exist."
                );
                $db->rollback($perform);
                $db->close($perform);
                return $result;
            }
        }

        $result = array(
            'status' => 'OK',
            'msg' => "",
            'result' => $query_result
        );

        $db->commit($perform);
        $db->close($perform);

        return $result;
    }

    public function get($id, $get_removed_item = false) {
        # Get the object's data.
        #
        # By default, removed items are not retrieved or considered not exist.
        #
        # Setting $get_removed_item to true will also retrieve the removed items data.

        $table = strtolower(get_class($this));

        $sql = "SELECT * FROM `{$table}` WHERE `{$table}`.`id` = '{$id}'";

        if (!$get_removed_item) {
            $sql .= " AND `{$table}`.`is_deleted` = '0'";
        }

        $perform = $this->perform_db;
        $db = $this->db;
        $result = $this->execute_query_get($sql,$perform,$db);

        return $result;
    }

    public function filter() {
        $num_args = func_num_args();

        $table = strtolower(get_class($this));

        $distinct = "";
        $from_clause = "`{$table}`";
        foreach ($this->joins as $foreign_table => $given_field) {
            $distinct = "DISTINCT";
            $from_clause .= " INNER JOIN `{$foreign_table}` ON `{$table}`.`{$given_field}` = `{$foreign_table}`.`id` ";
        }

        $sql = "SELECT {$distinct} `{$table}`.* FROM ";
        $sql .= $from_clause;

        # do not include deleted records
        $where_clause = " `{$table}`.`is_deleted` = '0' ";
        $args = func_get_args();
        foreach ($args as $expression) {
            # $expression can be instanceof Expression class or instanceof Logic class or instanceof Not class.
            # Both class can return expression as string by calling the get() function.
            $where_clause .= " AND {$expression->get()} ";
        }

        if ($where_clause != "")
            $sql .= " WHERE {$where_clause}";

        $this->select = $sql;
        return $this;
    }

    public function order_by() {
        # This function will be used to construct ORDER BY statement.
        #
        # This accepts any number of string parameters representing the field name.
        # Provide - (minus) to represent descending order otherwise it will be in ascending order.

        $this->orders = func_get_args();
        return $this;
    }

    public function limit($start, $limit) {
        # This function will be used to construct LIMIT statement.

        $this->query_start = $start;
        $this->query_limit = $limit;
        return $this;
    }

    public function fetch() {

        $table = strtolower(get_class($this));

        $sql = $this->select;

        $order_by_clause = "";
        foreach ($this->orders as $order_field) {
            $order_table = $table;
            $sort = "ASC";
            # Check if sort is given
            if (substr($order_field,0,1) == '-') {
                $order_field = substr($order_field,1,(strlen($order_field) - 1));
                $sort = 'DESC';
            }
            # Check if table is provided
            $temp = explode("__", $order_field);
            if(count($temp) == 2) {
                $order_table = $temp[0];
                $order_field = $temp[1];
            }
            $order_by_clause .= "`{$order_table}`.`{$order_field}` {$sort}, ";
        }
        $order_by_clause = trim($order_by_clause, ", ");

        if ($order_by_clause != "")
            $sql .= " ORDER BY {$order_by_clause}";

        if ($this->query_start > -1 AND $this->query_limit > -1)
            $sql .= " LIMIT {$this->query_start}, {$this->query_limit}";

        $perform = $this->perform_db;
        $db = $this->db;
        $result = $this->execute_query_list($sql,$perform,$db);

        $list = array();
        if($result['status'] == 'OK') {
            $result_list = $result['result'];
            // foreach ($result_list as $record) {
            //     $object_name = get_class($this);
            //     $object = new $object_name;
            //     foreach ($object->get_fields() as $field => $value) {
            //         $object->{$field} = $record[$field];
            //     }
            //     $list[] = $object;
            // }
            $list = $this->toObject($result_list);
        }

        # Clear query related protected fields
        $this->select = "";
        $this->joins = array();
        $this->orders = array();
        $this->query_start = -1;
        $this->query_limit = -1;

        return $list;
    }

    public function toObject($result_list) {
        $list = array();
        foreach ($result_list as $record) {
            $object_name = get_class($this);
            $object = new $object_name;
            foreach ($object->get_fields() as $field => $value) {
                $object->{$field} = $record[$field];
            }
            $list[] = $object;
        }

        return $list;
    }

    public function fetchDistinct() {
        if (strpos($this->select, 'DISTINCT') === false) {
            $this->select = str_replace("SELECT", "SELECT DISTINCT", $this->select);
        }
        call_user_func_array(array($this,'fetch'), func_get_args());
    }

    public function fetchOptions($selectedValue, $filter = null, $filterValue = null) {
        if ($filter !== null && $filterValue !== null) {
            $this->filter($this->{$filter}($filterValue));
        } else {
            $this->filter();
        }
        return call_user_func_array(array($this,'fetch'), func_get_args());
    }

    public function count() {

        $table = strtolower(get_class($this));

        $sql = $this->select;

        $sql = str_replace("`{$table}`.*", "`{$table}`.`id`", $sql);

        $order_by_clause = "";
        foreach ($this->orders as $order_field) {
            $order_table = $table;
            $sort = "ASC";
            # Check if sort is given
            if (substr($order_field,0,1) == '-') {
                $order_field = substr($order_field,1,(strlen($order_field) - 1));
                $sort = 'DESC';
            }
            # Check if table is provided
            $temp = explode("__", $order_field);
            if(count($temp) == 2) {
                $order_table = $temp[0];
                $order_field = $temp[1];
            }
            $order_by_clause .= "`{$order_table}`.`{$order_field}` {$sort}, ";
        }
        $order_by_clause = trim($order_by_clause, ", ");

        if ($order_by_clause != "")
            $sql .= " ORDER BY {$order_by_clause}";

        if ($this->query_start > -1 AND $this->query_limit > -1)
            $sql .= " LIMIT {$this->query_start}, {$this->query_limit}";

        $sql = "SELECT COUNT(`id`) as `count` FROM (" . $sql . ") as `count_table`;";

        $perform = $this->perform_db;
        $db = $this->db;
        $result = $this->execute_query_list($sql,$perform,$db);

        $count = 0;
        if($result['status'] == 'OK') {
            $count = $result['result'][0]['count'];
        }

        return $count;
    }

    public function save() {
        // Not updating if delete_by is not set.
        $this->delete_by = '0';

        if ($this->id == '0') {
            $this->create_date = date("Y-m-d H:i:s");
            if (isset($_SESSION['auth'])) {
                $this->create_by = $_SESSION['auth']['userid'];
            }
            return $this->insert();
        } else {
            $this->update_date = date("Y-m-d H:i:s");
            if (isset($_SESSION['auth'])) {
                $this->update_by = $_SESSION['auth']['userid'];
            }
            return $this->update();
        }
    }

    public function delete() {

        if ($this->id == '')
            throw new Exception("Unable to delete an object. Object id does not have a value.", 1);

        $table = strtolower(get_class($this));

        $sql = "DELETE FROM `{$table}` WHERE `{$table}`.`id` = '{$this->id}';";

        $perform = $this->perform_db;
        $db = $this->db;
        $db->connect($perform);
        $db->start_transaction($perform);
        $perform_db_inside = false;
        $result = $this->execute_query($sql,"deleted",$perform_db_inside,$db);

        if ($result['status'] == 'OK') {
            # Delete all the data connected to this object from foreign table
            // foreach ($this->inlines as $foreign_table => $foreign_key) {
            //     $sql = "DELETE FROM `{$foreign_table}` WHERE `{$foreign_key}` = '{$this->id}'";
            //     $foreign_table_result = $this->execute_query($sql,"deleted",$perform_db_inside,$db);
            //     if ($foreign_table_result['status'] == 'ERROR') {
            //         $db->rollback($perform);
            //         $db->close($perform);
            //         return $foreign_table_result;
            //     }
            // }
        }
        else {
            $db->rollback($perform);
            $db->close($perform);
            return $result;
        }

        $db->commit($perform);
        $db->close($perform);

        return $result;
    }

    public function remove($remove = true) {
        # Removes an object saved in the database.
        #
        # This only updates the `is_deleted` field to '1' which represents yes otherwise '0'
        #
        # Setting $remove parameter to false restores the deleted object.

        $value = '1';
        $remarks = 'removed';
        if (!$remove) {
            $value = '0';
            $remarks = 'restored';
        }

        if ($this->id == '')
            throw new Exception("Unable to remove an object. Object id does not have a value.", 1);

        $table = strtolower(get_class($this));

        $this->delete_date = date("Y-m-d H:i:s");
        if (isset($_SESSION['auth'])) {
            $this->delete_by = $_SESSION['auth']['userid'];
        }
        $sql = "UPDATE
                    `{$table}`
                SET
                    `is_deleted` = '{$value}',
                    `delete_date` = '{$this->delete_date}',
                    `delete_by` = {$this->delete_by}
                WHERE
                    `{$table}`.`id` = '{$this->id}';";

        $perform = $this->perform_db;
        $db = $this->db;
        $db->connect($perform);
        $db->start_transaction($perform);
        $perform_db_inside = false;
        $result = $this->execute_query($sql,$remarks,$perform_db_inside,$db);

        if ($result['status'] == 'OK') {
            # Remove all the data connected to this object from foreign table
            // foreach ($this->foreign_tables as $foreign_table => $foreign_key) {
            //     $sql = "UPDATE `{$foreign_table}` SET `is_deleted` = '{$value}' WHERE `{$foreign_key}` = '{$this->id}'";
            //     $foreign_table_result = $this->execute_query($sql,$remarks,$perform_db_inside,$db);
            //     if ($foreign_table_result['status'] == 'ERROR') {
            //         $db->rollback($perform);
            //         $db->close($perform);
            //         return $foreign_table_result;
            //     }
            // }
        }
        else {
            $db->rollback($perform);
            $db->close($perform);
            return $result;
        }

        $db->commit($perform);
        $db->close($perform);

        return $result;
    }

    private function insert() {

        if ($this->id != '0')
            throw new Exception("Unable to insert an object. Object id should not have a value.", 1);

        $table = strtolower(get_class($this));

        $fields = "";
        $values = "";
        foreach ($this->fields as $field => $val) {
            $value = $val->get_value();
            if ($field == 'id' OR $field == 'is_deleted')
                continue;
            $fields .= "`{$field}`,";
            if ($value === false) {
                $value = 0;
            }
            if ($value instanceof CalculatedField) {
                $value = $value->value;
            }
            if ($this->fields[$field] instanceof ForeignKey) {
                if ($value == '') {
                    $value = 0;
                }
            }
            $values .= ($value != 'NULL') ? "'$value'," : "$value,";
        }
        $fields = trim($fields, ",");
        $values = trim($values, ",");

        $sql = "INSERT INTO `{$table}` ({$fields}) VALUES ({$values});";

        $perform = $this->perform_db;
        $db = $this->db;
        $result = $this->execute_query($sql,"added",$perform,$db);

        if ($result['status'] == 'OK') {
            $this->get($result['id']);
        }

        return $result;
    }

    private function update() {

        if ($this->id == '')
            throw new Exception("Unable to update object. Object id does not have a value.", 1);

        if (!empty($this->modified)) {
            $table = strtolower(get_class($this));

            $fetch_object_after_update = false;
            $expression = "";
            foreach ($this->modified as $field) {
                $value = $this->{$field};
                if ($value instanceof CalculatedField) {
                    $fetch_object_after_update = true;
                    $expression .= "`{$table}`.`{$field}` = `{$table}`.`{$field}` {$value->operator} {$value->value}, ";
                }
                else {
                    if ($this->fields[$field] instanceof ForeignKey) {
                        if ($value == '') {
                            $value = 0;
                        }
                    } elseif ($this->fields[$field] instanceof BooleanField) {
                        if ($value == true || $value === true) {
                            $value = 1;
                        } else {
                            $value = 0;
                        }
                    }
                    $value = ($value != 'NULL') ? "'$value'" : $value;
                    $expression .= "`{$table}`.`{$field}` = {$value}, ";
                }
            }
            $expression = trim($expression, ", ");

            $sql = "UPDATE `{$table}` SET {$expression} WHERE `{$table}`.`id` = '{$this->id}';";

            $perform = $this->perform_db;
            $db = $this->db;
            $result = $this->execute_query($sql,"updated",$perform,$db);

            if ($result['status'] == 'OK') {
                if ($fetch_object_after_update) {
                    # Fetch object data
                    $this->get($this->id);
                }
            }

            return $result;
        }
    }

    function create_table() {
        $table = strtolower(get_class($this));

        $create_schema = "--------------------<br/>";
        $create_msg = "Table <strong>`{$table}`</strong> ";
        $alter_msg = "";

        $db = new DB();
        $db->connect();

        $result = $db->create_table($table);
        if($result === false) {
            $create_msg .= " {$db->error()}.";
        } else {
            $create_msg .= " successfully created.";
        }
        $create_msg .= "<br/><br/>";
        foreach($this->fields as $field => $value) {
            if($field == 'id' || $field == 'is_deleted') {
                continue;
            }
            $result = $db->alter_table($table,$field,$value->datatype(),$value->get_value());
            if($result === false) {
                $alter_msg .= "Column <strong>`{$field}`</strong> {$db->error()}. --> " . $value->datatype() . " " . $value->get_value();
            } else {
                $alter_msg .= "<span style='color: green;'>Column <strong>`{$field}`</strong> successfully added.</span>";
            }
            $alter_msg .= "<br/>";
        }
        $db->close();

        return $create_schema . $create_msg . $alter_msg . "<br/>";
    }
}

class Model extends ModelDatabase {

    public $maintainable = true;

    public function __construct($db = null) {
        $this->fields['id'] = new IntegerField();
        $this->fields['create_date'] = new DateTimeField(array("readonly"=>true,"canAdd"=>false,"hidden"=>true));
        $this->fields['create_by'] = new ForeignKey(array("modelName"=>"user","readonly"=>true,"hidden"=>true));
        $this->fields['update_date'] = new DateTimeField(array("readonly"=>true,"hidden"=>true));
        $this->fields['update_by'] = new ForeignKey(array("modelName"=>"user","readonly"=>true,"hidden"=>true));
        $this->fields['delete_date'] = new DateTimeField(array("hidden"=>true));
        $this->fields['delete_by'] = new ForeignKey(array("modelName"=>"user","required"=>false,"hidden"=>true));
        $this->fields['is_deleted'] = new BooleanField(array("value"=>false,"readonly"=>true,"hidden"=>true));

        parent::__construct($db);
    }

    public function __toString() {
        return str_replace("_", " ", strtolower(get_class($this)));
    }

    public function __get($name) {
        if (isset($this->fields[$name])) {
            if ($this->fields[$name]->get_value() instanceof CalculatedField) {
                return $this->fields[$name]->get_value()->assumed_value();
            }
            return $this->fields[$name]->get_value();
        }
        else {
            throw new Exception("The field you are trying to get does not exists.", 1);
        }
    }

    public function __set($name, $value) {
        if (isset($this->fields[$name])) {
            $this->fields[$name]->set_value($value);
            if($name != 'id')
                $this->modified[$name] = $name;
        }
        else {
            throw new Exception("The field you are trying to set does not exists.", 1);
        }
    }

    public function __return() {
        return "";
    }

    public function __returnAdd() {
        return $this->__return();
    }

    public function __returnUpdate() {
        return $this->__return();
    }

    public function __platform() {
        return get_class($this);
    }

    public function __platformAdd() {
        return $this->__platform();
    }

    public function __platformUpdate() {
        return $this->__platform();
    }

    public function verbose() {
        return ucwords(str_replace("_"," ",get_class($this)));
    }

    public function get_fields() {
        return $this->fields;
    }

    public function get_field_object($field) {
        if (isset($this->fields[$field])) {
            return $this->fields[$field];
        }
        return null;
    }

    private function queryset($queryParams) {
        $adminName = get_class($this) . "_admin";
        $admin = new $adminName;
        $filterParams = array();

        if (isset($queryParams["q"]) && $queryParams["q"] != "") {
            $searchFieldsParams = array();
            foreach ($admin->searchFields as $field) {
                $fieldFuncName = $field . "__contains";
                $value = $queryParams["q"];
                $searchFieldsParams[] = $this->{$fieldFuncName}($value);
            }
            $filterParams[] = call_user_func_array('_OR', $searchFieldsParams);
        }

        unset($queryParams["q"]);
        foreach($queryParams as $key => $value) {
            if ($value != "") {
                // Get field type of $key
                $field = $this->get_field_object($key);
                if ($field instanceof DateTimeField || $field instanceof DateField) {
                    // Parse from date and to date (`MM/DD/YYYY - MM/DD/YYYY`)

                    $dates = explode(' - ', $value);

                    $from = date("Y-m-d 00:00:00", strtotime($dates[0]));
                    $to = date("Y-m-d 23:59:59", strtotime($dates[0]));

                    $fromFuncName = $key . "__gte";
                    $toFuncName = $key . "__lte";
                    
                    $filterParams[] = _AND($this->{$fromFuncName}($from), $this->{$toFuncName}($to));

                } else {
                    $fieldFuncName = $key . "__exact";
                    $filterParams[] = $this->{$fieldFuncName}($value);
                }
            }
        }

        return $filterParams;
    }

    public function count_queryset($queryParams) {
        $filterParams = $this->queryset($queryParams);

        call_user_func_array(array($this,'filter'), $filterParams);
        $result = $this->order_by("-create_date")->count();
        return $result;
    }

    public function get_queryset($queryParams, $start = null, $limit = null) {
        $filterParams = $this->queryset($queryParams);

        call_user_func_array(array($this,'filter'), $filterParams);
        $result = $this->order_by("-create_date")->limit($start, $limit)->fetch();
        return $result;
    }
}

?>
