<?php
// Returns a font-awesome icon of the given module
function GetModuleIcon($module) {
    global $modules;
    if(isset($modules[$module]["icon"])) {
        return $modules[$module]["icon"];
    }
    return "fa-square";
}

// Returns true if the current user is allowed to do
// anything on the given module.
function IsModulePermitted($module) {
    if(isset($_SESSION['auth'])) {
        $auth = $_SESSION['auth'];
        if($auth["superuser"]) {
            return true;
        }

        $up = new user_permission();
        $count = $up->filter(
            $up->user__exact($auth["userid"]),
            $up->permission__module__exact($module)
        )->count();
        if($count > 0) {
            return true;
        }

        $gp = new group_permission();
        $count = $gp->filter(
            $gp->group__in($auth["groups"]),
            $gp->permission__module__exact($module)
        )->count();
        if($count > 0) {
            return true;
        }
    }
    return false;
}
?>
