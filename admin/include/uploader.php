<?php

function upload($FILE, $folder = "", $filename = "", $overwrite = true) {

    global $_MEDIA_;

    $uploaded_file = $FILE;

    $basename = basename($uploaded_file["name"]);

    $target_dir = $_MEDIA_ . $folder . DIRECTORY_SEPARATOR;
    $target_file = $target_dir . $basename;
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    $target_file = ($filename == "") ? $target_dir . basename($uploaded_file["tmp_name"]) . '.' . $imageFileType : $target_dir . $filename . '.' . $imageFileType;

    $status = "OK";
    $msg    = "";
    $result = array();

    // Check if image file is a actual image or fake image

    $check = getimagesize($uploaded_file["tmp_name"]);
    if($check !== false) {
        $status = "OK";
        $msg    = "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        $status = "ERROR";
        $msg = "File is not an image.";
        $uploadOk = 0;
    }

    // Check if file already exists
    if(!$overwrite) {
        if ($uploadOk == 1 && file_exists($target_file)) {
            $status = "ERROR";
            $msg    = "File already exists. ";
            $uploadOk = 0;
        }
    }
    // Check file size
    if ($uploadOk == 1 && $uploaded_file["size"] > 5000000) {
        $status = "ERROR";
        $msg    = "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($uploadOk == 1 && $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        $status = "ERROR";
        $msg    = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        $result['status'] = $status;
        $result['msg']    = $msg . ' File not uploaded.';
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($uploaded_file["tmp_name"], $target_file)) {
            $status = "OK";
            $msg    = "The file ". basename($uploaded_file["name"]). " has been uploaded.";
            $data   = basename($target_file);
        } else {
            $status = "ERROR";
            $msg    = "Sorry, there was an error uploading your file.";
            $data   = "";
        }
        $result['status'] = $status;
        $result['msg']    = $msg;
        $result['filename']   = $data;
    }

    return $result;
}

?>
