<?php

function Compare($str1, $str2, $resultOnSuccess, $resuleOnFailure) {
    $result = $resuleOnFailure;
    if ($str1 == $str2) {
        return $resultOnSuccess;
    }
    return $result;
}

// Check whether a selected page is submenu of the collapsible menu.
function ActiveCollapsible($selectedPage, $pages) {
    if(in_array($selectedPage, $pages)) {
        return "";
    }
    return "collapse";
}

// Check whether a non-collapsible menu was selected.
function Active($selectedPage, $page) {
    return Compare($selectedPage, $page, "active", "");
}

// Get the application name based on the directory
function GetApp() {
    $subFolders = array(
        "action",
        "include",
        "static",
        "media",
        "page",
        "models",
        "api",
    );

    $doc_root = str_replace("/", _DS_, $_SERVER['DOCUMENT_ROOT']); // get the directory root. ex: /var/www/html
    $result = str_replace($doc_root, "", getcwd()); // remove the directory root string from result of getcwd()

    // $result = str_replace(_DS_ . 'admin', "", $result); // remove `/admin` in $result
    
    // Make sure that the last word is the current working directory outside admin folder.
    $split = explode(_DS_, trim($result, _DS_));
    $n = count($split);
    if ($n > 0) {
        $result = $split[0];
    }
    
    $result = _DS_ . $result;

    $subFolderPos = false;
    foreach ($subFolders as $subFolder) {
        $subFolderPos = strpos($result,$subFolder);
        if($subFolderPos !== false) {
            $result = substr($result,0,($subFolderPos - 1));
            break;
        }
    }

    return $result;
}

// Used by GetModels. Scan the given file for the class declared there.
function GetModels_file_get_php_classes($filepath, $str_replace = "_admin") {
    $php_code = file_get_contents($filepath);
    $classes = GetModels_get_php_classes($php_code, $str_replace);
    return $classes;
}

// Used by GetModels. Returns an array of classes.
function GetModels_get_php_classes($php_code, $str_replace = "_admin") {
    $classes = array();
    $tokens = token_get_all($php_code);
    $count = count($tokens);
    for ($i = 2; $i < $count; $i++) {
        if (   $tokens[$i - 2][0] == T_CLASS
            && $tokens[$i - 1][0] == T_WHITESPACE
            && $tokens[$i][0] == T_STRING) {

            $class_name = $tokens[$i][1];

            // Remove '_admin'. Example: 'item_admin' will become 'item'.
            $classes[] = str_replace($str_replace, "", $class_name);
        }
    }
    return $classes;
}

// Returns a list of classes name under models/<subfolder>/admin.php.
function GetModels() {
    $models = array();
    $folders = ScanDirectory(_MODELSDIR_);
    foreach ($folders as $folder) {
        $models[$folder] = GetModels_file_get_php_classes(_MODELSDIR_.$folder._DS_."admin.php");
    }
    return $models;
}

// Returns a list of classes name under models/<subfolder>/admin.php.
// This will be use for creation of database.
function GetModelsForCreateDB() {
    $models = array();
    $folders = ScanDirectory(_MODELSDIR_);
    foreach ($folders as $folder) {
        $models[$folder] = GetModels_file_get_php_classes(_MODELSDIR_.$folder._DS_."models.php");
    }
    return $models;
}

// Include the models in your script. Example: the models is in models/inventory/*
function IncludeModels() {
    $subfolders = ScanDirectory(_MODELSDIR_);
    foreach ($subfolders as $subfolder) {
        $dir = _MODELSDIR_.$subfolder;
        foreach (glob($dir._DS_."*.php") as $filename) {
            include $filename;
        }
    }
}

// Returns an array of filename or folder name in a given directory
function ScanDirectory($dir) {
    $result = array();
    if(is_dir($dir)) {
        $result = scandir($dir);
        unset($result[0]);
        unset($result[1]);
        if(($key = array_search("import.php", $result)) !== false) {
            unset($result[$key]);
        }
        if(($key = array_search("import-css.php", $result)) !== false) {
            unset($result[$key]);
        }
        if(($key = array_search("import-js.php", $result)) !== false) {
            unset($result[$key]);
        }
    }
    return $result;
}

function consolelog() {
    if(session_id() == '') {
        session_start();
    }
    if (!isset($_SESSION['consolelog'])) {
        $_SESSION['consolelog'] = array();
    }
    $n = func_num_args();
    
    if ($n > 0) {
        $args = func_get_args();
        foreach ($args as $key => $msg) {
            if(is_array($msg) || is_object($msg)) {
                $msg = json_encode($msg);
            } elseif (is_bool($msg)) {
                if ($msg == true) {
                    $msg = 'true';
                } else {
                    $msg = 'false';
                }
            }
            $_SESSION['consolelog'][] = $msg;
        }
    }
}

function display_consolelogs() {
    if (isset($_SESSION['consolelog'])) {
        print "<script>";
        foreach ($_SESSION['consolelog'] as $msg) {
            print "console.log('{$msg}');";
        }
        print "</script>";
        unset($_SESSION['consolelog']);
    }
}

function println() {

    $n = func_num_args();

    if ($n > 0) {

        $args = func_get_args();
        foreach ($args as $key => $arg) {
            if (is_array($arg) || is_object($arg)) {
                print "<pre>";
                print_r($arg);
                print "</pre>";
            } elseif (is_bool($arg)) {
                if ($arg == true) {
                    print "true ";
                } else {
                    print "false ";
                }
            } else {
                print $arg . " ";
            }
        }
    }
    print "<br />";
}

function debug() {
    
    $n = func_num_args();
    
    if ($n > 0) {

        $args = func_get_args();
        foreach ($args as $key => $arg) {
            if (is_array($arg) || is_object($arg)) {
                print "<pre>";
                print_r($arg);
                print "</pre>";
            } elseif (is_bool($arg)) {
                if ($arg == true) {
                    print "true ";
                } else {
                    print "false ";
                }
            } else {
                print $arg . " ";
            }
        }
    }
    print "<br />";

    exit;
}

function negate($operator) {
    $prefix = "";
    if (substr($operator, 0, 1) == '!') {
        $prefix = "NOT";
    }
    return $prefix;
}

function valueOnOperator($operator, $value) {
    $value = str_replace('"', '”', $value);
    $value = str_replace("'", "’", $value);
    $result = $value;
    switch ($operator) {
        case 'LIKE':
            $result = "'%{$value}%'";
            break;
        case '=':
            $result = "'{$value}'";
            break;
        case '<':
            $result = "'{$value}'";
            break;
        case '<=':
            $result = "'{$value}'";
            break;
        case '>':
            $result = "'{$value}'";
            break;
        case '>=':
            $result = "'{$value}'";
            break;
        
        default:
            break;
    }
    return $result;
}

?>
