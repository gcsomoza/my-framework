<?php

include "db.conf.php";

class DB {

    private $con;

    private function log($message) {
      $file = fopen(_DIR_ . "dberror.logs.".date("Ymd").".log","a");
      fwrite($file, date("Y-m-d H:i:s") . "\t: " . $message . "\n");
      fclose($file);
    }

    public function connect($perform = true) {

        if($perform) {

            $this->con = mysqli_connect(_DBHOST_,_DBUSER_,_DBPASS_);

            if(mysqli_connect_errno()) {
                $err = "Failed to connect to MySQL server. " . mysqli_connect_error();
                $this->log($err);
                die($err);
            }

            $this->con->select_db(_DBNAME_);

            mysqli_set_charset($this->con,"utf8");
        }
    }

    public function connect_nodb($perform = true) {

        if($perform) {

            $this->con = mysqli_connect(_DBHOST_,_DBUSER_,_DBPASS_);

            if(mysqli_connect_errno()) {
                $err = "Failed to connect to MySQL server. " . mysqli_connect_error();
                $this->log($err);
                die($err);
            }

            mysqli_set_charset($this->con,"utf8");
        }
    }

    public function select_db($dbname) {
        $this->con->select_db($dbname);
    }

    public function close($perform = true) {
        if($perform) {
            mysqli_close($this->con);
        }
    }

    public function create_schema($schema) {
        $sql =  "CREATE DATABASE `{$schema}`";
        return mysqli_query($this->con,$sql);
    }

    public function create_table($table) {
        $sql =  "CREATE TABLE `{$table}` (
                    `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
                    `is_deleted` BOOLEAN NOT NULL DEFAULT 0,
                    PRIMARY KEY (`id`)
                )
                ENGINE = InnoDB;";
        return mysqli_query($this->con,$sql);
    }

    public function alter_table($table, $column, $datatype, $value = "NULL") {
        $default = $value;
        if ($value != 'NULL') {
            if ($value === false || $value == '0') {
                $default = 0;
            } elseif ($value === true) {
                $default = 1;
            } elseif (is_string($value)) {
                $default = "''";
            }
        }
        
        $sql =  "ALTER TABLE `{$table}`
                ADD COLUMN `{$column}` {$datatype} DEFAULT {$default};";
        
        return mysqli_query($this->con,$sql);
    }

    public function escape($value) {
        return mysqli_real_escape_string($this->con,$value);
    }

    public function start_transaction($perform = true) {
        if($perform) {
            mysqli_autocommit($this->con,FALSE);
            $this->query("START TRANSACTION;");
        }
    }

    public function query($sql) {
        return mysqli_query($this->con,$sql);
    }

    public function commit($perform = true) {
        if($perform) {
            mysqli_commit($this->con);
            mysqli_autocommit($this->con,TRUE);
        }
    }

    public function rollback($perform = true) {
        if($perform) {
            mysqli_rollback($this->con);
            mysqli_autocommit($this->con,TRUE);
        }
    }

    public function fetch_array($query) {
        return mysqli_fetch_array($query);
    }

    public function fetch_assoc($query) {
        return mysqli_fetch_assoc($query);
    }

    public function fetch_fields($query) {
        return mysqli_fetch_fields($query);
    }

    public function free_result($query) {
        mysqli_free_result($query);
    }

    public function insert_id() {
        return mysqli_insert_id($this->con);
    }

    public function error() {
        return mysqli_error($this->con);
    }

    public function errno() {
        return mysqli_errno($this->con);
    }

}

?>
